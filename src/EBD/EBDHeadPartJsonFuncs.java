/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import static EBD.EBDMain.merger;
import EBD.EBDMain.EBDEnums;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import skyproc.FLST;
import skyproc.FormID;
import skyproc.GRUP_TYPE;
import skyproc.RACE;
import skyproc.SPGlobal;

/**
 *
 * @author Don
 */
public class EBDHeadPartJsonFuncs {

    public static ArrayList<String> getJSONBlocklist() {

        Gson gson;
        gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        final String PATH = "JsonExchange\\HeadpartBlocklist.json";

        ArrayList<String> tmpList = new ArrayList<>();

        File file = new File(PATH);

        if (file.exists()) {

            try (FileReader fr = new FileReader(file)) {

                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(fr);
                EBDBlocklistJson blockListObj = gson.fromJson(jsonElement, EBDBlocklistJson.class);
                tmpList.addAll(blockListObj.getBlocklist());
                fr.close();
                //FileWriter writer = new FileWriter (file);
                //file.delete();
                blockListObj.clearBlocklist();
                try (FileWriter writer = new FileWriter(PATH)) {
                    writer.write(gson.toJson(blockListObj));
                } catch (IOException e) {
                    System.err.println(e.toString());
                    SPGlobal.logException(e);
                    JOptionPane.showMessageDialog(null, "There was an exception while writing " + PATH + ": '" + e + "'");
                }
            } catch (IOException e) {
                System.err.println(e.toString());
                SPGlobal.logException(e);
                //JOptionPane.showMessageDialog(null, "There was an exception while writing HeadpartBlocklist.json: '" + e + "'  The headpart feature will not work without it.");
            }
        }

        return tmpList;
    }

    public static void writeJSON() {

        EBDHeadPartJson HDPTObj = new EBDHeadPartJson();

        Gson gson;
        gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        try (FileWriter writer = new FileWriter("JsonExchange\\HeadpartDB.json");) {
            //write converted json data to a file named "file.json"

            writer.write(gson.toJson(HDPTObj));
            writer.close();

        } catch (IOException e) {
            System.err.println(e.toString());
            SPGlobal.logException(e);
            JOptionPane.showMessageDialog(null, "There was an exception while writing HeadpartDB.json: '" + e + "'  The headpart feature will not work without it.");
        }

    }

    private static class EBDBlocklistJson {

        ArrayList<String> Blocklist;

        private EBDBlocklistJson() {
            this.Blocklist = new ArrayList<>();
        }

        private ArrayList<String> getBlocklist() {
            return this.Blocklist;
        }

        private void clearBlocklist() {
            this.Blocklist.clear();
        }
    }

    private static class EBDHeadPartJson {

        private Map<String, CGender> Gender;
        private int Initialized;
        private int Version;

        private EBDHeadPartJson() {
            this.Gender = new HashMap<>();
            this.Initialized = 20;
            this.Version = EBDMain.versionInt;

            String sGender = "";
            String Part = "";

            Map<String, FormID> headPartMap = new HashMap<>();

            for (EBDEnums.Gender GEN : EBDEnums.Gender.values()) {

                switch (GEN) {
                    case FEMALE:
                        sGender = "Female";

                        break;
                    case MALE:
                        sGender = "Male";

                        break;
                }

                int iEnabGen = 19;

                if (EBDMain.EBDSettings.isHeadPartEnabled(GEN)) {
                    iEnabGen = 20;
                }

                CGender genderClass = new CGender(sGender, iEnabGen);

                Gender.put(sGender, genderClass);

                hdpt_loop:
                for (EBDEnums.Headpart HEADPART : EBDEnums.Headpart.values()) {

                    EBDHeadPack HeadPack = new EBDHeadPack(merger, GEN, HEADPART, EBDMain.EBDSettings.isHeadDisableVanillaPartsEnabled(GEN, HEADPART));

                    switch (HEADPART) {
                        case HAIR:
                            Part = "Hair";

                            break;
                        case EYES:
                            Part = "Eyes";

                            break;
                        case SCARS:
                            Part = "Scars";

                            break;
                        case BROWS:
                            Part = "Brows";

                            break;
                        case BEARD:
                            Part = "Beard";

                            //if (GEN == EBDEnums.Gender.FEMALE) {
                                //no beards for females, sorry
                                //break hdpt_loop;
                            //}
                            break;
                    }

                    int iProb = EBDMain.EBDSettings.getHeadChangeProbab(GEN, HEADPART);
                    int iEnabType = 19;
                    if (EBDMain.EBDSettings.isHeadPartEnabled(GEN, HEADPART) && !HeadPack.getAllRaces().isEmpty()) {
                        iEnabType = 20;
                    }

                    CGender.CType typeClass = genderClass.addType(Part, iEnabType, iProb);

                    if (EBDMain.EBDSettings.isHeadPartEnabled(GEN, HEADPART)) {

                        Map<String, ArrayList<String>> RaceMap = new HashMap<>();
                        for (FormID raceID : HeadPack.getAllRaces()) {
                            //"ArgonianRace": "__formData|EveryBody's Different.esp|0x3161"
                            RACE raceRC = (RACE) merger.getMajor(raceID, GRUP_TYPE.RACE);
                            String EDID = raceRC.getEDID();
                            //FLST headPartList = new FLST("EBDHeadPartDummyList" + sGender + Part + raceRC.getEDID());

                            //String modName = headPartList.getFormMaster().print();
                            //String sForm = headPartList.getFormStr().replace(modName, "");
                            ArrayList<FormID> HeadListForm = HeadPack.getHeadPartsByRace(raceID);
                            ArrayList<String> HeadListString = new ArrayList<>();
                            for (FormID fForm : HeadListForm) {
                                headPartMap.put(fForm.getMaster().printNoSuffix(), fForm);
                                String modName = fForm.getMaster().print();
                                String sForm = fForm.getFormStr().replace(modName, "");
                                int iFormClean = Integer.parseInt(sForm, 16);
                                sForm = Integer.toHexString(iFormClean);
                                sForm = "0x" + sForm;
                                String PartForm = "__formData|" + modName + "|" + sForm;
                                HeadListString.add(PartForm);
                            }

                            RaceMap.put(EDID, HeadListString);

                        }
                        typeClass.setRaces(RaceMap);
                    }

                }

            }
            FLST headPartList = new FLST("EBDHeadPartDummyList");
            headPartList.addAll(headPartMap.values());

        }

        private class CGender {

            transient private String Gender;
            private int Enabled;
            private Map<String, CType> Type;

            private CGender() {

            }

            private CGender(String Gen, int Enab) {
                this.Gender = Gen;
                this.Enabled = Enab;
                this.Type = new HashMap<>();
            }

            private CType addType(String Type, int Enab, int Prob) {
                CType type = new CType(Type, Enab, Prob);
                this.Type.put(Type, type);
                return type;
            }

            @Override
            public String toString() {
                return Gender;
            }

            private class CType {

                private transient String Type;
                private int Probability;
                private int Enabled;
                private Map<String, ArrayList<String>> Races = new HashMap<>();

                private CType() {

                }

                private CType(String Type, int Enab, int Prob) {
                    this.Type = Type;
                    this.Probability = Prob;
                    this.Enabled = Enab;
                }

                private void setRaces(Map<String, ArrayList<String>> Races) {
                    this.Races = Races;
                }

                @Override
                public String toString() {
                    return Type;
                }
            }
        }

    }
}

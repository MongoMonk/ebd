/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import EBD.EBDMain.EBDEnums;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import skyproc.FormID;
import skyproc.SPGlobal;

/**
 *
 * @author Don
 */
public class EBDXMLTexPackParser {

    private void DebugLogMain(String message) {
        if (EBDMain.EBDSettings.isDebugEnabled()) {
            SPGlobal.log(this.getClass().getSimpleName(), message);
        }
    }

    private void DebugLogSummary(String message) {

        DebugLogMain(message);
        SPGlobal.logSpecial(EBDMain.EBDLOGS.SUMMARY, this.getClass().getSimpleName(), message);
    }
    
    private String XML_FILES_DIR;
    private SortedSet<MainPackStorage> MAIN_PACKS;
    private HashMap<String, FormID> raceMap;
    private HashMap<FormID, String> raceMapAlt;

    public EBDXMLTexPackParser(HashMap<String, ArrayList<String>> MainPacksWithSubPacks, EBDEnums.Gender GENDER, EBDEnums.Race RACE) {
        this.MAIN_PACKS = new TreeSet<>();
        this.XML_FILES_DIR = "TexturePackXmlFiles\\";
        this.raceMap = EBDMain.getAllRaces();
        this.raceMapAlt = EBDMain.getAllRacesAlt();

        switch (GENDER) {
            case FEMALE:
                XML_FILES_DIR += "Female";
                break;
            case MALE:
                XML_FILES_DIR += "Male";
                break;
        }

        switch (RACE) {
            case HUMANOID:
                XML_FILES_DIR += "\\";
                break;
            case KHAJIIT:
                XML_FILES_DIR += "Khajiit\\";
                break;
            case ARGONIAN:
                XML_FILES_DIR += "Argonian\\";
                break;
        }
        ArrayList<FormID> VALID_RACES_RUNTIME = EBDMain.getAllRaces(RACE);

        for (String MainPack : MainPacksWithSubPacks.keySet()) {
            ArrayList<String> SUB_PACKS = MainPacksWithSubPacks.get(MainPack);
            //turn underscores into two of the same and spaces into underscores to avoid xml errors
            //MainPack = this.stringConvertToXml(MainPack);
            MainPackStorage store = new MainPackStorage(MainPack, SUB_PACKS, VALID_RACES_RUNTIME);
            this.MAIN_PACKS.add(store);

            File XmlFile = new File(this.getFileString(store.getPackName()));
            if (!XmlFile.exists()) {
                try {
                    XmlFile.createNewFile();
                    //JOptionPane.showMessageDialog(null, "New FILE!");
                    this.writeMainPackXmlFile(store.getPackName());

                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "XML File Creation error. MainPack: " + store.getPackName() + ". Error: " + ex
                            + "\n" + "File: " + this.getFileString(store.getPackName()));
                }

            } else {
                try {
                    this.loadMainPackFromFileToMemory(MainPack);
                    this.writeMainPackXmlFile(MainPack);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "XML File Init error. Mainpack: " + store.getPackName() + " : " + ex);
                }
            }

        }

    }

    public ArrayList<String> getAllMainPacks() {
        ArrayList<String> tmpList = new ArrayList<>();
        this.MAIN_PACKS.stream().forEach((store) -> {
            tmpList.add(store.getPackName());
        });
        return tmpList;
    }

    public ArrayList<String> getActiveMainPacks() {
        ArrayList<String> tmpList = new ArrayList<>();
        this.MAIN_PACKS.stream().filter((store) -> (store.isActive())).forEach((store) -> {
            tmpList.add(store.getPackName());
        });
        return tmpList;
    }

    public boolean isMainPackActive(String MainPack) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            return store.isActive();
        }
        return false;
    }

    public boolean isSubPackActive(String MainPack, String SubPack) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            MainPackStorage.SubPackStorage subStore = store.getSubPackByName(SubPack);
            if (subStore != null) {
                return subStore.isActive();
            }
        }
        return false;
    }

    public void setMainPackActive(String MainPack, boolean b) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            store.setActive(b);
        }
    }

    public void setSubPackActive(String MainPack, String SubPack, boolean b) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            MainPackStorage.SubPackStorage subStore = store.getSubPackByName(SubPack);
            if (subStore != null) {
                subStore.setActive(b);
            }
        }
    }

    public ArrayList<String> getAllSubPacksByMainPack(String MainPack) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            return store.getAllSubPacks();
        } else {
            return new ArrayList<>();
        }
    }

    public ArrayList<String> getActiveSubPacksByMainPack(String MainPack) {
        MainPackStorage store = this.getPackByName(MainPack);
        if (store != null) {
            return store.getActiveSubPacks();
        } else {
            return new ArrayList<>();
        }
    }

    public void setActiveRaceForMainPackInMemory(String MP, FormID Race, Boolean active) {
        MainPackStorage store = this.getPackByName(MP);
        if (store != null) {
            store.addRace(Race, active);
        }
    }

    public void setActiveRaceForSubPackInMemory(String MP, String SP, FormID Race, Boolean active) {
        MainPackStorage store = this.getPackByName(MP);
        if (store != null) {
            store.addRaceSubPack(SP, Race, active);
            store.setRacesInactiveInAllSubPacks();
        }

    }

    public ArrayList<FormID> getActiveRacesForMainPack(String MainPack) {
        return this.getPackByName(MainPack).getActiveRaces();
    }

    public ArrayList<FormID> getAllRacesForMainPack(String MainPack) {
        return this.getPackByName(MainPack).getAllRaces();
    }

    public ArrayList<FormID> getAllRacesForSubPack(String MainPack, String SubPack) {
        return this.getPackByName(MainPack).getSubPackByName(SubPack).getAllRaces();
    }

    public ArrayList<FormID> getActiveRacesForSubPack(String MainPack, String SubPack) {
        return this.getPackByName(MainPack).getSubPackByName(SubPack).getActiveRaces();
    }

    public Boolean isRaceActiveForMainPack(String MainPack, FormID Race) {
        int isActive = this.getPackByName(MainPack).isRaceActive(Race);
        return isActive == 1;

    }

    public Boolean isRaceActiveForSubPack(String MainPack, String SubPack, FormID Race) {
        int isActive = this.getPackByName(MainPack).isRaceActiveSubPack(SubPack, Race);
        return isActive == 1;
    }

    private Document getDocumentByMainPack(String MainPack) {
        try {
            File file = new File(this.getFileString(MainPack));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(EBDXMLTexPackParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String getFileString(String s) {
        return (XML_FILES_DIR + s + ".xml");
    }

    private MainPackStorage getPackByName(String name) {
        for (MainPackStorage store : this.MAIN_PACKS) {
            if (store.getPackName().equals(name)) {
                return store;
            }
        }
        return null;
    }

    private void writeMainPackXmlFile(String MainPack) {
        try {
            ArrayList<String> SubPacks = this.getPackByName(MainPack).getAllSubPacks();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("MainPack");
            rootElement.setAttribute("Name", MainPack);
            if (this.getPackByName(MainPack).isActive()) {
                rootElement.setAttribute("isActive", "YES");
            } else {
                rootElement.setAttribute("isActive", "NO");
            }
            doc.appendChild(rootElement);

            Element MainPackRacesElement = doc.createElement("MainPackRaces");
            rootElement.appendChild(MainPackRacesElement);

            for (FormID race : this.getPackByName(MainPack).getAllRaces()) {
                Element RaceElement = doc.createElement("Race");
                RaceElement.setAttribute("FormID", race.getFormStr());
                RaceElement.setAttribute("EDID", this.raceMapAlt.get(race));
                int isRaceActive = this.getPackByName(MainPack).isRaceActive(race);
                if (isRaceActive == 1) {
                    RaceElement.setAttribute("isActive", "YES");
                } else if (isRaceActive == 0) {
                    RaceElement.setAttribute("isActive", "NO");
                }
                MainPackRacesElement.appendChild(RaceElement);
            }

            for (String subPack : SubPacks) {

                Element SubPackElement = doc.createElement("SubPack");
                SubPackElement.setAttribute("Name", subPack);
                if (this.getPackByName(MainPack).getSubPackByName(subPack).isActive()) {
                    SubPackElement.setAttribute("isActive", "YES");
                } else {
                    SubPackElement.setAttribute("isActive", "NO");
                }
                rootElement.appendChild(SubPackElement);

                Element SubPackRacesElement = doc.createElement("SubPackRaces");
                SubPackElement.appendChild(SubPackRacesElement);

                for (FormID race : this.getPackByName(MainPack).getSubPackByName(subPack).getAllRaces()) {
                    Element RaceElement = doc.createElement("Race");
                    RaceElement.setAttribute("FormID", race.getFormStr());
                    RaceElement.setAttribute("EDID", this.raceMapAlt.get(race));
                    int isRaceActive = this.getPackByName(MainPack).getSubPackByName(subPack).isRaceActive(race);
                    if (isRaceActive == 1) {
                        RaceElement.setAttribute("isActive", "YES");
                    } else if (isRaceActive == 0) {
                        RaceElement.setAttribute("isActive", "NO");
                    }
                    SubPackRacesElement.appendChild(RaceElement);
                }
            }

            this.writeXMLFile(doc, new File(this.getFileString(MainPack)));

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(EBDMain.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File 1 Creation error: " + MainPack + " : " + ex);
        }
    }

    private void loadMainPackFromFileToMemory(String MainPack) {
        Document doc = this.getDocumentByMainPack(MainPack);
        if (doc.getDocumentElement().getAttribute("isActive").equalsIgnoreCase("NO")) {
            this.getPackByName(MainPack).setActive(false);
        } else {
            this.getPackByName(MainPack).setActive(true);
        }
        NodeList rsMPListParent = doc.getElementsByTagName("MainPackRaces"); //only contains a single entry, there might be a better way to do this
        for (int j = 0; j < rsMPListParent.getLength(); j++) {
            Element MainPackRacesElement = (Element) rsMPListParent.item(j);
            NodeList rList = MainPackRacesElement.getElementsByTagName("Race");
            for (int i = 0; i < rList.getLength(); i++) {
                Element raceElement = (Element) rList.item(i);
                String raceString = raceElement.getAttribute("FormID");
                String raceEDID = raceElement.getAttribute("EDID");                 //get EDID attribute if FormID not found; legacy support for xml files created with EBD version < 3.30
                FormID raceID;
                if (!raceString.isEmpty()) {
                    raceID = new FormID(raceString);
                } else {
                    raceID = this.raceMap.get(raceEDID);
                }
                if (raceID == null) {
                    DebugLogSummary("Error when reading race from xml file: " + raceEDID + "_" + raceString + ". Race was not found in game; will be skipped.");
                    continue;
                }
                int isRaceActive = this.getPackByName(MainPack).isRaceActive(raceID);
                if (isRaceActive != -1) {
                    if (raceElement.getAttribute("isActive").equalsIgnoreCase("YES")) {
                        this.getPackByName(MainPack).addRace(raceID, Boolean.TRUE);
                    } else {
                        this.getPackByName(MainPack).addRace(raceID, Boolean.FALSE);
                    }
                }
            }
        }

        NodeList sList = doc.getElementsByTagName("SubPack");

        for (int i = 0; i < sList.getLength(); i++) {
            Element subPackElement = (Element) sList.item(i);
            String subPackName = subPackElement.getAttribute("Name");
            MainPackStorage.SubPackStorage subPack = this.getPackByName(MainPack).getSubPackByName(subPackName);
            if (subPack != null) {
                if (subPackElement.getAttribute("isActive").equalsIgnoreCase("NO")) {
                    subPack.setActive(false);
                } else {
                    subPack.setActive(true);
                }
                NodeList rsListParent = subPackElement.getElementsByTagName("SubPackRaces"); //only contains a single entry, there might be a better way to do this
                for (int j = 0; j < rsListParent.getLength(); j++) {
                    Element SubPackRacesElement = (Element) rsListParent.item(j);
                    NodeList rsList = SubPackRacesElement.getElementsByTagName("Race");
                    for (int k = 0; k < rsList.getLength(); k++) {
                        Element raceElement = (Element) rsList.item(k);
                        String raceString = raceElement.getAttribute("FormID");
                        String raceEDID = raceElement.getAttribute("EDID");                 //get EDID attribute if FormID not found; legacy support for xml files created with EBD version < 3.30
                        FormID raceID;
                        if (!raceString.isEmpty()) {
                            raceID = new FormID(raceString);
                        } else {
                            raceID = this.raceMap.get(raceEDID);
                        }
                        if (raceID == null) {
                            DebugLogSummary("Error when reading race from xml file: " + raceEDID + "_" + raceString + ". Race was not found in game; will be skipped.");
                            continue;
                        }
                        int isRaceActive = this.getPackByName(MainPack).getSubPackByName(subPackName).isRaceActive(raceID);
                        if (isRaceActive != -1) {
                            if (raceElement.getAttribute("isActive").equalsIgnoreCase("YES")) {
                                this.setActiveRaceForSubPackInMemory(MainPack, subPackName, raceID, Boolean.TRUE);
                            } else {
                                this.setActiveRaceForSubPackInMemory(MainPack, subPackName, raceID, Boolean.FALSE);
                            }
                        }
                    }
                }
            }
        }
    }

    public void updateXMLMainPackFromUI(String MainPack) {
        this.writeMainPackXmlFile(MainPack);
    }

    private void writeXMLFile(Document doc, File file) {
        try {

            doc.getDocumentElement().normalize();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(file);

            transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
            transformer.setOutputProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Logger.getLogger(EBDXMLTexPackParser.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File Write error: " + file.getName() + " : " + ex);
        }
    }

    private class MainPackStorage implements Comparable<MainPackStorage> {

        private boolean isActive;

        private final String PACK_NAME;
        private final HashMap<FormID, Boolean> RaceData;
        private final SortedSet<SubPackStorage> SubPacks;

        private MainPackStorage(String name, ArrayList<String> SubPacksList, ArrayList<FormID> races) {
            this.PACK_NAME = name;
            this.RaceData = new HashMap<>();
            this.SubPacks = new TreeSet<>();
            HashSet<String> SubNames = new HashSet<>(SubPacksList);
            SubNames.stream().forEach((SubPack) -> {
                this.SubPacks.add(new SubPackStorage(SubPack));
            });
            races.stream().forEach((race) -> {
                this.RaceData.put(race, Boolean.TRUE);
                this.SubPacks.stream().forEach((subStore) -> {
                    subStore.addNewRace(race, Boolean.TRUE);
                });
            });
            this.isActive = true;

        }

        public void setRacesInactiveInAllSubPacks() {  //sets a race which is inactive in all subpacks inactive in the mainpack as well
            HashSet<FormID> tmpList = new HashSet<>();
            this.SubPacks.stream().forEach((subStore) -> {
                this.getActiveRaces().stream().filter((raceName) -> (subStore.isRaceActive(raceName) == 1)).forEach((raceName) -> {
                    tmpList.add(raceName);
                });
            });
            this.getActiveRaces().stream().filter((raceName) -> (!tmpList.contains(raceName))).forEach((raceName) -> {
                this.addRace(raceName, Boolean.FALSE);
            });
        }

        public boolean isActive() {
            return this.isActive;
        }

        public void setActive(boolean b) {
            this.isActive = b;
        }

        public SubPackStorage getSubPackByName(String SubPack) {
            for (SubPackStorage subStore : this.SubPacks) {
                if (subStore.getPackName().equalsIgnoreCase(SubPack)) {
                    return subStore;
                }
            }
            return null;
        }

        public ArrayList<String> getAllSubPacks() {
            ArrayList<String> tmpList = new ArrayList<>();
            this.SubPacks.stream().forEach((subStore) -> {
                tmpList.add(subStore.getPackName());
            });
            return tmpList;
        }

        public ArrayList<String> getActiveSubPacks() {
            ArrayList<String> tmpList = new ArrayList<>();
            this.SubPacks.stream().filter((subStore) -> (subStore.isActive())).forEach((subStore) -> {
                tmpList.add(subStore.getPackName());
            });
            return tmpList;
        }

        public void addRaceSubPack(String SubPack, FormID race, Boolean active) {
            this.SubPacks.stream().filter((subStore) -> (subStore.getPackName().equalsIgnoreCase(SubPack))).forEach((subStore) -> {
                subStore.addNewRace(race, active);
            });
        }

        public int isRaceActiveSubPack(String SubPack, FormID race) {
            for (SubPackStorage subStore : this.SubPacks) {
                if (subStore.getPackName().equalsIgnoreCase(SubPack)) {
                    return subStore.isRaceActive(race);
                }
            }
            return -1;
        }

        public void removeRaceSubPack(String SubPack, FormID race) {
            this.SubPacks.stream().filter((subStore) -> (subStore.getPackName().equalsIgnoreCase(SubPack))).forEach((subStore) -> {
                subStore.removeRace(race);
            });
        }

        public ArrayList<FormID> getAllRaces() {
            SortedSet<FormID> keys = new TreeSet<>(this.RaceData.keySet());
            return new ArrayList<>(keys);
        }

        public ArrayList<FormID> getActiveRaces() {
            SortedSet<FormID> tmpList = new TreeSet<>();
            this.RaceData.keySet().stream().filter((race) -> (this.RaceData.get(race))).forEach((race) -> {
                tmpList.add(race);
            });
            return new ArrayList<>(tmpList);
        }

        public int isRaceActive(FormID race) {
            //-1 not there
            //0 not active
            //1 active
            int active;
            Boolean b = this.RaceData.get(race);
            if (b == null) {
                active = -1;
            } else if (!b) {
                active = 0;
            } else {
                active = 1;
            }
            return active;
        }

        public String getPackName() {
            return this.PACK_NAME;
        }

        public void removeRace(FormID race) {
            this.RaceData.remove(race);
            this.SubPacks.stream().forEach((subStore) -> {
                subStore.removeRace(race);
            });

        }

        public void addRace(FormID race, Boolean active) {
            this.RaceData.put(race, active);
            this.SubPacks.stream().forEach((subStore) -> {
                if (active) {
                    //only add to substore if race is not present there
                    if (subStore.isRaceActive(race) == -1) {
                        subStore.addNewRace(race, active);
                    }
                } else {
                    subStore.removeRace(race);
                }
            });
        }

        @Override
        public int compareTo(MainPackStorage m) {
            return this.PACK_NAME.compareTo(m.PACK_NAME);
        }

        private class SubPackStorage implements Comparable<SubPackStorage> {

            private boolean isActive;

            private final String PACK_NAME;
            private final HashMap<FormID, Boolean> RaceData;

            private SubPackStorage(String name) {
                this.PACK_NAME = name;
                this.RaceData = new HashMap<>();
                this.isActive = true;
            }

            public boolean isActive() {
                return this.isActive;
            }

            public void setActive(boolean b) {
                this.isActive = b;
            }

            public ArrayList<FormID> getAllRaces() {
                SortedSet<FormID> keys = new TreeSet<>(this.RaceData.keySet());
                return new ArrayList<>(keys);
            }

            public ArrayList<FormID> getActiveRaces() {
                SortedSet<FormID> tmpList = new TreeSet<>();
                this.RaceData.keySet().stream().filter((race) -> (this.RaceData.get(race))).forEach((race) -> {
                    tmpList.add(race);
                });
                return new ArrayList<>(tmpList);
            }

            public int isRaceActive(FormID race) {
                //-1 not there
                //0 not active
                //1 active
                int active;
                Boolean b = this.RaceData.get(race);
                if (b == null) {
                    active = -1;
                } else if (!b) {
                    active = 0;
                } else {
                    active = 1;
                }
                return active;
            }

            public String getPackName() {
                return this.PACK_NAME;
            }

            public void removeRace(FormID race) {
                this.RaceData.remove(race);
            }

            public void addNewRace(FormID race, Boolean active) {
                this.RaceData.put(race, active);
            }

            @Override
            public int compareTo(SubPackStorage s) {
                return this.PACK_NAME.compareTo(s.PACK_NAME);
            }
        }
    }
}

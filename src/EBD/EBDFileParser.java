/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import EBD.EBDMain.EBDEnums;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Objects;
import skyproc.*;

/**
 *
 * @author Don
 */
public class EBDFileParser {
    //INFO: TX00:Color; TX01:Normal; TX02:EnvironmentMask; TX03:DetailSK; TX04:Height; TX05:Environment; TX06:Multilayer; TX07:SpacularS    

    private static final String EBD_TEX_DIR = SPGlobal.pathToData + "\\textures\\EBD";  //absolute path for the ebd dir 
    private static final String EBD_TEX_FEM_DIR = EBD_TEX_DIR + "\\Female";  //female body and hand textures
    private static final String EBD_TEX_MALE_DIR = EBD_TEX_DIR + "\\Male";
    //   private String[] FemaleBodyFiles = {"FemaleBody_1.dds", "FemaleBody_1_msn.dds", "FemaleBody_1_sk.dds", "FemaleBody_1_s.dds"};
    //  private String[] FemaleHandsFiles = {"FemaleHands_1.dds", "FemaleHands_1_msn.dds", "FemaleHands_1_sk.dds", "FemaleHands_1_s.dds"};
    //contains both body and hands files in the correct order; makes more sense
    private static final String[] TEX_FILES_HUMAN = {"Body_1.dds", "Body_1_msn.dds", "NoFile2", "Body_1_sk.dds", "NoFile4", "NoFile5", "NoFile6", "Body_1_s.dds",
        "Hands_1.dds", "Hands_1_msn.dds", "NoFile2", "Hands_1_sk.dds", "NoFile4", "NoFile5", "NoFile6", "Hands_1_s.dds",
        "Head.dds", "Head_msn.dds", "NoFile2", "Head_sk.dds", "Detailmap.dds", "NoFile5", "NoFile6", "Head_S.dds"};
    private static final String[] TEX_FILES_BEAST = {"Body.dds", "Body_msn.dds", "NoFile2", "Body_sk.dds", "NoFile4", "NoFile5", "NoFile6", "Body_s.dds",
        "Hands.dds", "Hands_msn.dds", "NoFile2", "Hands_sk.dds", "NoFile4", "NoFile5", "NoFile6", "Hands_s.dds",
        "Head.dds", "Head_msn.dds", "NoFile2", "Head_sk.dds", "Detailmap.dds", "NoFile5", "NoFile6", "Head_S.dds"};
    private static final String[] MESH_FILES = {"body_0.nif", "body_1.nif", "feet_0.nif", "feet_1.nif",
        "hands_0.nif", "hands_1.nif"};
    private final String FEM_STRING = "Female";
    private final String ARGONIAN_STRING = "Argonian";
    private final String KHAJIIT_STRING = "Khajiit";
    private final String MALE_STRING = "Male";
    private final String HUMANOID_STRING = "Humanoid";
    private String Parser_Gender = "";
    private String Parser_Race = "";
    TexturePack MasterTexPack;
    private final int NoOfFiles = 8;

    private void DebugLogMain(String message) {
        if (EBDMain.EBDSettings.isDebugEnabled()) {
            SPGlobal.log(this.getClass().getSimpleName(), message);
        }
    }

    private void DebugLogSummary(String message) {

        DebugLogMain(message);
        SPGlobal.logSpecial(EBDMain.EBDLOGS.SUMMARY, this.getClass().getSimpleName() + "_" + this.Parser_Gender + this.Parser_Race, message);
    }

    private boolean isLatinInput(String input) {
        return input.matches("\\w+");
    }

    public EBDFileParser(EBDEnums.Gender GENDER, EBDEnums.Race RACE) {
        File fTex = null;
        String[] sTexFilesToGet = new String[TEX_FILES_HUMAN.length];
        String[] sMeshFilesToGet = new String[MESH_FILES.length];
        DebugLogMain("ParserStart");
        DebugLogMain("Create " + GENDER.name() + " " + RACE.name() + " File Parser.");
        try {
            final String fileString;
            final String genString;
            switch (GENDER) {
                case FEMALE:
                    fileString = EBD_TEX_FEM_DIR;
                    genString = FEM_STRING;
                    this.Parser_Gender = this.FEM_STRING;
                    break;
                case MALE:
                    fileString = EBD_TEX_MALE_DIR;
                    genString = MALE_STRING;
                    this.Parser_Gender = this.MALE_STRING;
                    break;
                default:
                    fileString = "";
                    genString = "";
            }

            for (int i = 0; i < sMeshFilesToGet.length; i++) {
                sMeshFilesToGet[i] = genString + MESH_FILES[i];
            }

            switch (RACE) {
                case HUMANOID:
                    this.Parser_Race = this.HUMANOID_STRING;
                    fTex = new File(fileString);
                    for (int i = 0; i < sTexFilesToGet.length; i++) {
                        sTexFilesToGet[i] = genString + TEX_FILES_HUMAN[i];
                        DebugLogMain("Look for tex file:" + sTexFilesToGet[i]);
                    }
                    break;
                case KHAJIIT:
                    this.Parser_Race = this.KHAJIIT_STRING;
                    fTex = new File(fileString + KHAJIIT_STRING);
                    for (int i = 0; i < sTexFilesToGet.length; i++) {
                        sTexFilesToGet[i] = genString + TEX_FILES_BEAST[i];
                        DebugLogMain("Look for tex file:" + sTexFilesToGet[i]);
                    }
                    break;

                case ARGONIAN:
                    this.Parser_Race = this.ARGONIAN_STRING;
                    fTex = new File(fileString + ARGONIAN_STRING);
                    for (int i = 0; i < sTexFilesToGet.length; i++) {
                        sTexFilesToGet[i] = ARGONIAN_STRING + genString + TEX_FILES_BEAST[i];
                        DebugLogMain("Look for tex file:" + sTexFilesToGet[i]);
                    }
                    break;
            }
            if (fTex != null) {
                if (fTex.exists()) {
                    DebugLogMain("Base Path: " + fTex.getPath());
                    MasterTexPack = new TexturePack(fTex, sTexFilesToGet, sMeshFilesToGet);

                } else {
                    DebugLogSummary("ERROR: Base Path: " + fTex.getPath() + " does not exist.");
                    MasterTexPack = null;
                }
            }

        } catch (Exception e) {
            // If a major error happens, print it everywhere and display a message box.
            System.err.println("Error in Constructor: " + e.toString());
            DebugLogSummary("Error in Constructor: " + e.toString());
            MasterTexPack = null;

        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.MasterTexPack);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EBDFileParser other = (EBDFileParser) obj;
        return Objects.equals(this.MasterTexPack, other.MasterTexPack);
    }

    class TexturePack {

        private ArrayList<MainPack> MainPacks;
        private final String[] sTexFileNames;
        private final String[] sMeshFileNames;
        private final String FILE_NOT_FOUND = "";
        private final String[] sBodyTexFileNames;
        private final String[] sHandTexFileNames;
        private final String[] sHeadTexFileNames;

        private class DirectoryFilter implements FilenameFilter {

            @Override
            public boolean accept(File f, String s) {
                return f.isDirectory();
            }
        }

        public TexturePack(File TexPackFolder, String[] sTexFilesToLookFor, String[] sMeshFilesToLookFor) {

            this.MainPacks = new ArrayList<>();
            this.sTexFileNames = sTexFilesToLookFor;
            this.sMeshFileNames = sMeshFilesToLookFor;

            this.sBodyTexFileNames = Arrays.copyOfRange(sTexFileNames, 0, 8);
            this.sHandTexFileNames = Arrays.copyOfRange(sTexFileNames, 8, 16);
            this.sHeadTexFileNames = Arrays.copyOfRange(sTexFileNames, 16, 24);

            DebugLogMain("TexturePackConstructor");
            for (File tmpFile : TexPackFolder.listFiles()) {
                if (tmpFile.isDirectory()) { //has to be a folder         
                    if (tmpFile.listFiles(new DirectoryFilter()).length > 0) { //has to contain subfolders 
                        DebugLogMain("Main Folders found: " + tmpFile.getName());
                        MainPack tmpPack = new MainPack(tmpFile);
                        if (!tmpPack.getSubPacksList().isEmpty()) {
                            MainPacks.add(tmpPack);
                            DebugLogMain("Main Folder " + tmpFile.getName() + " is valid MainPack");
                        }
                    }
                }
            }
            if (MainPacks.isEmpty()) {
                DebugLogSummary("No Mainpacks found, aborting.");
            } else {
                DebugLogSummary("All Base SubPacks were created successfully. Number of MainPacks: " + Integer.toString(this.MainPacks.size()));
                for (int i = 0; i < this.MainPacks.size(); i++) {
                    DebugLogSummary("MainPack No. " + Integer.toString(i + 1) + ": " + this.MainPacks.get(i).getMainPackName() + " includes " + Integer.toString(this.MainPacks.get(i).getSubPacks(EBDEnums.TextureType.BODY).size()) + " Body SubPacks. " + Integer.toString(this.MainPacks.get(i).getSubPacks(EBDEnums.TextureType.HANDS).size()) + " Hand SubPacks. " + Integer.toString(this.MainPacks.get(i).getSubPacks(EBDEnums.TextureType.FACE).size()) + " Head SubPacks. " + Integer.toString(this.MainPacks.get(i).getNoOfMeshFiles()) + " Mesh Files.");
                }
            }
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 29 * hash + Objects.hashCode(this.MainPacks);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final TexturePack other = (TexturePack) obj;
            if (!Arrays.deepEquals(this.sTexFileNames, other.sTexFileNames)) {
                return false;
            }
            return Arrays.deepEquals(this.sMeshFileNames, other.sMeshFileNames);
        }

        public MainPack getMainPack(String Name) {
            for (MainPack mp : this.MainPacks) {
                if (mp.getMainPackName().equalsIgnoreCase(Name)) {
                    return mp;
                }
            }
            return null;
        }

        public int getNoOfMainPacks() {
            return this.MainPacks.size();
        }

        public ArrayList<String> getSubPackFolderListByMainPack(String MainPack) {
            for (MainPack mpack : this.MainPacks) {
                if (mpack.getMainPackName().equalsIgnoreCase(MainPack)) {
                    return mpack.getSubPacksList();
                }
            }
            return new ArrayList<>();
        }

        public ArrayList<String> getAllMainPacksNames() {
            ArrayList<String> tmpStr = new ArrayList<>();
            this.MainPacks.stream().forEach((tmpPack) -> {
                tmpStr.add(tmpPack.getMainPackName());
            });
            return tmpStr;
        }

        public String getMissingFileString() {
            return this.FILE_NOT_FOUND;
        }

        private String[] getTextureFiles(EBDEnums.TextureType TYPE) {
            switch (TYPE) {
                case BODY:
                    return this.sBodyTexFileNames;
                case HANDS:
                    return this.sHandTexFileNames;
                case FACE:
                    return this.sHeadTexFileNames;
                default:
                    return null;
            }
        }

        class MainPack {

            private class BodyDDSFilter implements FilenameFilter {

                @Override
                public boolean accept(File f, String s) {
                    return s.toLowerCase().endsWith(".dds") && s.toLowerCase().contains("body");
                }
            }

            private class HandDDSFilter implements FilenameFilter {

                @Override
                public boolean accept(File f, String s) {
                    return s.toLowerCase().endsWith(".dds") && s.toLowerCase().contains("hands");
                }
            }

            private class HeadDDSFilter implements FilenameFilter {

                @Override
                public boolean accept(File f, String s) {
                    return s.toLowerCase().endsWith(".dds") && s.toLowerCase().contains("head");
                }
            }

            private class NIFFilter implements FilenameFilter {

                @Override
                public boolean accept(File f, String s) {
                    return s.toLowerCase().endsWith(".nif");
                }
            }
            private LinkedHashSet<SubPack> SubPacksBody;
            private LinkedHashSet<SubPack> SubPacksHands;
            private LinkedHashSet<SubPack> SubPacksHead;
            private LinkedHashSet<SubPack> finishedPacks;
            private LinkedHashSet<String> SubPackList;
            private Boolean[] hasMeshFile;
            private String[] sMeshFilePath;
            private String sMeshFolder;
            private File fMeshFile;
            private final File PACK_FILE;
            private final BodyDDSFilter BodyDDSFilter = new BodyDDSFilter();
            private final HandDDSFilter HandsDDSFilter = new HandDDSFilter();
            private final HeadDDSFilter HeadDDSFilter = new HeadDDSFilter();
            private final NIFFilter NIFFilter = new NIFFilter();
            private final static String MeshString = "\\meshes\\";
            private String MeshPackRelPath;

            public MainPack(File MainPackTexFolder) {

                this.SubPacksBody = new LinkedHashSet<>();
                this.SubPacksHands = new LinkedHashSet<>();
                this.SubPacksHead = new LinkedHashSet<>();
                this.SubPackList = new LinkedHashSet<>();

                this.PACK_FILE = MainPackTexFolder;
                this.hasMeshFile = new Boolean[sMeshFileNames.length];
                this.sMeshFilePath = new String[sMeshFileNames.length];
                this.sMeshFolder = MainPackTexFolder.getPath().toLowerCase().replace("textures", "meshes");
                this.MeshPackRelPath = this.sMeshFolder.substring(this.sMeshFolder.indexOf(MeshString) + MeshString.length(), this.sMeshFolder.length());
                this.fMeshFile = null;
                try {
                    this.fMeshFile = new File(this.sMeshFolder);
                    DebugLogMain("Check Mesh folder " + this.sMeshFolder);
                } catch (Exception e) {
                    DebugLogMain("Warning: Mesh folder " + this.sMeshFolder + " does not exist: " + e);
                }

                for (File tmpFile : MainPackTexFolder.listFiles()) {
                    if (tmpFile.isDirectory()) { //has to be a folder and has to contain at least one .dds file

                        if (tmpFile.listFiles(BodyDDSFilter).length > 0) {
                            DebugLogMain("Create Base SubPack Body: " + tmpFile.getName());
                            SubPack tmpPack = new SubPack(tmpFile, EBDEnums.TextureType.BODY);
                            if (tmpPack.getNumberOfFiles() > 0) {
                                this.SubPacksBody.add(tmpPack);
                                this.SubPackList.add(tmpFile.getName());
                                DebugLogMain("Create Base SubPack Body: " + tmpFile.getName() + " was created");
                            }
                        }
                        if (tmpFile.listFiles(HandsDDSFilter).length > 0) {
                            DebugLogMain("Create Base SubPack Hands: " + tmpFile.getName());
                            SubPack tmpPack = new SubPack(tmpFile, EBDEnums.TextureType.HANDS);
                            if (tmpPack.getNumberOfFiles() > 0) {
                                this.SubPacksHands.add(tmpPack);
                                this.SubPackList.add(tmpFile.getName());
                                DebugLogMain("Create Base SubPack Hands: " + tmpFile.getName() + " was created");
                            }
                        }
                        if (tmpFile.listFiles(HeadDDSFilter).length > 0) {
                            DebugLogMain("Create Base SubPack Head: " + tmpFile.getName());
                            SubPack tmpPack = new SubPack(tmpFile, EBDEnums.TextureType.FACE);
                            if (tmpPack.getNumberOfFiles() > 0) {
                                this.SubPacksHead.add(tmpPack);
                                this.SubPackList.add(tmpFile.getName());
                                DebugLogMain("Create Base SubPack Head: " + tmpFile.getName() + " was created");
                            }
                        }
                    }
                }

                if (this.SubPacksBody.isEmpty()) {
                    DebugLogSummary("No Base Body Subpacks found inside " + this.PACK_FILE.getName());
                }
                if (this.SubPacksHands.isEmpty()) {
                    DebugLogSummary("No Base Hand Subpacks found inside " + this.PACK_FILE.getName());
                }
                if (this.SubPacksHead.isEmpty()) {
                    DebugLogSummary("No Base Head Subpacks found inside " + this.PACK_FILE.getName());
                }
                for (int i = 0; i < this.hasMeshFile.length; i++) {
                    this.hasMeshFile[i] = false;
                    sMeshFilePath[i] = FILE_NOT_FOUND;
                }

                if (fMeshFile.exists()) {
                    DebugLogMain("Mesh folder " + fMeshFile.getName() + " exists. Looking for .nif files");
                    for (int i = 0; i < this.hasMeshFile.length; i++) {
                        for (String sFile : fMeshFile.list(NIFFilter)) {
                            if (sFile.equalsIgnoreCase(sMeshFileNames[i])) {
                                this.hasMeshFile[i] = true;
                                DebugLogMain("Found Meshfile in Mainpack: " + sMeshFileNames[i] + " in " + fMeshFile.getName());

                            }
                            sMeshFilePath[i] = this.MeshPackRelPath + "\\" + sMeshFileNames[i];
                        }
                    }
                    DebugLogMain("Found " + Integer.toString(this.getNoOfMeshFiles()) + " meshes in Mainpack " + fMeshFile.getName());
                }

            }

            public SubPack getSubPackByName(String SubName, EBDEnums.TextureType TYPE) {
                for (SubPack pack : this.getSubPacks(TYPE)) {
                    if (pack.getSubPackFolderName().equalsIgnoreCase(SubName)) {
                        return pack;
                    }
                }
                return null;
            }

            @Override
            public int hashCode() {
                int hash = 7;
                hash = 97 * hash + Objects.hashCode(this.SubPacksBody);
                hash = 97 * hash + Objects.hashCode(this.SubPacksHands);
                hash = 97 * hash + Objects.hashCode(this.SubPacksHead);
                hash = 97 * hash + Objects.hashCode(this.SubPackList);
                hash = 97 * hash + Objects.hashCode(this.PACK_FILE);
                return hash;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final MainPack other = (MainPack) obj;
                return Objects.equals(this.PACK_FILE, other.PACK_FILE);
            }

            public LinkedHashSet<MainPack.SubPack> getCustomSubPacks(ArrayList<String> FolderNames, EBDEnums.TextureType TYPE) {
                LinkedHashSet<MainPack.SubPack> tmpListSubPack = new LinkedHashSet<>();

                DebugLogMain("Create Custom SubPack: " + TYPE.name() + " with folders: " + FolderNames);
                for (String FolderName : FolderNames) {
                    DebugLogMain("Find SubPack for Custom SubPack Creation: " + TYPE.name() + ". Folder: " + FolderName);

                    if (this.getSubPackByName(FolderName, TYPE) != null) {
                        SubPack tmpPack = new SubPack(this.getSubPackByName(FolderName, TYPE));
                        tmpListSubPack.add(tmpPack);
                        DebugLogMain("Create Custom SubPack: " + TYPE.name() + ". Name: " + tmpPack.getSubPackFolderName() + " found.");
                    }
                }

                tmpListSubPack = this.getFinalSubPacks(tmpListSubPack, TYPE);

                for (SubPack sub : tmpListSubPack) {
                    DebugLogMain("CUSTOM_SUBPACK: " + sub.getRelativePathAllFilesSingleString() + " FROM FOLDER: " + FolderNames.toString());
                }

                return tmpListSubPack;
            }

            private MainPack(MainPack oldPack) {
                this.SubPacksBody = new LinkedHashSet<>(oldPack.getSubPacks(EBDEnums.TextureType.BODY));
                this.SubPacksHands = new LinkedHashSet<>(oldPack.getSubPacks(EBDEnums.TextureType.HANDS));
                this.SubPacksHead = new LinkedHashSet<>(oldPack.getSubPacks(EBDEnums.TextureType.FACE));
                this.PACK_FILE = oldPack.PACK_FILE;
            }

            public int getMaxNoOfFiles(EBDEnums.TextureType TYPE) {
                return getTextureFiles(TYPE).length;
            }

            public String getMainPackName() {
                return this.PACK_FILE.getName();
            }

            public ArrayList<String> getSubPacksList() {
                return new ArrayList<>(this.SubPackList);
            }

            public String getMeshFile(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        if (this.hasMeshFile(1)) {
                            return this.getMeshFile(1);
                        }
                    case FEET:
                        if (this.hasMeshFile(3)) {
                            return this.getMeshFile(3);
                        }
                    case HANDS:
                        if (this.hasMeshFile(5)) {
                            return this.getMeshFile(5);
                        }
                    default:
                        return FILE_NOT_FOUND;
                }
            }

            public boolean hasMeshFile(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        if (this.hasMeshFile(1)) {
                            return true;
                        }
                    case FEET:
                        if (this.hasMeshFile(3)) {
                            return true;
                        }
                    case HANDS:
                        if (this.hasMeshFile(5)) {
                            return true;
                        }
                    default:
                        return false;
                }
            }

            private int getNoOfMeshFiles() {
                int NoOfMeshes = 0;
                for (boolean b : this.hasMeshFile) {
                    if (b) {
                        NoOfMeshes++;
                    }
                }
                return NoOfMeshes;
            }

            public String getMeshFile(int NoOfFile) {
                if (this.hasMeshFile[NoOfFile]) {
                    return this.sMeshFilePath[NoOfFile];
                } else {
                    return FILE_NOT_FOUND;
                }
            }

            protected boolean hasMeshFile(int NoOfFile) {
                return this.hasMeshFile[NoOfFile];
            }

            private LinkedHashSet<SubPack> getFinalSubPacks(LinkedHashSet<SubPack> SubPacks, EBDEnums.TextureType TYPE) {
                this.finishedPacks = new LinkedHashSet<>();
                SubPacks = this.setupSubPacksMissingFiles(SubPacks, TYPE);
                DebugLogMain(TYPE.toString() + "_SetUpSubPacksRecursive");
                setupSubPacksRecursiv(SubPacks, SubPacks);
                DebugLogMain(TYPE.toString() + "_CountSubPacksBeforeDupeRemove: " + SubPacks.size());
                SubPacks = this.finishedPacks;
                DebugLogMain(TYPE.toString() + "_CountSubPacksAfterDupeRemove: " + SubPacks.size());
                return SubPacks;
            }

            private LinkedHashSet<SubPack> setupSubPacksMissingFiles(LinkedHashSet<SubPack> workPackAr, EBDEnums.TextureType TYPE) {
                boolean[] hasFile = new boolean[this.getMaxNoOfFiles(TYPE)];

                for (int i = 0; i < hasFile.length; i++) {
                    hasFile[i] = false;
                }

                workPackAr.stream().forEach((tmpPack) -> {
                    for (int i = 0; i < hasFile.length; i++) {
                        if (tmpPack.hasFileSet(i)) {
                            hasFile[i] = true;
                        }
                    }
                });

                workPackAr.stream().forEach((tmpPack) -> {

                    for (int i = 0; i < hasFile.length; i++) {
                        if (!hasFile[i]) {
                            tmpPack.setPathOfFile(i, FILE_NOT_FOUND);
                        }

                    }

                });
                return workPackAr;
            }

            public LinkedHashSet<SubPack> getSubPacks(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        return this.SubPacksBody;
                    case HANDS:
                        return this.SubPacksHands;
                    case FACE:
                        return this.SubPacksHead;
                    default:
                        return null;
                }
            }
            

            private void setupSubPacksRecursiv(LinkedHashSet<SubPack> workPackList, LinkedHashSet<SubPack> comparePackList) {
                LinkedHashSet<SubPack> intern = new LinkedHashSet<>();
                boolean isFinished = true;
                boolean fileAdded;

                for (SubPack workPack : workPackList) {
                    DebugLogMain("EBDLevelInternWork + " + "look for workPack: " + workPack.getTexPackNamesSingleString() + " withNoFiles: " + workPack.getNumberOfFiles() + "/" + workPack.getNumberOfMaxFiles());
                    DebugLogMain("EBDWorkPackHasFiles" + workPack.getRelativePathAllFilesSingleString());
                    if (workPack.hasAllFilesSet()) {
                        intern.add(workPack);
                    } else {
                        for (SubPack comparePack : comparePackList) {  //go through all comparePacks
                            DebugLogMain("EBDLevelInternFilesMiising+ " + "look for comparePack: " + comparePack.getTexPackNamesSingleString() + " withNoFiles: " + comparePack.getNumberOfFiles() + "/" + comparePack.getNumberOfMaxFiles());
                            DebugLogMain("EBDComparePackHasFiles" + comparePack.getRelativePathAllFilesSingleString());
                            fileAdded = false;
                            SubPack newWorkPack = new SubPack(workPack);
                            // the intersetion check should make sure that files in subpaks "stay together"
                            // but I don't think it makes sense on a single type level
                            // deactivated for now
//                            if (!newWorkPack.hasIntersectingBaseFiles(comparePack)) {
                            for (int j = 0; j < newWorkPack.getNumberOfMaxFiles(); j++) {  //go through all files of the texpack 
                                if (comparePack.hasFileSet(j) && !newWorkPack.hasFileSet(j)) {  //if a compare pack has a missing file
                                    newWorkPack.setPathOfFile(j, comparePack);
                                    DebugLogMain("EBDLevelInternWork + " + "add file to pack: " + newWorkPack.getTexPackNamesSingleString() + " + file: " + newWorkPack.getRelativePathOfFile(j));
                                    fileAdded = true;
                                }
                            }
//                            }
                            if (fileAdded) {
                                intern.add(newWorkPack);
                            }
                        }
                    }
                }

//                DebugLog("EBD", "internsize: " + Integer.toString(intern.size()));
//                this.removeDuplicate(intern);
//                DebugLog("EBD", "internsize afte dupremove: " + Integer.toString(intern.size()));
                for (SubPack subInt : intern) {
                    if (subInt.hasAllFilesSet()) {
                        finishedPacks.add(subInt);
                        DebugLogMain("EBDLevelFinal: " + "Pack added: " + subInt.getTexPackNames());
                        //DebugLog("EBDLevelFinal: " + "with Files: " + intern.get(i).getRelativePathAllFilesSingleString());
                    } else {
                        isFinished = false;
                    }
                }

                if (!isFinished) {
                    DebugLogMain("RecursCalled");
                    intern.removeAll(finishedPacks);
                    this.setupSubPacksRecursiv(intern, comparePackList);
                }

            }

            class SubPack {

                private final String SubPackFolderName;
                private Integer NoOfMaxFiles;
                private LinkedHashSet<String> TexPackNames;
                private String PackRelPath;
                private Boolean[] isFileSet;
                private Boolean[] isBaseFile; //files which are physically in the same folder as the original subpack
                private String[] FileRelPath;
                private String[] TextureFiles;
                private EBDEnums.TextureType TYPE;
                private FilenameFilter DDSFilter;
                private String Path;
                private String TexString = "\\textures\\";

                public SubPack(File TexPackFolder, EBDEnums.TextureType TXTYPE) {
                    this.SubPackFolderName = TexPackFolder.getName();
                    this.Path = TexPackFolder.getPath();
                    // somewhat convoluted method to get the relative path
                    this.PackRelPath = this.Path.substring(this.Path.indexOf(TexString) + TexString.length(), this.Path.length());
                    this.TYPE = TXTYPE;
                    this.TextureFiles = getTextureFiles(TYPE);
                    this.NoOfMaxFiles = this.TextureFiles.length;
                    this.isFileSet = new Boolean[NoOfMaxFiles];
                    this.isBaseFile = new Boolean[NoOfMaxFiles];
                    this.FileRelPath = new String[NoOfMaxFiles];
                    this.TexPackNames = new LinkedHashSet<>();
                    this.TexPackNames.add(this.SubPackFolderName);

                    switch (this.TYPE) {
                        case BODY:
                            DDSFilter = BodyDDSFilter;
                            break;
                        case HANDS:
                            DDSFilter = HandsDDSFilter;
                            break;
                        case FACE:
                            DDSFilter = HeadDDSFilter;
                            break;

                    }
                    for (int i = 0; i < NoOfMaxFiles; i++) {
                        this.FileRelPath[i] = this.PackRelPath + "\\" + this.TextureFiles[i];
                        for (String list : TexPackFolder.list(DDSFilter)) {
                            if (this.TextureFiles[i].equalsIgnoreCase(list)) {
                                this.isFileSet[i] = true;
                                this.isBaseFile[i] = true;
                                break;
                            } else {
                                this.isFileSet[i] = false;
                                this.isBaseFile[i] = false;
                            }
                        }
                    }
                }

                public String getSubPackFolderName() {
                    return this.SubPackFolderName;
                }
                
                
                //check if any of the base files intersect meaning these two packs are not compatible
                public boolean hasIntersectingBaseFiles(SubPack comparePack) {
                    for (int i = 0; i < NoOfMaxFiles; i++) {
                        if (this.isBaseFile[i] && comparePack.isBaseFile[i]) {
                            return true;
                        }
                    }
                    return false;
                }
              

                @Override
                public boolean equals(Object obj) {
                    if (obj == null) {
                        return false;
                    }
                    if (getClass() != obj.getClass()) {
                        return false;
                    }
                    final SubPack other = (SubPack) obj;
                    if (!Arrays.deepEquals(this.FileRelPath, other.FileRelPath)) {
                        return false;
                    }
                    return this.TYPE == other.TYPE;
                }

                @Override
                public int hashCode() {
                    int hash = 7;
                    hash = 29 * hash + Objects.hashCode(this.NoOfMaxFiles);
                    hash = 29 * hash + Arrays.deepHashCode(this.FileRelPath);
                    hash = 29 * hash + Objects.hashCode(this.TYPE.toString());
                    return hash;
                }

                public EBDEnums.TextureType getSubPackType() {
                    return this.TYPE;
                }

                public SubPack(SubPack oldPack) {
                    this.TYPE = oldPack.TYPE;
                    this.DDSFilter = oldPack.DDSFilter;
                    this.TextureFiles = oldPack.TextureFiles;
                    this.SubPackFolderName = oldPack.SubPackFolderName;
                    this.NoOfMaxFiles = oldPack.NoOfMaxFiles;
                    this.TexPackNames = new LinkedHashSet<>(oldPack.TexPackNames);
                    this.PackRelPath = oldPack.PackRelPath;
                    this.FileRelPath = Arrays.copyOf(oldPack.FileRelPath, oldPack.FileRelPath.length);
                    this.isBaseFile = Arrays.copyOf(oldPack.isBaseFile, oldPack.isBaseFile.length);
                    this.isFileSet = Arrays.copyOf(oldPack.isFileSet, oldPack.isFileSet.length);
                }

                public void setPathOfFile(int NoOfFile, SubPack newPack) {
                    if (newPack.hasFileSet(NoOfFile)) {
                        this.FileRelPath[NoOfFile] = newPack.getRelativePathOfFile(NoOfFile);
                        this.isFileSet[NoOfFile] = true;
                        this.TexPackNames.add(newPack.getSubPackFolderName());
                    }
                }

                public boolean SubHasMeshFile(int NoOfFile) {
                    return hasMeshFile(NoOfFile);
                }

                public String SubGetMeshFile(int NoOfFile) {
                    return getMeshFile(NoOfFile);
                }

                public void setPathOfFile(int NoOfFile, String newPath) {
                    this.FileRelPath[NoOfFile] = newPath;
                    this.isFileSet[NoOfFile] = true;
                }

                public boolean hasAllFilesSet() {
                    int cnt = 0;
                    for (Boolean fileSet : this.isFileSet) {
                        if (fileSet) {
                            cnt++;
                        }
                    }
                    return (cnt == this.isFileSet.length);
                }

                public String getFileName(int NoOfFile) {
                    return this.TextureFiles[NoOfFile];
                }

                public boolean hasFile(int NoOfFile) {
                    return !this.FileRelPath[NoOfFile].equals(FILE_NOT_FOUND);
                }

                public String[] getFileNames() {
                    return this.TextureFiles;
                }

                public LinkedHashSet<String> getTexPackNames() {
                    return this.TexPackNames;
                }

                public String getTexPackNamesSingleString() {
                    String retString = "";
                    for (String s : this.TexPackNames) {
                        retString = retString + s + ", ";
                    }
                    return retString;
                }

                public int getNumberOfMaxFiles() {
                    return this.NoOfMaxFiles;
                }

                public int getNumberOfFiles() {
                    int cnt = 0;
                    for (Boolean tmpBool : this.isFileSet) {
                        if (tmpBool) {
                            cnt++;
                        }
                    }
                    return cnt;
                }

                public String getRelativePathOfPack() {
                    return this.PackRelPath;
                }

                public String getRelativePathOfFile(int NoOfFile) {
                    return this.FileRelPath[NoOfFile];
                }

                public String getRelativePathAllFilesSingleString() {
                    String retString = "";
                    for (int i = 0; i < this.NoOfMaxFiles; i++) {
                        if (this.isFileSet[i]) {
                            retString = retString + this.FileRelPath[i] + ", ";
                        }
                    }
                    return retString;
                }

                public String[] getRelativePathOfAllFiles() {
                    String[] tmp = new String[this.isFileSet.length];
                    for (int i = 0; i < tmp.length; i++) {
                        if (this.isFileSet[i]) {
                            tmp[i] = this.getRelativePathOfFile(i);

                        } else {
                            tmp[i] = FILE_NOT_FOUND;
                        }
                    }
                    return tmp;
                }

                public boolean hasFileSet(int FileNumber) {
                    return this.isFileSet[FileNumber];
                }
            }
        }
    }

    public boolean hasMeshMainPack(String MainPack, EBDEnums.TextureType TxType) {
        TexturePack.MainPack MPack = this.MasterTexPack.getMainPack(MainPack);
        if (MPack != null) {
            return MPack.hasMeshFile(TxType);
        } else {
            return false;
        }
    }

    public String getMeshFileByMainPack(String MainPack, EBDEnums.TextureType TxType) {
        TexturePack.MainPack MPack = this.MasterTexPack.getMainPack(MainPack);
        if (MPack != null) {
            return MPack.getMeshFile(TxType);
        } else {
            return this.MasterTexPack.getMissingFileString();
        }
    }

    public HashMap<String, ArrayList<String>> getMainPacksWithSubPacks() {
        HashMap<String, ArrayList<String>> tmpList = new HashMap<>();
        this.MasterTexPack.getAllMainPacksNames().stream().forEach((MainPack) -> {
            tmpList.put(MainPack, this.MasterTexPack.getSubPackFolderListByMainPack(MainPack));
        });
        return tmpList;
    }

    public LinkedHashSet<TexturePack.MainPack.SubPack> getCustomSubPacksForMainPack(String MainPack, ArrayList<String> FolderNames, EBDEnums.TextureType TYPE) {
        TexturePack.MainPack MPack = this.MasterTexPack.getMainPack(MainPack);
        if (MPack != null) {
            return MPack.getCustomSubPacks(FolderNames, TYPE);
        } else {
            return new LinkedHashSet<>();
        }
    }

    public boolean isSubPackType(String MainPack, String SubPack, EBDEnums.TextureType TYPE) {
        TexturePack.MainPack MPack = this.MasterTexPack.getMainPack(MainPack);
        if (MPack != null) {
            if (MPack.getSubPacks(TYPE).stream().anyMatch((SPack) -> (SPack.getSubPackFolderName().equalsIgnoreCase(SubPack)))) {
                return true;
            }
        }
        return false;
    }
    
    // check if two subpacks of different type have intersecting base files
    // this done to see
    public boolean hasIntersectingBaseFiles(String MainPack, String SubPackFolder1, String SubPackFolder2, EBDEnums.TextureType TYPE) {
        TexturePack.MainPack MPack = this.MasterTexPack.getMainPack(MainPack);
        if (MPack != null) {
            TexturePack.MainPack.SubPack SPack1 = null;
            TexturePack.MainPack.SubPack SPack2 = null;
            boolean f1 = false;
            boolean f2 = false;
            for (TexturePack.MainPack.SubPack SPack: MPack.getSubPacks(TYPE)) {
                if (SPack.getSubPackFolderName().equalsIgnoreCase(SubPackFolder1)) {
                    SPack1 = SPack;
                    f1 = true;
                }
                else if (SPack.getSubPackFolderName().equalsIgnoreCase(SubPackFolder2)) {
                    SPack2 = SPack;
                    f2 = true;
                }
                if (f1 && f2) {
                    break;
                }                
            }
            if (SPack1 != null && SPack2 != null) {
                return SPack1.hasIntersectingBaseFiles(SPack2);
            }
        }
        return false;
    }

    public boolean isValid() {
        return this.MasterTexPack != null;
    }

    public int getNumberOfFiles() {
        return this.NoOfFiles;
    }
}
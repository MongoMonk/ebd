package EBD;

import EBD.EBDSaveFile.Settings;
import EBD.EBDUtilityFuncs.npcFuncs;
import EBD.gui.HeadSettingsPanel;
import EBD.gui.HeightSettingsPanel;
import EBD.gui.MainSettingsPanel;
import EBD.gui.OtherSettingsPanel;
import EBD.gui.TextureMeshSettingsPanel;
import EBD.gui.WelcomePanel;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import lev.gui.LSaveFile;
import skyproc.*;
import skyproc.exceptions.BadMod;
import skyproc.exceptions.MissingMaster;
import skyproc.genenums.ActorValue;
import skyproc.genenums.Gender;
import skyproc.genenums.SoundVolume;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPProgressBarPlug;
import skyproc.gui.SUM;
import skyproc.gui.SUMGUI;

/**
 *
 * @author MongoMonk
 */
public class EBDMain implements SUM {

    /*
     * The important functions to change are:
     * - getStandardMenu(), where you set up the GUI
     * - runChangesToPatch(), where you put all the processing code and add records to the output patch.
     */

 /*
     * The types of records you want your patcher to import. Change this to
     * customize the import to what you need.
     */
    GRUP_TYPE[] importRequests = new GRUP_TYPE[]{
        GRUP_TYPE.NPC_,
        GRUP_TYPE.RACE,
        GRUP_TYPE.TXST,
        GRUP_TYPE.ARMA,
        GRUP_TYPE.ARMO,
        GRUP_TYPE.HDPT,
        GRUP_TYPE.FLST};
    public static String myPatchName = "EveryBody's Different";
    public static String authorName = "MongoMonk";
    public static String version = "3.45 " + SPGlobal.gameName;
    public static int versionInt = 345;
    public static String welcomeText = "This patcher diversifies NPCs in numerous ways.\n\n"
            + "For questions refer to the Readme.";
    public static String descriptionToShowInSUM = "Diversifies NPCs on a random basis.";
    public static Color headerColor = new Color(255, 200, 0);  // Yellow/Orange
    public static Color settingsColor = new Color(40, 140, 255);  // Lightblue
    public static Font settingsFont = new Font("Serif", Font.BOLD, 15);
    public static SkyProcSave save = new EBDSaveFile();
    public static EBDRandomFunctions rnd = new EBDRandomFunctions();
    public static KYWD processedByPatcherKYD;
    public static KYWD validScriptRaceKYD;
    public static KYWD processFaceKYD;
    public static KYWD validHeadPartKYD;
    public static Mod merger;
    public static Mod patch;

    public static EBDXMLHeadPartParser XMLHeadPartParser;

    public static enum EBDLOGS {

        TEXTUREHASHES,
        FACEGEN,
        SUMMARY
    }

    // Do not write the bulk of your program here
    // Instead, write your patch changes in the "runChangesToPatch" function
    // at the bottom
    public static void main(String[] args) {
        try {
            SPGlobal.createGlobalLog();
            SUMGUI.open(new EBDMain(), args);
        } catch (Exception e) {
            // If a major error happens, print it everywhere and display a message box.
            System.err.println(e.toString());
            SPGlobal.logException(e);
            JOptionPane.showMessageDialog(null, "There was an exception thrown during program execution: '" + e + "'  Check the debug logs or contact the author.");
            SPGlobal.closeDebug();
        }
    }

    @Override
    public String getName() {
        return myPatchName;

    }

    // This function labels any record types that you "multiply".
    // For example, if you took all the armors in a mod list and made 3 copies,
    // you would put ARMO here.
    // This is to help monitor/prevent issues where multiple SkyProc patchers
    // multiply the same record type to yeild a huge number of records.
    @Override
    public GRUP_TYPE[] dangerousRecordReport() {
        // None
        return new GRUP_TYPE[]{GRUP_TYPE.ARMO, GRUP_TYPE.ARMA, GRUP_TYPE.TXST};
    }

    @Override
    public GRUP_TYPE[] importRequests() {
        return importRequests;
    }

    @Override
    public boolean importAtStart() {
        return EBDSettings.isImportAtStartupEnabled();
    }

    @Override
    public boolean hasStandardMenu() {
        return true;
    }

    // This is where you add panels to the main menu.
    // First create custom panel classes (as shown by YourFirstSettingsPanel),
    // Then add them here.
    @Override
    public SPMainMenuPanel getStandardMenu() {
        SPMainMenuPanel settingsMenu = new SPMainMenuPanel(getHeaderColor());

        settingsMenu.setWelcomePanel(new WelcomePanel(settingsMenu));

        settingsMenu.addMenu(new MainSettingsPanel(settingsMenu), false, save, Settings.MAIN_SETTINGS);
        settingsMenu.addMenu(new HeadSettingsPanel(settingsMenu), false, save, Settings.HEAD_SETTINGS);
        settingsMenu.addMenu(new HeightSettingsPanel(settingsMenu), false, save, Settings.HEIGHT_SETTINGS);
        settingsMenu.addMenu(new TextureMeshSettingsPanel(settingsMenu), false, save, Settings.TEXTURE_MESH_SETTINGS);
        settingsMenu.addMenu(new OtherSettingsPanel(settingsMenu), false, save, Settings.OTHER_SETTINGS);

        return settingsMenu;
    }

    // Usually false unless you want to make your own GUI
    @Override
    public boolean hasCustomMenu() {
        return false;
    }

    @Override
    public JFrame openCustomMenu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasLogo() {
        return false;
    }

    @Override
    public URL getLogo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasSave() {
        return true;
    }

    @Override
    public LSaveFile getSave() {
        return save;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public ModListing getListing() {
        return new ModListing(getName(), false);
    }

    @Override
    public Mod getExportPatch() {
        Mod out = new Mod(getListing());
        out.setAuthor(authorName);
        out.setDescription("Everybody's Different: skyproc patcher for diversifying NPCs. Patch Version: " + version);
        return out;
    }

    @Override
    public Color getHeaderColor() {
        return headerColor;
    }

    // Add any custom checks to determine if a patch is needed.
    // On Automatic Variants, this function would check if any new packages were
    // added or removed.
    @Override
    public boolean needsPatching() {
        return true; //return true in case a user has modified any of the numerous external files & settings EBD uses
    }

    // This function runs when the program opens to "set things up"
    // It runs right after the save file is loaded, and before the GUI is displayed
    @Override
    public void onStart() throws Exception {
        SPGlobal.newSpecialLog(EBDLOGS.SUMMARY, "EBD Summary.txt");
        SPGlobal.newSpecialLog(EBDLOGS.FACEGEN, "EBD_Missing_FaceGen.txt");
        SPGlobal.newSpecialLog(EBDLOGS.TEXTUREHASHES, "Texture_Hashes.txt");
        this.Maintenance();
    }

    // This function runs right as the program is about to close.
    @Override
    public void onExit(boolean patchWasGenerated) throws Exception {
        //  System.out.println((EBDSettings.HeightSettings.getFemaleHeightOffset()));
    }

    // Add any mods that you REQUIRE to be present in order to patch.
    @Override
    public ArrayList<ModListing> requiredMods() {
        return new ArrayList<>(0);
    }

    @Override
    public String description() {
        return descriptionToShowInSUM;
    }

    void log(String... aMessage) {
        System.out.println(Arrays.toString(aMessage));
        SPGlobal.log("EDBMessage", aMessage);
        //     JOptionPane.showMessageDialog(null, aMessage, "Test Titel", JOptionPane.OK_CANCEL_OPTION);       
    }

    void DebugLogMain(String message) {
        if (EBDSettings.isDebugEnabled()) {
            SPGlobal.log(this.getClass().getSimpleName(), message);
        }
    }

    void DebugLogSummary(String message) {
        SPGlobal.logSpecial(EBDLOGS.SUMMARY, this.getClass().getSimpleName(), message);
        DebugLogMain(message);
    }

    void DebugLogFaceGen(String message) {
        SPGlobal.logSpecial(EBDLOGS.FACEGEN, this.getClass().getSimpleName(), message);
    }

    void Maintenance() {
        ArrayList<String> subList = new ArrayList<>();
        subList.add("Female");
        subList.add("Male");
        subList.add("MaleKhajiit");
        subList.add("MaleArgonian");
        subList.add("FemaleKhajiit");
        subList.add("FemaleArgonian");
        ArrayList<String> mainList = new ArrayList<>();
        String ebd = "\\EBD\\";
        mainList.add(SPGlobal.pathToData + "\\SkyProc Patchers" + ebd + "TexturePackXmlFiles\\");
        mainList.add(SPGlobal.pathToData + "\\Meshes\\" + ebd);
        mainList.add(SPGlobal.pathToData + "\\Textures\\" + ebd);

        String JsonEx = SPGlobal.pathToData + "\\SkyProc Patchers" + ebd + "JsonExchange";
        String consis = SPGlobal.pathToData + "\\SkyProc Patchers" + ebd + "Consistency";
        String xmlSettings = SPGlobal.pathToData + "\\SkyProc Patchers" + ebd + "DynamicSettings";

        createFolder(consis);
        createFolder(xmlSettings);
        createFolder(JsonEx);
        mainList.stream().forEach((main) -> {
            subList.stream().forEach((sub) -> {
                createFolder(main + sub);
            });
        });

        ArrayList<File> oldFiles = new ArrayList<>();
        oldFiles.add(new File(SPGlobal.pathToData + "\\scripts\\EBDQuestScript.pex"));
        oldFiles.add(new File(SPGlobal.pathToData + "\\scripts\\EBDExcludeNPCScript.pex"));
        oldFiles.add(new File(SPGlobal.pathToData + "\\scripts\\EBDNewLooksScript.pex"));
        oldFiles.add(new File(SPGlobal.pathToData + "\\scripts\\EBDTestScript.pex"));
        for (File oldFile : oldFiles) {
            if (oldFile.exists()) {
                DebugLogSummary("Found old file: " + oldFile.getAbsolutePath() + ". Removing.");
                oldFile.delete();
            }
        }
    }

    private void createFolder(String folder) {
        File f = new File(folder);
        if (!f.exists()) {
            f.mkdirs();
            DebugLogSummary("Create Missing Folder: " + f.getAbsolutePath());
        }
    }

    static class EBDSettings {

        public static boolean isImportAtStartupEnabled() {

            return save.getBool(Settings.IMPORT_AT_START);
        }

        public static boolean isAddAllModsAsMasterEnabled() {

            return save.getBool(Settings.ALL_MODS_MASTERS);
        }

        public static boolean isBodyHeightEnabled() {
            return save.getBool(Settings.BODY_HEIGHT);
        }

        public static boolean isBodyTexturesEnabled() {
            return save.getBool(Settings.BODY_TEXTURES_MESHES);
        }

        public static boolean isBodyTexturesOverwriteCustomEnabled() {
            return save.getBool(Settings.BODY_TEX_OVERWRITE_CUSTOM);
        }

        public static boolean isBodyTexturesFemaleEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_FEMALE);
        }

        public static boolean isBodyTexturesMaleEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_MALE);
        }

        public static boolean isBodyTexturesFemaleKhajiitEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_FEMALE_KHAJIIT);
        }

        public static boolean isBodyTexturesMaleKhajiitEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_MALE_KHAJIIT);
        }

        public static boolean isBodyTexturesFemaleArgonianEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_FEMALE_ARGONIAN);
        }

        public static boolean isBodyTexturesMaleArgonianEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_MALE_ARGONIAN);
        }

        public static boolean isBodyTexturesDisableUniquesEnabled() {
            return save.getBool(Settings.BODY_TEX_MESH_DISABLE_UNIQUES);
        }

        public static boolean isFemaleAnimationsEnabled() {
            return save.getBool(Settings.ANIM_FEMALE);
        }

        public static boolean isDebugEnabled() {
            return save.getBool(Settings.DEBUG);
        }

        //      public static boolean isDebugScriptEnabled() {
        //          return save.getBool(Settings.DEBUG_SCRIPT);
        //      }
        public static boolean isHelperScriptEnabled() {
            return EBDSettings.isBodyTexturesEnabled() || EBDSettings.isHeadEnabled();
        }

        public static boolean isFaceComplexSliderEnabled() {
            return save.getBool(Settings.FACE_TEX_COMP_SLIDER);
        }

        public static boolean isHeadPartEnabled(EBDEnums.Gender GEN, EBDEnums.Headpart HDPT) {
            return XMLHeadPartParser.isActive(GEN, HDPT);
        }

        public static boolean isUIExtensionsInstalled() {
            //Enderal brings its own UIExtensions
            if (EBDMain.isEnderal()) {
                return true;
            }
            ModListing UIExListing = new ModListing("UIExtensions", false);
            try {
                return SPImporter.getActiveModList().contains(UIExListing);
            } catch (IOException ex) {
                Logger.getLogger(EBDMain.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }

        public static boolean isHeadPartEnabled(EBDEnums.Gender GEN) {
            return XMLHeadPartParser.isActive(GEN);
        }

        public static boolean isHeadDisableUniquesEnabled() {
            return save.getBool(Settings.HEAD_DISABLE_UNIQUES);
        }

        public static boolean isHeadDisableElderEnabled() {
            return save.getBool(Settings.HEAD_DISABLE_ELDER);
        }

        public static boolean isHeadDisableInCombatEnabled() {
            return save.getBool(Settings.HEAD_DISABLE_IN_COMBAT);
        }

        public static boolean isHeadDisableVanillaPartsEnabled(EBDEnums.Gender GEN, EBDEnums.Headpart HDPT) {
            return XMLHeadPartParser.isVanillaDisabled(GEN, HDPT);
        }

        public static boolean isHeadEnabled() {
            return save.getBool(Settings.BODY_HEAD);
        }

        public static boolean isHeadSpellsEnabled() {
            return save.getBool(Settings.HEAD_SPELLS);
        }

        public static boolean isHeadSpellsDebugEnabled() {
            return save.getBool(Settings.HEAD_SPELLS_DEBUG);
        }

        public static boolean isConsistencyEnabled() {
            return save.getBool(Settings.CONSISTENCY);
        }

        public static int getHeadChangeProbab(EBDEnums.Gender GEN, EBDEnums.Headpart HDPT) {
            return XMLHeadPartParser.getProbability(GEN, HDPT);
        }

        static class HeightSettings {

            public static boolean isHeightDisableUniquesEnabled() {
                return save.getBool(Settings.HEIGHT_DISABLE_UNIQUES);
            }

            public static int getFemaleHeightOffset() {
                return (save.getInt(Settings.HEIGHT_FEMALE_OFFSET));
            }

            public static int getNordMin() {
                return save.getInt(Settings.HEIGHT_Nord_MIN);
            }

            public static int getNordMax() {
                return save.getInt(Settings.HEIGHT_Nord_MAX);
            }

            public static int getBretonMin() {
                return save.getInt(Settings.HEIGHT_Breton_MIN);
            }

            public static int getBretonMax() {
                return save.getInt(Settings.HEIGHT_Breton_MAX);
            }

            public static int getImperialMin() {
                return save.getInt(Settings.HEIGHT_Imperial_MIN);
            }

            public static int getImperialMax() {
                return save.getInt(Settings.HEIGHT_Imperial_MAX);
            }

            public static int getRedguardMin() {
                return save.getInt(Settings.HEIGHT_Redguard_MIN);
            }

            public static int getRedguardMax() {
                return save.getInt(Settings.HEIGHT_Redguard_MAX);
            }

            public static int getOrcMin() {
                return save.getInt(Settings.HEIGHT_Orc_MIN);
            }

            public static int getOrcMax() {
                return save.getInt(Settings.HEIGHT_Orc_MAX);
            }

            public static int getHighElfMin() {
                return save.getInt(Settings.HEIGHT_HighElf_MIN);
            }

            public static int getHighElfMax() {
                return save.getInt(Settings.HEIGHT_HighElf_MAX);
            }

            public static int getWoodElfMin() {
                return save.getInt(Settings.HEIGHT_WoodElf_MIN);
            }

            public static int getWoodElfMax() {
                return save.getInt(Settings.HEIGHT_WoodElf_MAX);
            }

            public static int getDarkElfMin() {
                return save.getInt(Settings.HEIGHT_DarkElf_MIN);
            }

            public static int getDarkElfMax() {
                return save.getInt(Settings.HEIGHT_DarkElf_MAX);
            }

            public static int getArgonianMin() {
                return save.getInt(Settings.HEIGHT_Argonian_MIN);
            }

            public static int getArgonianMax() {
                return save.getInt(Settings.HEIGHT_Argonian_MAX);
            }

            public static int getKhajiitMin() {
                return save.getInt(Settings.HEIGHT_Khajiit_MIN);
            }

            public static int getKhajiitMax() {
                return save.getInt(Settings.HEIGHT_Khajiit_MAX);
            }

            public static int getElderMin() {
                return save.getInt(Settings.HEIGHT_Elder_MIN);
            }

            public static int getElderMax() {
                return save.getInt(Settings.HEIGHT_Elder_MAX);
            }

            public static int getOtherMin() {
                return save.getInt(Settings.HEIGHT_Other_MIN);
            }

            public static int getOtherMax() {
                return save.getInt(Settings.HEIGHT_Other_MAX);
            }
        }
    }

    void setupHeadparts(ScriptRef... scripts) {
        String Gender = "";
        String Part = "";
        int iPapyrusType = -1;

        for (EBDEnums.Gender GEN : EBDEnums.Gender.values()) {

            switch (GEN) {
                case FEMALE:
                    Gender = "Female";
                    break;
                case MALE:
                    Gender = "Male";

                    break;
            }
            FLST HeadPartList = new FLST("EBDPartListContainer" + Gender);

            GLOB isHeadPartEnabled = new GLOB("EBDHeadPart" + Gender + "Enabled", skyproc.GLOB.GLOBType.Short);
            isHeadPartEnabled.setValue((EBDSettings.isHeadPartEnabled(GEN)));
            isHeadPartEnabled.setConstant(true);

            HeadPartList.addFormEntry(isHeadPartEnabled.getForm());

            hdpt_loop:
            for (EBDEnums.Headpart HEADPART : EBDEnums.Headpart.values()) {

                EBDHeadPack HeadPack = new EBDHeadPack(merger, GEN, HEADPART, EBDSettings.isHeadDisableVanillaPartsEnabled(GEN, HEADPART));

                /* Papyrus Headpart types
                 int Property Type_Misc = 0 AutoReadOnly
                 int Property Type_Face = 1 AutoReadOnly
                 int Property Type_Eyes = 2 AutoReadOnly
                 int Property Type_Hair = 3 AutoReadOnly
                 int Property Type_FacialHair = 4 AutoReadOnly
                 int Property Type_Scar = 5 AutoReadOnly
                 int Property Type_Brows = 6 AutoReadOnly */
                switch (HEADPART) {
                    case HAIR:
                        Part = "Hair";
                        iPapyrusType = 3;
                        break;
                    case EYES:
                        Part = "Eyes";
                        iPapyrusType = 2;
                        break;
                    case SCARS:
                        Part = "Scars";
                        iPapyrusType = 5;
                        break;
                    case BROWS:
                        Part = "Brows";
                        iPapyrusType = 6;
                        break;
                    case BEARD:
                        Part = "Beards";
                        iPapyrusType = 4;
                        //if (GEN == EBDEnums.Gender.FEMALE) {
                        //no beards for females, sorry
                        //break hdpt_loop;
                        //}
                        break;
                }

                FLST headRaceListContainer = new FLST("EBDRaceListContainer" + Gender + Part);

                GLOB iPartType = new GLOB("EBD" + Gender + Part + "Type", skyproc.GLOB.GLOBType.Short);
                iPartType.setValue((float) iPapyrusType);
                iPartType.setConstant(true);

                GLOB isPartEnabled = new GLOB("EBD" + Gender + Part + "Enabled", skyproc.GLOB.GLOBType.Short);
                //has to be enabled and the patcher has to find some actual entries
                if (EBDSettings.isHeadPartEnabled(GEN, HEADPART) && !HeadPack.getAllRaces().isEmpty()) {
                    isPartEnabled.setValue(true);
                } else {
                    isPartEnabled.setValue(false);
                }
                isPartEnabled.setConstant(true);

                GLOB iPartProbab = new GLOB("EBD" + Gender + Part + "Probability", skyproc.GLOB.GLOBType.Short);
                iPartProbab.setValue((float) (EBDSettings.getHeadChangeProbab(GEN, HEADPART)));
                iPartProbab.setConstant(true);

                if (EBDSettings.isHeadPartEnabled(GEN, HEADPART)) {

                    for (FormID raceID : HeadPack.getAllRaces()) {

                        RACE raceRC = (RACE) merger.getMajor(raceID, GRUP_TYPE.RACE);
                        FLST headPartList = new FLST("EBD" + Gender + Part + raceRC.getEDID());
                        headPartList.addAll(HeadPack.getHeadPartsByRace(raceID));

                        FLST headRaceList = new FLST("EBDRaceList" + Gender + Part + raceRC.getEDID());
                        headRaceList.addFormEntry(raceID);
                        headRaceList.addFormEntry(headPartList.getForm());
                        headRaceListContainer.addFormEntry(headRaceList.getForm());
                    }
                }

                FLST headPartListContainer = new FLST("EBDPartListContainer" + Gender + Part);

                headPartListContainer.addFormEntry(isPartEnabled.getForm());
                headPartListContainer.addFormEntry(iPartType.getForm());
                headPartListContainer.addFormEntry(iPartProbab.getForm());
                headPartListContainer.addFormEntry(headRaceListContainer.getForm());
                HeadPartList.addFormEntry(headPartListContainer.getForm());

            }

            for (ScriptRef script : scripts) {
                script.setProperty("HeadPartList" + Gender, HeadPartList.getForm());
            }
        }
        for (ScriptRef script : scripts) {
            if (!script.getProperties().contains("EBDHeadPartKeyWord")) {
                script.setProperty("EBDHeadPartKeyWord", validHeadPartKYD.getForm());
            }
        }
    }

    void setupMCM_2() {
        ModListing skyUIModListing = new ModListing("SkyUI", false);
        Mod skyUIMod = null;
        try {
            skyUIMod = SPImporter.importMod(skyUIModListing, SPGlobal.pathToData, GRUP_TYPE.QUST);
        } catch (BadMod | MissingMaster ex) {
            DebugLogMain("Error. SkyUI.esp not found: " + ex);
            JOptionPane.showMessageDialog(null, "SkyUI.esp not found. Patcher will be terminated: Exception: '" + ex);
            System.exit(0);
        }
        ScriptRef MCMScript = new ScriptRef("EBDConfigMenu");
        QUST qu = (QUST) skyUIMod.getMajor(new FormID("000814", "SkyUI.esp"), GRUP_TYPE.QUST);
        QUST newQu = (QUST) patch.makeCopy(qu, "EBDConfigMenuQuest");
        newQu.setName("EBDConfigMenuQuest");
        newQu.getScriptPackage().addScript(MCMScript);
    }

//    private void setEBDQuestProperty(ScriptRef SR) {
//        SR.setProperty("EBDQuest", EBDQuest.getForm());
//    }
    void setupHelperScript() {

        ScriptRef HelperScript = new ScriptRef("EBDHelperScript");
        FormID PlayerREFID = new FormID("000014", "Skyrim.esm");
        HelperScript.setProperty("PlayerREF", PlayerREFID);
        HelperScript.setProperty("EBDHeadPartKeyWord", validHeadPartKYD.getForm());
        HelperScript.setProperty("EBDScriptKeyWord", validScriptRaceKYD.getForm());
        HelperScript.setProperty("EBDProcessFace", processFaceKYD.getForm());
        //this.setupMainScriptProperties(QuestScript);
        //EBDQuest = NiftyFunc.makeScriptQuest(QuestScript);

//        QUST qu = (QUST) skyrimMod.getMajor(new FormID("000E46", "Skyrim.esm"), GRUP_TYPE.QUST);
//        QUST newQu = (QUST) patch.makeCopy(qu, "EBDMainQuest");
//        newQu.setName("EBDMainQuest");
//        newQu.getConditions().clear();
//        newQu.getScriptPackage().addScript(QuestScript);
//        EBDQuest = newQu;
        GLOB isScriptEnabled = new GLOB("EBDHelperScriptEnabled", skyproc.GLOB.GLOBType.Short);
        isScriptEnabled.setValue(EBDSettings.isHelperScriptEnabled());
        isScriptEnabled.setConstant(true);

//        GLOB scriptDebugEnabled = new GLOB("EBDHelperScriptDebugEnabled", skyproc.GLOB.GLOBType.Short);
//        scriptDebugEnabled.setValue(EBDSettings.isDebugScriptEnabled());
//        scriptDebugEnabled.setConstant(true);
        GLOB isFaceEnabled = new GLOB("EBDFaceFixEnabled", skyproc.GLOB.GLOBType.Short);
        isFaceEnabled.setValue(EBDSettings.isBodyTexturesEnabled());
        isFaceEnabled.setConstant(true);

        GLOB isHeadEnabled = new GLOB("EBDHeadEnabled", skyproc.GLOB.GLOBType.Short);
        isHeadEnabled.setValue(EBDSettings.isHeadEnabled());
        isHeadEnabled.setConstant(true);

        GLOB isHeadSpellsEnabled = new GLOB("EBDHeadSpellsEnabled", skyproc.GLOB.GLOBType.Short);
        isHeadSpellsEnabled.setValue(EBDSettings.isHeadSpellsEnabled() && EBDSettings.isUIExtensionsInstalled()); //both have to be enabled
        isHeadSpellsEnabled.setConstant(true);

        GLOB isHeadInCombatDisabled = new GLOB("EBDHeadInCombatDisabled", skyproc.GLOB.GLOBType.Short);
        isHeadInCombatDisabled.setValue(EBDSettings.isHeadDisableInCombatEnabled());
        isHeadInCombatDisabled.setConstant(true);

        HelperScript.setProperty("isHeadInCombatDisabled", isHeadInCombatDisabled.getForm());
        HelperScript.setProperty("isHeadSpellsEnabled", isHeadSpellsEnabled.getForm());
        HelperScript.setProperty("isScriptEnabled", isScriptEnabled.getForm());
//        FaceFixScript.setProperty("isDebugEnabled", scriptDebugEnabled.getForm());
        HelperScript.setProperty("isHeadEnabled", isHeadEnabled.getForm());

        if (EBDSettings.isHelperScriptEnabled()) {

            if (EBDSettings.isBodyTexturesEnabled() || EBDSettings.isHeadEnabled()) {

                //ScriptRef HelperRef = new ScriptRef("EBDHelperScript");
                SPEL addScriptSpell = NiftyFunc.genScriptAttachingSpel(HelperScript, "EBD");
                MGEF scripEffect = (MGEF) patch.getMajor(addScriptSpell.getMagicEffects().get(0).getMagicRef(), GRUP_TYPE.MGEF);
                scripEffect.set(MGEF.SpellEffectFlag.NoDeathDispel, true);
                HelperScript.setProperty("EBDHelperMagicEffect", addScriptSpell.getMagicEffects().get(0).getMagicRef());
                HelperScript.setProperty("EBDHelperSpell", addScriptSpell.getForm());

                //create spells
                if (EBDSettings.isHeadEnabled()) {
                    if (EBDSettings.isHeadSpellsEnabled()) {
                        if (EBDSettings.isUIExtensionsInstalled()) {
                            this.addUIExtensionsRequirement(patch);
                            SPEL SpCustomizer = this.setupEBDSpell("Customizer", "Changes the look of an NPC. The NPC will get a new hair style/eye colour. This concerns only the headpart feature.", true);
                            HelperScript.setProperty("EBDCustomizerSpell", SpCustomizer.getForm());

                        } else {
                            JOptionPane.showMessageDialog(null, "UIExtentions is not installed. Headspells won't be enabled.");
                            DebugLogSummary("UIExtentions is not installed although the Headspells are enabled. Please download & install UIExtensions to make the headspells work. More information in the readme.");
                        }
                    }
                    if (EBDSettings.isHeadSpellsDebugEnabled()) {
                        HelperScript.setProperty("EBDDebugSpell", this.setupEBDSpell("Debug", "Test spell. Cast to undress an NPC. Cast again to redress the NPC. Re-applies face tints. Prints information of set headparts.", false).getForm());
                    }
                }
                for (RACE race : merger.getRaces()) {
                    if (npcFuncs.isPlayableRace(race)) {
                        boolean hasSpell = false;
                        for (FormID id : race.getSpells()) {
                            if (id.equals(addScriptSpell.getForm())) {
                                hasSpell = true;
                                break;
                            }
                        }
                        if (hasSpell == false) {
                            race.getSpells().add(addScriptSpell.getForm());
                            patch.addRecord(race);
                        }
                    }
                }
            }
        }
    }

    public Mod getSkyrimMod() {
        ModListing skyrimModListing = new ModListing("Skyrim", true);
        Mod skyrimMod = null;
        try {
            skyrimMod = SPImporter.importMod(skyrimModListing, SPGlobal.pathToData, GRUP_TYPE.ARMO, GRUP_TYPE.ARMA, GRUP_TYPE.SPEL, GRUP_TYPE.MGEF, GRUP_TYPE.QUST);
        } catch (BadMod | MissingMaster ex) {
            DebugLogMain("Error. Skyrim.esm not found: " + ex);
            JOptionPane.showMessageDialog(null, SPGlobal.pathToData + "Skyrim.esm not found. Patcher will be terminated: Exception: '" + ex);
            System.exit(0);
        }
        return skyrimMod;
    }

    private static boolean isSkyrimSE() {
        // check specifically for SkyrimSE
        return (SPGlobal.gameName.equals("SkyrimSE"));
    }

    private static boolean isEnderal() {
        // check for Enderal in general
        ModListing enderalModListing = new ModListing("Enderal - Forgotten Stories", true);
        try {
            SPImporter.importMod(enderalModListing, SPGlobal.pathToData, GRUP_TYPE.ARMO);
            //DebugLogSummary("We are running Enderal");
            return true;
        } catch (BadMod | MissingMaster ex) {
            return false;
        }

    }

    public HashMap<ModListing, Mod> getModList() {
        HashMap<ModListing, Mod> modList = new HashMap<>();
        ArrayList<ModListing> list = new ArrayList<>();
        try {
            list = SPImporter.getActiveModList();
        } catch (IOException ex) {
            DebugLogMain("Error. Could not get active mod list. " + ex);
        }
        for (ModListing listing : list) {
            try {
                Mod newMod = SPImporter.importMod(listing, SPGlobal.pathToData, GRUP_TYPE.NPC_);
                modList.put(listing, newMod);
            } catch (BadMod | MissingMaster ex) {
                DebugLogMain("Error. " + listing + " not found: " + ex);
            }

        }
        return modList;
    }

    SPEL setupEBDSpell(String SpellName, String Desc, boolean isHeadpartScript) {
        FormID SP_ID = null;
        FormID MG_ID = null;
        if (EBDMain.isSkyrimSE()) {
            // Enderal SE
            if (EBDMain.isEnderal()) {
                SP_ID = new FormID("012FD2", "Skyrim.esm"); //heal other enderal: 02A00A
                MG_ID = new FormID("04DBA3", "Skyrim.esm"); //soultrap: 04DBA3 ; heal other enderal: 01CEA7
            } // Skyrim SE
            else {
                SP_ID = new FormID("028532", "Skyrim.esm"); //soultrap: 04dba4 ; fade other: 028532; paralysis enderal: 02A00A
                MG_ID = new FormID("01EA6B", "Skyrim.esm"); //soultrap: 04DBA3 ; fade other: 01EA6B; paralysis enderal: 09AA3F
            }
        //Skyrim LE or Enderal LE
        } else {
            SP_ID = new FormID("04dba4", "Skyrim.esm"); //soultrap: 04dba4 ; fade other: 028532
            MG_ID = new FormID("04DBA3", "Skyrim.esm"); //soultrap: 04DBA3 ; fade other: 01EA6B
        }

        //   FormID MG_SoultrapID = new FormID("04DBA3", "Skyrim.esm"); //to get shiny effects from
        FormID EquipVoiceID = new FormID("025BEE", "Skyrim.esm");
        ScriptRef NewScriptRef = new ScriptRef("EBD" + SpellName + "Script");
        if (isHeadpartScript) {
            NewScriptRef.setProperty("EBDHeadPartKeyWord", validHeadPartKYD.getForm());
        }
        // MGEF MGSoultrap = (MGEF) skyrimMod.getMajor(MG_SoultrapID, GRUP_TYPE.MGEF);
        Mod skyrimMod = this.getSkyrimMod();
        SPEL SPTemplate = (SPEL) skyrimMod.getMajor(SP_ID, GRUP_TYPE.SPEL);
        MGEF MGTemplate = (MGEF) skyrimMod.getMajor(MG_ID, GRUP_TYPE.MGEF);
        SPEL newSPEL = (SPEL) patch.makeCopy(SPTemplate, "EBD" + SpellName + "Spell");
        MGEF newMGEF = (MGEF) patch.makeCopy(MGTemplate, "EBD" + SpellName + "MGEF");

        newMGEF.setName("EBD" + SpellName + "Effect");
        newMGEF.set(MGEF.SpellEffectFlag.Hostile, false);
        newMGEF.set(MGEF.SpellEffectFlag.FXPersist, false);
        newMGEF.set(MGEF.SpellEffectFlag.Painless, true);
        newMGEF.set(MGEF.SpellEffectFlag.NoHitEvent, true);
        newMGEF.set(MGEF.SpellEffectFlag.NoDuration, true);
        newMGEF.set(MGEF.SpellEffectFlag.NoHitEffect, true);
        newMGEF.setSkillType(ActorValue.Illusion);
        // newMGEF.setHitEffectArt(MGSoultrap.getHitEffectArt());
        // newMGEF.setHitShader(MGSoultrap.getHitShader());
        //newMGEF.setProjectile(MGSoultrap.getProjectile());
        newMGEF.setSkillLevel(5);
        newMGEF.setSkillUsageMult(0);
        newMGEF.setSoundVolume(SoundVolume.Silent);
        newMGEF.setDescription(Desc);
        newMGEF.getConditions().clear();
        newMGEF.getScriptPackage().getScripts().clear();
        newMGEF.getScriptPackage().addScript(NewScriptRef);
        newSPEL.setName("EBD" + SpellName + "Spell");
        newSPEL.setBaseCost(0);
        newSPEL.setSpellType(SPEL.SPELType.LesserPower);
        newSPEL.setChargeTime((float) 0.15);
        //newLooksSPEL.setPerkRef(FormID.NULL);
        newSPEL.set(SPEL.SPELFlag.ManualCostCalculation, true);
        newSPEL.setEquipSlot(EquipVoiceID);
        newSPEL.getMagicEffects().clear();
        newSPEL.setChargeTime((float) 0.15);
        newSPEL.addMagicEffect(newMGEF);
        return newSPEL;
    }

    void setupNPCs() {
        boolean isNPCchanged;
        boolean isValidScript;
        boolean isValidHeadPart;
        boolean hasFaceGen;
        EBDXMLConsisParser consisParserHeight = null;
        String heightString = "Height";
        EBDTexturePack FemaleTX = null;
        EBDTexturePack MaleTX = null;
        EBDTexturePack FemaleKhajiitTX = null;
        EBDTexturePack MaleKhajiitTX = null;
        EBDTexturePack FemaleArgonianTX = null;
        EBDTexturePack MaleArgonianTX = null;
        HashMap<ModListing, Mod> ModList = this.getModList();

        int npcValidCount = 0;

        if (EBDSettings.isBodyHeightEnabled() && EBDSettings.isConsistencyEnabled()) {
            consisParserHeight = new EBDXMLConsisParser(EBDUtilityFuncs.npcFuncs.getHeightHash(), "Height", heightString);
            DebugLogSummary("Consistency is enabled for the Height feature. Hash has changed since the last run: " + consisParserHeight.isNewHash() + ". Entries: " + Integer.toString(consisParserHeight.getSize()));
        }

        if (EBDSettings.isFemaleAnimationsEnabled()) {
            merger.getRaces().forEach(race -> {
                if (EBDUtilityFuncs.npcFuncs.isPlayableRace(race)) {
                    try {
                        if (race.getPhysicsModel(Gender.FEMALE) != null) {
                            Model model = race.getPhysicsModel(Gender.FEMALE);
                            if (model.getFileName().equals("Actors\\Character\\DefaultMale.hkx")) {
                                model.setFileName("Actors\\Character\\DefaultFemale.hkx");
                                //if (race.getEDID().equals("OrcRace")) {
                                DebugLogSummary("Modifying " + race.getEDID() + " to have females use female animations.");
                            }
                        }
                    } catch (NullPointerException e) {
                        DebugLogSummary("Warning: Found race with null physics model: " + race.getEDID());
                    }
                }
            });
        }

        if (EBDSettings.isBodyTexturesEnabled()) {

            if (EBDSettings.isBodyTexturesFemaleEnabled()) {
                SPProgressBarPlug.setStatus("Creating Female Texture Packs");
                FemaleTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.FEMALE, EBDEnums.Race.HUMANOID);
            }
            if (EBDSettings.isBodyTexturesMaleEnabled()) {
                SPProgressBarPlug.setStatus("Creating Male Texture Packs");
                MaleTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.MALE, EBDEnums.Race.HUMANOID);
            }
            if (EBDSettings.isBodyTexturesFemaleKhajiitEnabled()) {
                SPProgressBarPlug.setStatus("Creating Female Khajiit Texture Packs");
                FemaleKhajiitTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.FEMALE, EBDEnums.Race.KHAJIIT);
            }
            if (EBDSettings.isBodyTexturesMaleKhajiitEnabled()) {
                SPProgressBarPlug.setStatus("Creating Male Khajiit Texture Packs");
                MaleKhajiitTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.MALE, EBDEnums.Race.KHAJIIT);
            }
            if (EBDSettings.isBodyTexturesFemaleArgonianEnabled()) {
                SPProgressBarPlug.setStatus("Creating Female Argonian Texture Packs");
                FemaleArgonianTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.FEMALE, EBDEnums.Race.ARGONIAN);
            }
            if (EBDSettings.isBodyTexturesMaleArgonianEnabled()) {
                SPProgressBarPlug.setStatus("Creating Male Argonian Texture Packs");
                MaleArgonianTX = new EBDTexturePack(merger, patch, EBDEnums.Gender.MALE, EBDEnums.Race.ARGONIAN);
            }
        }

        processedByPatcherKYD = new KYWD("PatchedByEBD");
        validScriptRaceKYD = new KYWD("EBDValidScriptRace");
        validHeadPartKYD = new KYWD("EBDValidHeadPartActor");
        processFaceKYD = new KYWD("EBDProcessFace");

        SPProgressBarPlug.setStatus("Set Up Helper Script");
        this.setupHelperScript();
        //this.setupMCM_2();

        SPProgressBarPlug.setStatus("Modify NPCs");
        for (NPC_ npc : merger.getNPCs()) {
            isNPCchanged = false;
            if (!npc.getRace().isNull()) {

                RACE npcRace = (RACE) merger.getMajor(npc.getRace(), GRUP_TYPE.RACE);
                if (npcFuncs.isValidNPC(npc, npcRace)) {
                    boolean bOutputFaceGen = EBDSettings.isDebugEnabled();
                    if (bOutputFaceGen) {
                        if (!npc.getName().equals("<NO TEXT>") && !npc.get(NPC_.NPCFlag.IsCharGenFacePreset)) {
                            hasFaceGen = false;
                            //String pathFaceGenNif = SPGlobal.pathToData + "meshes\\actors\\character\\facegendata\\facegeom\\" + npc.getFormMaster().print() + "\\00" + npc.getForm().getTitle().replace(npc.getFormMaster().print(), ".nif");
                            String pathFaceGenDds = SPGlobal.pathToData + "textures\\actors\\Character\\facegendata\\facetint\\" + npc.getFormMaster().print() + "\\00" + npc.getForm().getTitle().replace(npc.getFormMaster().print(), ".dds");
                            String pathBSAFaceGenDds = "textures\\actors\\Character\\facegendata\\facetint\\" + npc.getFormMaster().print() + "\\00" + npc.getForm().getTitle().replace(npc.getFormMaster().print(), ".dds");
                            File FileDds = new File(pathFaceGenDds);
                            if (FileDds.exists()) {
                                hasFaceGen = true;
                            }
                            if (!hasFaceGen) {
                                for (BSA bsa : skyproc.BSA.loadInBSAs(BSA.FileType.DDS)) {
                                    //DebugLogFaceGen(npc.getName() + " from " + bsa.getFilePath());
                                    if (bsa.hasFile(pathBSAFaceGenDds)) {
                                        hasFaceGen = true;
                                    }
                                }

                            }
                            if (!hasFaceGen) {
                                DebugLogFaceGen("Found no facegen files for: " + npc.getName() + "; " + npc.getFormStr());
                            }
                        }
                    }

                    isValidScript = true;
                    isValidHeadPart = true;

                    if (!EBDSettings.isHeadEnabled()) {
                        isValidHeadPart = false;
                    } else if (EBDSettings.isHeadDisableElderEnabled() && npcRace.getEDID().contains("ElderRace")) {
                        isValidHeadPart = false;
                    } else if (EBDSettings.isHeadDisableUniquesEnabled() && npc.get(NPC_.NPCFlag.Unique)) {
                        isValidHeadPart = false;
                    } else if (!EBDSettings.isHeadPartEnabled(EBDEnums.Gender.FEMALE) && npc.get(NPC_.NPCFlag.Female)) {
                        isValidHeadPart = false;
                    } else if (!EBDSettings.isHeadPartEnabled(EBDEnums.Gender.MALE) && !npc.get(NPC_.NPCFlag.Female)) {
                        isValidHeadPart = false;
                    }
                    if (EBDSettings.isBodyTexturesEnabled()) {
                        if (npc.get(NPC_.NPCFlag.Female)) {  //women
                            if (FemaleTX != null && FemaleTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeHuman(npc, npcRace)) {
                                    isNPCchanged = FemaleTX.setNPCTextures(npc);
                                }
                            }
                            if (FemaleKhajiitTX != null && FemaleKhajiitTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeKhajiit(npc, npcRace)) {
                                    isNPCchanged = FemaleKhajiitTX.setNPCTextures(npc);
                                }
                            }
                            if (FemaleArgonianTX != null && FemaleArgonianTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeArgonian(npc, npcRace)) {
                                    isNPCchanged = FemaleArgonianTX.setNPCTextures(npc);
                                }
                            }
                        } else if (!npc.get(NPC_.NPCFlag.Female)) { //men
                            if (MaleTX != null && MaleTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeHuman(npc, npcRace)) {
                                    isNPCchanged = MaleTX.setNPCTextures(npc);
                                }
                            }
                            if (MaleKhajiitTX != null && MaleKhajiitTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeKhajiit(npc, npcRace)) {
                                    isNPCchanged = MaleKhajiitTX.setNPCTextures(npc);
                                }
                            }
                            if (MaleArgonianTX != null && MaleArgonianTX.isValid()) {
                                if (npcFuncs.isValidNPCTexChangeArgonian(npc, npcRace)) {
                                    isNPCchanged = MaleArgonianTX.setNPCTextures(npc);
                                }
                            }
                        }
                    }

                    if (EBDSettings.isBodyHeightEnabled() && (npcFuncs.isValidNPCHeightChange(npc))) {   //only change NPC height if it is at the default value of 1.0. This means that already height altered NPCs are not changed
                        float consisHeight = -1;
                        if (consisParserHeight != null && !consisParserHeight.isNewHash()) {
                            String conString = consisParserHeight.getNPCValue(npc.getFormStr(), heightString);
                            if (conString != null && !conString.isEmpty()) {
                                consisHeight = Float.parseFloat(conString);
                            }
                        }
                        if (consisHeight != -1) {
                            npc.setHeight(consisHeight);
                        } else {
                            float height = npcFuncs.getNPCHeightByRace(npcRace, npc);
                            npc.setHeight(height);
                            if (consisParserHeight != null) {
                                consisParserHeight.addNPC(npc.getFormStr(), heightString, Float.toString(height));
                            }
                        }
                        isNPCchanged = true;
                    }

                    if (!EBDSettings.isHeadEnabled() && !EBDSettings.isBodyTexturesEnabled()) {
                        isValidScript = false;
                    }

                    if (npc.get(NPC_.NPCFlag.Female) && EBDSettings.isFemaleAnimationsEnabled()) {
                        if (npc.get(NPC_.NPCFlag.OppositeGenderAnims)) {
                            npc.set(NPC_.NPCFlag.OppositeGenderAnims, false);
                            isNPCchanged = true;
                        }
                    }

                    if (isValidScript) {
                        npc.getKeywordSet().addKeywordRef(validScriptRaceKYD.getForm());
                        if (npc.get(NPC_.TemplateFlag.USE_KEYWORDS) && npc.isTemplated()) {  //setup templated npc's keywords
                            NPC_ npc_tpl = (NPC_) merger.getMajor(npc.getTemplate(), GRUP_TYPE.NPC_);
                            if (npc_tpl != null) {
                                npc.set(NPC_.TemplateFlag.USE_KEYWORDS, false); //don't inherit keywords from template
                                npc_tpl.getKeywordSet().getKeywordRefs().stream().filter((tpl_kywd) -> (!npc.getKeywordSet().getKeywordRefs().contains(tpl_kywd) && !tpl_kywd.equals(validHeadPartKYD.getForm()))).forEach((tpl_kywd) -> {
                                    npc.getKeywordSet().addKeywordRef(tpl_kywd);
                                });
                            }
                        }
                    }

                    if (isNPCchanged) {
                        npc.getKeywordSet().addKeywordRef(processedByPatcherKYD.getForm());
                    }

                    if (isValidHeadPart) {
                        npc.getKeywordSet().addKeywordRef(validHeadPartKYD.getForm());
                        skyproc.genenums.Gender genflag;
                        if (npc.get(NPC_.NPCFlag.Female)) {
                            genflag = skyproc.genenums.Gender.FEMALE;
                        } else {
                            genflag = skyproc.genenums.Gender.MALE;
                        }
                        EBDUtilityFuncs.npcFuncs.setBaseFaceID(npc, npcRace, genflag);
                    }

                    // assign
                    if (isNPCchanged || isValidScript) {
                        patch.addRecord(npc);
                        npcValidCount++;
                    }
                }
            }
        }

        if (consisParserHeight != null) {
            consisParserHeight.writeConsisXmlFile();
        }

        List<EBDTexturePack> texPackList = Arrays.asList(FemaleTX, MaleTX, FemaleKhajiitTX, MaleKhajiitTX, FemaleArgonianTX, MaleArgonianTX);
        texPackList.stream().filter((txPack) -> (txPack != null && txPack.isValid())).forEach((txPack) -> {
            txPack.lastNPCEvent();
        });

        DebugLogSummary("Total Number of modified NPCs: " + Integer.toString(npcValidCount) + "; Total number of available NPCs: " + Integer.toString(merger.getNPCs().size()));

    }

    @Override
    public void runChangesToPatch() throws Exception {

        SPGlobal.setAllModsAsMasters(EBDSettings.isAddAllModsAsMasterEnabled());

        DebugLogSummary("EBD Version: " + this.getVersion());
        if (EBDMain.isEnderal()) {
            DebugLogSummary("Enderal Detected");
        }
        this.outputSettingsDebug();

        patch = SPGlobal.getGlobalPatch();

        merger = new Mod(getName() + "Merger", false);
        merger.addAsOverrides(SPGlobal.getDB());

        XMLHeadPartParser = new EBDXMLHeadPartParser();

        SPProgressBarPlug.setStatus("Set Up Headparts");
        this.importBlockedHeadparts();
        this.writeHeadparts();

        SPProgressBarPlug.setStatus("Set Up NPCs");
        //this.setupMCM();
        this.setupNPCs();

    }

    public void importBlockedHeadparts() {
        if (EBDSettings.isHeadEnabled() && EBDSettings.isHeadSpellsEnabled()) {
            ArrayList<String> list = EBDHeadPartJsonFuncs.getJSONBlocklist();
            if (list.size() > 0) {
                EBDTextFileParser.addHeadPartEDIDBlacklist(list);
                DebugLogSummary("Imported " + Integer.toString(list.size()) + " headparts from JSON blocklist file. The headparts will now be ignored by the patcher.");
            }
        }
    }

    public void writeHeadparts() {
        if (EBDSettings.isHeadEnabled()) {
            EBDHeadPartJsonFuncs.writeJSON();
        }
    }

    public static ArrayList<FormID> getAllRaces(EBDMain.EBDEnums.Race RACE) {
        ArrayList<FormID> Races = new ArrayList<>();
        Mod allRacesMod = new Mod("tmp" + "Merger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            switch (RACE) {
                case HUMANOID:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceHumanoid(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
                case KHAJIIT:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceKhajiit(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
                case ARGONIAN:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceArgonian(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
            }
        }
        return Races;
    }

    public static ArrayList<FormID> getAllRacesID(EBDMain.EBDEnums.Race RACE) {
        ArrayList<FormID> Races = new ArrayList<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            switch (RACE) {
                case HUMANOID:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceHumanoid(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
                case KHAJIIT:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceKhajiit(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
                case ARGONIAN:
                    if (EBDUtilityFuncs.npcFuncs.isPlayableRaceArgonian(tmpRace)) {
                        Races.add(tmpRace.getForm());
                    }
                    break;
            }
        }
        return Races;
    }

    public static LinkedHashMap<String, FormID> getAllRaces() {
        LinkedHashMap<String, FormID> raceMap = new LinkedHashMap<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            if (EBDUtilityFuncs.npcFuncs.isPlayableRace(tmpRace)) {
                raceMap.put(tmpRace.getEDID(), tmpRace.getForm());
            }
        }
        return raceMap;
    }

    public static LinkedHashMap<FormID, String> getAllRacesAlt() {
        LinkedHashMap<FormID, String> raceMap = new LinkedHashMap<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            if (EBDUtilityFuncs.npcFuncs.isPlayableRace(tmpRace)) {
                raceMap.put(tmpRace.getForm(), tmpRace.getEDID());
            }
        }
        return raceMap;
    }

    public static LinkedHashMap<FormID, String> getAllRacesNames() {
        LinkedHashMap<FormID, String> raceMap = new LinkedHashMap<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            if (EBDUtilityFuncs.npcFuncs.isPlayableRace(tmpRace)) {
                raceMap.put(tmpRace.getForm(), tmpRace.getName());
            }
        }
        return raceMap;
    }

    public static LinkedHashMap<String, FormID> getCustomRaces() {
        LinkedHashMap<String, FormID> raceMap = new LinkedHashMap<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            if (EBDUtilityFuncs.npcFuncs.isPlayableRace(tmpRace)) {
                if (!EBDUtilityFuncs.npcFuncs.isSkyrimFormID(tmpRace.getForm())) {
                    raceMap.put(tmpRace.getEDID(), tmpRace.getForm());
                }
            }
        }
        return raceMap;
    }

    public static LinkedHashMap<String, FormID> getVanillaRaces() {
        LinkedHashMap<String, FormID> raceMap = new LinkedHashMap<>();
        Mod allRacesMod = new Mod("tmpMerger", false);
        allRacesMod.addAsOverrides(SPGlobal.getDB());
        for (RACE tmpRace : allRacesMod.getRaces()) {
            if (EBDUtilityFuncs.npcFuncs.isPlayableRace(tmpRace)) {
                if (EBDUtilityFuncs.npcFuncs.isSkyrimFormID(tmpRace.getForm())) {
                    raceMap.put(tmpRace.getEDID(), tmpRace.getForm());
                }
            }
        }
        return raceMap;
    }

    public void outputSettingsDebug() throws Exception {
        for (Enum en : EBDSaveFile.Settings.values()) {
            try {
                this.DebugLogSummary(en.toString() + ": " + save.getStr(en));
                this.DebugLogMain(en.toString() + ": " + save.getStr(en));

            } catch (NullPointerException e) {
                //this.DebugLogSummary(en.toString() + " is not a setting.");
            }
        }
    }

    public void addUIExtensionsRequirement(Mod patch) {
        // Enderal has its own UIExtensions
        if (EBDMain.isEnderal()) {
            return;
        }
        FormID UIExtID = new FormID("002852", "UIExtensions.esp");
        FLST UIExtList = new FLST("UIExtDummyList");
        UIExtList.addFormEntry(UIExtID);
    }

    public static class EBDEnums {

        public enum Race {

            HUMANOID,
            KHAJIIT,
            ARGONIAN
        }

        public enum Gender {

            FEMALE,
            MALE
        }

        public enum Headpart {

            HAIR,
            EYES,
            SCARS,
            BROWS,
            BEARD
        }

        public enum TextureType {

            FACE,
            BODY,
            HANDS,
            FEET,
            TAIL
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Don
 */
public class EBDXMLConsisParser {

    private final File XML_FILE;
    private final String NAME;
    private final HashMap<String, HashMap<String, String>> consisMap;
    private final int MemoryHash;
    private final List<String> VALUE_TYPES;
    private int FileHash = 0;

    public EBDXMLConsisParser(int hash, String TYPE, String... ValueTypes) {
        this.consisMap = new HashMap<>();
        String XML_FILE_PATH = "Consistency\\" + TYPE + "Consistency.xml";
        this.XML_FILE = new File(XML_FILE_PATH);
        this.NAME = TYPE;
        this.MemoryHash = hash;
        this.VALUE_TYPES = Arrays.asList(ValueTypes);

        if (!this.XML_FILE.exists()) {
            try {
                this.XML_FILE.createNewFile();

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "XML File Creation error. File: " + this.XML_FILE.getPath() + ". Error: " + ex);
            }

        } else {
            try {
                this.loadConsisFromFileToMemory();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "XML File Load error. File: " + this.XML_FILE.getPath() + ". Error: " + ex);
            }
        }

    }
    

    private String getName() {
        return this.NAME;
    }
    
    public int getSize() {
        return this.consisMap.size();
    }

    private int getHash() {
        return this.MemoryHash;
    }

    private File getXML_FILE() {
        return XML_FILE;
    }

    public boolean isNewHash() {
        return (this.getHash() != this.FileHash);
    }

    public List<String> getValueTypes() {
        return this.VALUE_TYPES;
    }

    public String getNPCValue(String NPC_ID, String ValueType) {
        HashMap<String, String> tmpMap = this.consisMap.get(NPC_ID);
        if (tmpMap != null) {
            return tmpMap.get(ValueType);
        }
        return null;
    }

    public void addNPC(String NPC_ID, String ValueType, String Value) {
        HashMap<String, String> tmpMap = this.consisMap.get(NPC_ID);
        if (tmpMap != null) {
            tmpMap.put(ValueType, Value);
        } else {
            HashMap<String, String> newMap = new HashMap<>();
            newMap.put(ValueType, Value);
            this.consisMap.put(NPC_ID, newMap);
        }
    }

    private Document getDocument() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(this.getXML_FILE());
            doc.getDocumentElement().normalize();
            return doc;
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(EBDXMLConsisParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void writeConsisXmlFile() {
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("ConsistencySave");
            rootElement.setAttribute("Type", this.getName());
            rootElement.setAttribute("Hash", Integer.toString(this.getHash()));
            doc.appendChild(rootElement);

            for (Map.Entry<String, HashMap<String, String>> entry : this.consisMap.entrySet()) {
                Element NPC_Element = doc.createElement("NPC");
                rootElement.appendChild(NPC_Element);  
                NPC_Element.setAttribute("FormID", entry.getKey());
                for (Map.Entry<String, String> subEntry : entry.getValue().entrySet()) {
                    NPC_Element.setAttribute(subEntry.getKey(), subEntry.getValue());
                }
                
            }

            this.writeXMLFile(doc);

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(EBDMain.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File 1 Creation error: " + this.getName() + " : " + ex);
        }
    }

    private void loadConsisFromFileToMemory() {

        Document doc = this.getDocument();
        this.FileHash = Integer.parseInt(doc.getDocumentElement().getAttribute("Hash"));

        NodeList NPC_List = doc.getElementsByTagName("NPC"); //only contains a single entry, there might be a better way to do this
        for (int j = 0; j < NPC_List.getLength(); j++) {
            Element NPC_Element = (Element) NPC_List.item(j);

            String formID = NPC_Element.getAttribute("FormID");
            HashMap<String, String> typeMap = new HashMap<>();

            this.getValueTypes().stream().forEach((valType) -> {
                String value = NPC_Element.getAttribute(valType);
                if (value != null && !value.isEmpty()) {
                    typeMap.put(valType, value);
                }
            });

            if (!typeMap.isEmpty()) {
                this.consisMap.put(formID, typeMap);
            }

        }

    }

    private void writeXMLFile(Document doc) {
        try {

            doc.getDocumentElement().normalize();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(this.getXML_FILE());

            transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
            transformer.setOutputProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Logger.getLogger(EBDXMLConsisParser.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File Write error: " + this.getXML_FILE().getName() + " : " + ex);
        }
    }

}

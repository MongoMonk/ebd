/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import skyproc.FormID;
import skyproc.SPGlobal;

/**
 *
 * @author Don
 */

//not even close to being finished here, ignore this file for now
public class EBDHeightJSONEqDist {



    Gson gson;
    EBDHeightRaceObj hHeightObj;
    final String FILE_NAME;

    public EBDHeightJSONEqDist() {
        this.gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        this.FILE_NAME = "DynamicSettings\\HeightRaceEqDist.json";
        this.hHeightObj = this.readJSON();
    }

    private class EBDHeightRaceObj {

        HashMap<String, ArrayList<String>> HeightMapMale; //sadly gson doesn't work with lists of FormIDs, so stupid String <--> FormID conversion has to take place, would probably custom type adapter

        private EBDHeightRaceObj() {
            this.HeightMapMale = new HashMap<>();
        }

        private ArrayList<String> getRaceList(String race) {
            if (this.HeightMapMale.containsKey(race)) {
                return this.HeightMapMale.get(race);
            } else {
                return new ArrayList<>();
            }
        }

        private boolean isInlcudedRace(String race) {
            return (this.HeightMapMale.containsKey(race) && this.HeightMapMale.get(race).contains(race));
        }

        private void setRace(String race, ArrayList<String> races) {
            this.HeightMapMale.put(race, races);
        }
    }
    

    public ArrayList<FormID> getRaceList(FormID raceForm) {
        ArrayList<String> races = this.hHeightObj.getRaceList(raceForm.getFormStr());
        ArrayList<FormID> racesID = new ArrayList<>();
        for (String race: races) {
            racesID.add(new FormID(race));
        }
        return racesID;
    }
    
    public boolean isActiveRace(FormID cRaceForm, FormID raceForm) {
        ArrayList<FormID> racesID = getRaceList(cRaceForm);
        return racesID.contains(raceForm);
    }
    public void setRace(FormID raceForm, ArrayList<FormID> raceForms) {
        ArrayList<String> races = new ArrayList<>();
        for (FormID raceID : raceForms) {
            races.add(raceID.getFormStr());
        } 
        
        this.hHeightObj.setRace(raceForm.getFormStr(), races);
        
        this.writeJSON();
    }

    public boolean isInlcudedRace(FormID raceForm) {
        return this.hHeightObj.isInlcudedRace(raceForm.getFormStr());
    }

    private EBDHeightRaceObj readJSON() {

        File file = new File(this.FILE_NAME);
        EBDHeightRaceObj tmp = new EBDHeightRaceObj();

        if (file.exists()) {

            try (FileReader fr = new FileReader(file)) {

                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(fr);
                //JsonReader jreader = new JsonReader(fr);
                tmp = this.gson.fromJson(jsonElement, EBDHeightRaceObj.class);
                fr.close();

                //FileWriter writer = new FileWriter (file);
            } catch (IOException e) {
                System.err.println(e.toString());
                SPGlobal.logException(e);
                //JOptionPane.showMessageDialog(null, "There was an exception while writing HeadpartBlocklist.json: '" + e + "'  The headpart feature will not work without it.");
            }
        }
        return tmp;
    }

    public void writeJSON() {
        try (FileWriter writer = new FileWriter(this.FILE_NAME);) {
            writer.write(this.gson.toJson(this.hHeightObj));
            writer.close();

        } catch (IOException e) {
            System.err.println(e.toString());
            SPGlobal.logException(e);
            JOptionPane.showMessageDialog(null, "There was an exception while writing " + this.FILE_NAME + ": '" + e + "'");
        }

    }

}

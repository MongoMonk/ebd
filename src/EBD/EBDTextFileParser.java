/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import skyproc.SPGlobal;

/**
 *
 * @author Don
 */
public class EBDTextFileParser {

    private static ArrayList<String> getFileLines(String FileName) {
        ArrayList<String> tmpList = new ArrayList<>();
        String FILE_PATH = "Files\\" + FileName;
        File newFile = new File(FILE_PATH);

        if (!newFile.exists()) {
            String err = "Error: The file \"" + FILE_PATH + "\" does not exist. It is required for EBD to work. Please make sure that you installed EBD properly.\nThe patcher will terminate.";

            JOptionPane.showMessageDialog(null, err);
            System.exit(0);

        } else {
            try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH))) {
                String line;
                while ((line = br.readLine()) != null) {
                    //alternative: S is for everything except whitespaces: line.matches("\\S")
                    line = line.trim();
                    if (!line.contains("//") && !line.isEmpty()) {
                        tmpList.add(line);
                    }
                }
                br.close();

            } catch (IOException ex) {
                SPGlobal.logException(ex);
                Logger.getLogger(EBDUtilityFuncs.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return tmpList;
    }

    private static void addFileLines(String FileName, ArrayList<String> sToAdd) { //write lines if not there; add double quotes ""
        ArrayList<String> tmpList = getFileLines(FileName);
        String FILE_PATH = "Files\\" + FileName;
        File newFile = new File(FILE_PATH);

        if (sToAdd.size() > 0) {
            if (newFile.exists()) {
                try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(FILE_PATH, true)))) {

                    //ZonedDateTime zdt = ZonedDateTime.now();
                    pw.println();
                    pw.println("//The following " + Integer.toString(sToAdd.size()) + " headparts were imported automatically by EBD from the HeadpartBlocklist.json file. They were removed from the file afterwards.");// Date: " +  zdt.toString());

                    for (String line : sToAdd) {
                        line = line.trim();
                        line = "\"" + line + "\"";
                        if (!tmpList.contains(line)) {
                            pw.println(line);
                        }

                    }
                    pw.print("//End of import.");
                    pw.close();

                } catch (IOException ex) {
                    Logger.getLogger(EBDUtilityFuncs.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static ArrayList<String> getRaceEDIDBlacklist() {
        return getFileLines("Race_BlockedEDIDs.txt");
    }

    public static ArrayList<String> getNpcEDIDBlacklist() {
        return getFileLines("Npc_BlockedEDIDs.txt");
    }

    public static ArrayList<String> getHeadPartEDIDBlacklist() {
        return getFileLines("HeadPart_BlockedEDIDs.txt");
    }

    public static void addHeadPartEDIDBlacklist(ArrayList<String> sToAdd) {
        addFileLines("HeadPart_BlockedEDIDs.txt", sToAdd);
    }

    public static ArrayList<String> getKhajiitEDIDList() {
        return getFileLines("TexturePack_validEDIDsKhajiit.txt");
    }

    public static ArrayList<String> getArgonianEDIDList() {
        return getFileLines("TexturePack_validEDIDsArgonian.txt");
    }

}

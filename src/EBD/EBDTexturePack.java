package EBD;

import EBD.EBDMain.EBDEnums;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import skyproc.*;
import skyproc.genenums.FirstPersonFlags;

/**
 * TODO - merge FaceStorage and EffectiveSubPack (?), they are more or less the
 * same
 *
 * @author Don
 */
public class EBDTexturePack {

    private void DebugLogMain(String message) {
        if (EBDMain.EBDSettings.isDebugEnabled()) {
            SPGlobal.log(this.getClass().getSimpleName() + "_" + this.GENDER_STRING + this.RACE_STRING, message);
        }
    }

    private void DebugLogSummary(String message) {
        DebugLogMain(message);
        SPGlobal.logSpecial(EBDMain.EBDLOGS.SUMMARY, this.getClass().getSimpleName() + "_" + this.GENDER_STRING + this.RACE_STRING, message);
    }

    private void DebugLogTexHash(String message) {
        SPGlobal.logSpecial(EBDMain.EBDLOGS.TEXTUREHASHES, this.getClass().getSimpleName(), message);

    }

    private void setFaceHDPTTexture(NPC_ npc) {
        //if npc uses a custom headpart for its face we remove it
        //this means the npc will not have a custom face but also no neck seam
        ArrayList<FormID> removeList = new ArrayList<>();
        for (FormID hdpt_id : npc.getHeadParts()) {
            //only modify mod added headparts
            if (!EBDUtilityFuncs.npcFuncs.isSkyrimFormID(hdpt_id)) {
                HDPT hdpt = (HDPT) this.mergerMod.getMajor(hdpt_id, GRUP_TYPE.HDPT);
                try {
                    if (hdpt.getHDPT_Type().equals(skyproc.HDPT.HDPT_Type.Face)) {
                        if (hdpt.getBaseTexture() != null && !hdpt.getBaseTexture().isNull()) {
                            DebugLogSummary("Found NPC with custom face part texture: " + npc.getEDID() + " from " + npc.getForm() + ". Clearing");
                            removeList.add(hdpt_id);
                        }
                    }
                } catch (Exception e) {
                    DebugLogSummary("While looking for NPC headparts encountered exception: " + e + " for " + npc.getName() + ";" + npc.getForm());
                }
            }
        }
        for (FormID hdpt_id : removeList) {
            npc.removeHeadPart(hdpt_id);
        }
    }
    private final TextureStorage TexStore;
    private Mod mergerMod;
    private Mod patchMod;

    //debug purposes
    private int npcConsisCounter;
    private int npcCounter;

    //for converting textureSets/Armors to hashes
    private final TextureSetTools TxTool;
    //hashmap containing all the FormIDs of Armor FormIDs and face TextureSet IDs for the consistency feature
    private final HashMap<Integer, FormID> TextureConsistencyMap;

    private final EBDEnums.Gender PACK_GENDER;
    private final EBDEnums.Race PACK_RACE;
    private final skyproc.genenums.Gender SKYPROCGEN;
    private final EBDFileParser EBDParser;
    private final List<FormID> AllRacesIDList;
    private final String GENDER_STRING;
    private final String RACE_STRING;
    private final EBDXMLConsisParser consisParser;

    //strings for saving into the xml consistency file
    private final String faceString;
    private final String bodyString;

    EBDTexturePack(Mod mod, Mod opatch, EBDEnums.Gender GENDER, EBDEnums.Race RACE) {
        this.mergerMod = mod;
        this.patchMod = opatch;
        this.PACK_GENDER = GENDER;
        this.PACK_RACE = RACE;
        this.faceString = "FaceHash";
        this.bodyString = "BodyHash";
        this.TxTool = new TextureSetTools();
        this.TextureConsistencyMap = new HashMap<>();
        this.npcConsisCounter = 0;
        this.npcCounter = 0;

        this.AllRacesIDList = EBDMain.getAllRacesID(this.PACK_RACE);
        this.EBDParser = new EBDFileParser(this.PACK_GENDER, this.PACK_RACE);

        switch (this.PACK_GENDER) {
            case MALE:
                SKYPROCGEN = skyproc.genenums.Gender.MALE;
                GENDER_STRING = "Male";

                break;
            case FEMALE:
                SKYPROCGEN = skyproc.genenums.Gender.FEMALE;
                GENDER_STRING = "Female";

                break;
            default:
                SKYPROCGEN = null;
                DebugLogMain("WARNING: TexPackSetupDefaultCalled");
                GENDER_STRING = "";
                break;
        }

        switch (this.PACK_RACE) {
            case HUMANOID:
                this.RACE_STRING = "Human";
                DebugLogMain("TexPackSetup");
                break;
            case KHAJIIT:
                this.RACE_STRING = "Khajit";
                DebugLogMain("TexPackSetup");
                break;
            case ARGONIAN:
                this.RACE_STRING = "Argonian";
                DebugLogMain("TexPackSetup");
                break;

            default:
                DebugLogMain("WARNING: TexPackSetupDefaultCalled");
                this.RACE_STRING = "";
                break;
        }

        if (EBDParser.isValid()) {

            this.TexStore = new TextureStorage();
            if (EBDMain.EBDSettings.isConsistencyEnabled()) {
                this.consisParser = new EBDXMLConsisParser(this.TexStore.hashCode(), "TexturePack" + this.GENDER_STRING + this.RACE_STRING, new String[]{this.bodyString, this.faceString});
                DebugLogSummary("Consistency is enabled for: " + this.GENDER_STRING + this.RACE_STRING + ". Check whether the hash has changed since the last run: "
                        + this.consisParser.isNewHash() + ".");
            } else {
                this.consisParser = null;
            }
            if (EBDMain.EBDSettings.isFaceComplexSliderEnabled()) {
                this.TexStore.addFaceTexturesToRaceRecords();
            }
            this.TexStore.debugOutputMainPacks();
            if (EBDMain.EBDSettings.isDebugEnabled()) {
                this.TexStore.debugOutputTexHash();
            }
        } else {
            DebugLogSummary("ERROR: The File Parser encountered an error. The TexturePack will not be created");
            this.TexStore = null;
            this.consisParser = null;
        }

    }

    public void lastNPCEvent() {
        if (this.consisParser != null) {
            this.consisParser.writeConsisXmlFile();
            DebugLogSummary("Out of " + Integer.toString(this.npcCounter) + " total NPCs a previous texture was found for " + Integer.toString(this.npcConsisCounter) + " of them.");
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.TexStore);
        hash = 13 * hash + Objects.hashCode(this.RACE_STRING);
        hash = 13 * hash + Objects.hashCode(this.GENDER_STRING);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EBDTexturePack other = (EBDTexturePack) obj;
        if (this.PACK_GENDER != other.PACK_GENDER) {
            return false;
        }
        return this.PACK_RACE == other.PACK_RACE;
    }

    public boolean isValid() {
        return this.TexStore != null;
    }

    public boolean setNPCTextures(NPC_ npc) {
        this.npcCounter++;
        if (this.consisParser != null && !this.consisParser.isNewHash()) {

            String armorStr = this.consisParser.getNPCValue(npc.getFormStr(), this.bodyString);
            String faceStr = this.consisParser.getNPCValue(npc.getFormStr(), this.faceString);
            FormID armorID = null;
            FormID faceID = null;
            boolean faceNull = true;
            boolean bodyNull = true;
            TextureStorage.LinkedObjSave lObjSave = null;

            if (armorStr != null && !armorStr.isEmpty()) {
                armorID = TextureConsistencyMap.get(Integer.parseInt(armorStr));
                //check if armor is still valid for the given NPC
                if (armorID != null) {
                    boolean armorValid = false;
                    for (TextureStorage.MainPackStorage MP : this.TexStore.getValidMainPacksByRace(npc.getRace())) {
                        if (armorValid) {
                            break;
                        }
                        for (TextureStorage.LinkedObjSave ObjSave : MP.getValidBodyArmorsByRace(npc.getRace())) {

                            if (armorID == (FormID) ObjSave.getMainObject()) {
                                armorValid = true;
                                lObjSave = ObjSave;
                                break;
                            }

                        }
                    }
                    if (!armorValid) {
                        DebugLogSummary("Found NPC with invalid armor: " + npc.getEDID() + ", FormID: " + npc.getFormStr() + ". Assingning a new one.");
                        return TexStore.setBodyTexture(npc);
                    }
                }
            }

            if (faceStr != null && !faceStr.isEmpty()) {
                faceID = TextureConsistencyMap.get(Integer.parseInt(faceStr));

                //check if the face texture is still valid for the given NPC
                if (faceID != null) {
                    boolean faceValid = false;
                    if (lObjSave != null) {
                        for (TextureStorage.MainPackStorage MP : this.TexStore.getValidMainPacksByRace(npc.getRace())) {

                            if (MP.getLinkedFaceTextures(TexStore.getBaseFaceID(npc), npc.getRace(), lObjSave.getLinkedSubPacks()).contains(faceID)) {

                                faceValid = true;
                                break;

                            }
                        }
                    } else {
                        for (TextureStorage.MainPackStorage MP : this.TexStore.getValidMainPacksByRace(npc.getRace())) {

                            if (MP.getNewFaceTextureByRaceID(npc.getRace()).contains(faceID)) {

                                faceValid = true;
                                break;

                            }
                        }
                    }
                    if (!faceValid) {
                        DebugLogSummary("Found NPC with invalid face " + npc.getEDID() + ", FormID: " + npc.getFormStr() + ". Assingning a new one.");
                        return TexStore.setBodyTexture(npc);
                    }
                }
            }

            if (armorID != null && !armorID.isNull()) {
                if (patchMod.contains(armorID)) {
                    npc.setSkin(armorID);
                    bodyNull = false;

                } else {
                    DebugLogSummary("Previous armor not found for: " + npc.getEDID() + ". " + npc.getFormStr());

                    return TexStore.setBodyTexture(npc);
                }

            }
            if (faceID != null && !faceID.isNull()) {
                if (patchMod.contains(faceID)) {
                    npc.setFeatureSet(faceID);
                    //this.setFaceHDPTTexture(npc, faceID);
                    npc.getKeywordSet().addKeywordRef(EBDMain.processFaceKYD.getForm());
                    faceNull = false;
                } else {
                    DebugLogSummary("Previous face texture not found for: " + npc.getEDID() + ". " + npc.getFormStr());

                    return TexStore.setBodyTexture(npc);
                }

            }
            if (!faceNull || !bodyNull) {
                this.npcConsisCounter++;
            }

            //no valid face/body texture found, set a new one
            if (faceNull && bodyNull) {
                return TexStore.setBodyTexture(npc);
            }

            return true;
        } else {
            return TexStore.setBodyTexture(npc);
        }

    }

    private class TextureStorage {

        private EBDXMLTexPackParser XmlParser;

        private final ArrayList<MainPackStorage> MainPacks;
        private final RaceFaceVanillaStorage BaseVanillaFaces;
        private final BodyVanillaStorage BaseVanillaBodyTex;
        private final EBDRandomFunctions rndfunc;

        private final int iMaxFiles;

        private TextureStorage() {
            this.MainPacks = new ArrayList<>();
            this.rndfunc = new EBDRandomFunctions();
            this.BaseVanillaFaces = new RaceFaceVanillaStorage();
            this.BaseVanillaBodyTex = new BodyVanillaStorage();

            DebugLogMain("TexPackStartXmlParser");

            this.XmlParser = new EBDXMLTexPackParser(EBDParser.getMainPacksWithSubPacks(), PACK_GENDER, PACK_RACE);

            DebugLogMain("TexPackStartSetup");

            this.iMaxFiles = EBDParser.getNumberOfFiles();
            this.createMainPacks();
            this.setupRecords();

        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 41 * hash + Objects.hashCode(this.MainPacks);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final TextureStorage other = (TextureStorage) obj;
            return Objects.equals(this.MainPacks, other.MainPacks);
        }

        private void createMainPacks() {
            this.XmlParser.getActiveMainPacks().stream().forEach((MainPackName) -> {
                TextureStorage.this.addMainPack(MainPackName, TextureStorage.this.XmlParser.getActiveSubPacksByMainPack(MainPackName));
                TextureStorage.this.setValidRaces(MainPackName);
            });

        }

        public BodyVanillaStorage getVanillaBodyRecords() {
            return this.BaseVanillaBodyTex;
        }

        public ArrayList<FormID> getBaseFaceTexturesByRace(FormID Race) {
            return this.BaseVanillaFaces.getAllFaceTexturesByRace(Race);
        }

        public void addMainPack(String Name, ArrayList<String> SubPacks) {
            MainPackStorage MP = new MainPackStorage(Name, SubPacks);
            this.MainPacks.add(MP);
        }

        public MainPackStorage getMainPack(int NoOfMainPack) {
            return this.MainPacks.get(NoOfMainPack);
        }

        public LinkedHashSet<FormID> getValidRacesForSubPack(int NoOfMainPack, ArrayList<String> SubPackNames) {
            return this.MainPacks.get(NoOfMainPack).getValidRacesForSubPack(SubPackNames);
        }

        private MainPackStorage getMainPackByName(String MainPack) {
            for (MainPackStorage store : this.MainPacks) {
                if (store.getMainPackName().equalsIgnoreCase(MainPack)) {
                    return store;
                }
            }
            return null;
        }

        public void setValidRaces(String MainPack) {
            ArrayList<FormID> validRaces = XmlParser.getActiveRacesForMainPack(MainPack);
            MainPackStorage store = this.getMainPackByName(MainPack);
            if (!validRaces.isEmpty() && store != null) {
                //DebugLogSummary("Found valid races for " + store.getMainPackName());
                store.setValidRaces(new LinkedHashSet<>(validRaces));

            } else {
                DebugLogSummary("No valid races found for " + MainPack + ". Pack will be ignored.");
            }

        }

        private ArrayList<FormID> getAllNewFaceTexturesByRace(FormID raceID) {
            ArrayList<FormID> tmpList = new ArrayList<>();
            this.MainPacks.stream().forEach((store) -> {
                tmpList.addAll(store.getNewFaceTextureByRaceID(raceID));
            });
            return tmpList;
        }

        public void addFaceTexturesToRaceRecords() {
            AllRacesIDList.stream().forEach((raceID) -> {
                RACE race = (RACE) mergerMod.getMajor(raceID, GRUP_TYPE.RACE);
                this.getAllNewFaceTexturesByRace(raceID).stream().forEach((texID) -> {
                    if (!race.getFaceDetailsTextureSet(SKYPROCGEN).contains(texID)) {
                        race.getFaceDetailsTextureSet(SKYPROCGEN).add(texID);
                        DebugLogMain("AddFaceTexturesToRaceRecords: " + race.getEDID() + ": " + texID.getFormStr());
                    }
                });
                patchMod.addRecord(race);
            });

        }

        public void debugOutputTexHash() {
            for (TextureStorage.MainPackStorage mp : this.MainPacks) {
                for (TextureStorage.LinkedObjSave os : mp.getBodyArmors()) {
                    FormID armorID = (FormID) os.getMainObject();
                    DebugLogTexHash("BodyHash: " + Integer.toString(TxTool.getArmorHash(armorID)) + "; BodyTex: " + TxTool.getBodyArmorPaths(armorID).toString());
                }
                for (TextureStorage.MainPackStorage.SubPackStorage sp : mp.getSubPacks()) {
                    for (TextureStorage.FaceStorage fs : sp.getFaceStores()) {
                        FormID faceID = fs.getNewFaceTexture();
                        DebugLogTexHash("FaceHash: " + Integer.toString(TxTool.getTextureSetHash(faceID)) + "; FaceTex: " + TxTool.getFaceTexturePaths(faceID).toString());
                    }
                }
            }
        }

        public void debugOutputMainPacks() {
            this.MainPacks.stream().forEach((store) -> {
                int iBodyArmorPack = store.getBodyArmors().size();
                int iFaceTexPack = 0;
                LinkedHashSet<String> racesNoFaceTex = new LinkedHashSet<>();
                LinkedHashSet<String> racesNoBodyArmor = new LinkedHashSet<>();
                iFaceTexPack = store.getSubPacks().stream().map((subStore) -> subStore.getFaceStores().size()).reduce(iFaceTexPack, Integer::sum);
                for (FormID raceID : store.getValidRaces()) {
                    RACE race = (RACE) mergerMod.getMajor(raceID, GRUP_TYPE.RACE);
                    int iBodyArmorRace = store.getValidBodyArmorsByRace(raceID).size();
                    int iFaceTexRace = store.getNewFaceTextureByRaceID(raceID).size();
                    DebugLogMain("MainPack " + store.getMainPackName() + " contents: " + "Race " + race.getEDID() + " has "
                            + Integer.toString(iBodyArmorRace) + " body amors and " + Integer.toString(iFaceTexRace) + " face textures");
                    //if the mainpack has no textures at all the user is notified anyway
                    //only add & print races with no textures if other races of that mainpack have corresponding textures
                    if (iFaceTexPack > 0 && iFaceTexRace == 0) {
                        racesNoFaceTex.add(race.getEDID());
                    }
                    if (iBodyArmorPack > 0 && iBodyArmorRace == 0) {
                        racesNoBodyArmor.add(race.getEDID());
                    }
                }
                String sWarning = "";
                if (iFaceTexPack == 0) {
                    sWarning = " NOTICE: There are NO face textures in this MainPack.";
                }
                if (iBodyArmorPack == 0) {
                    sWarning += " NOTICE: There are NO body armors in this MainPack.";
                }
                DebugLogSummary("MainPack " + store.getMainPackName() + " has " + Integer.toString(store.getSubPacks().size()) + " Base SubPacks and " + Integer.toString(iBodyArmorPack)
                        + " Body Armors and " + Integer.toString(iFaceTexPack) + " Face Textures. It is valid for " + Integer.toString(store.ValidRaces.size()) + " races." + sWarning);
                if (!racesNoFaceTex.isEmpty()) {
                    DebugLogSummary("NOTICE: MainPack " + store.getMainPackName() + " has no face textures for the races " + racesNoFaceTex.toString()
                            + " even though the races are active for the mainpack.");
                }
                if (!racesNoBodyArmor.isEmpty()) {
                    DebugLogSummary("NOTICE: MainPack " + store.getMainPackName() + " has no body armors for the races " + racesNoBodyArmor.toString()
                            + " even though the races are active for the mainpack.");
                }
            });
        }

        private ArrayList<MainPackStorage> getValidMainPacksByRace(FormID RaceID) {
            ArrayList<MainPackStorage> tmpList = new ArrayList<>();
            this.MainPacks.stream().forEach((MainPackStorage store) -> {
                if (store.getValidRaces().stream().anyMatch((StoredRaceID) -> (StoredRaceID.equals(RaceID)))) {
                    tmpList.add(store);
                }
            });
            return tmpList;
        }

        private FormID getBaseFaceID(NPC_ npc) {
            FormID npcRaceID = npc.getRace();
            RACE raceRC = (RACE) mergerMod.getMajor(npcRaceID, GRUP_TYPE.RACE);

            FormID baseFaceID = npc.getFeatureSet();
            if (baseFaceID.isNull()) {
                baseFaceID = raceRC.getDefaultFaceTexture(SKYPROCGEN);
                if (baseFaceID.isNull()) {
                    ArrayList<FormID> baseFaceList = raceRC.getFaceDetailsTextureSet(SKYPROCGEN);
                    if (!baseFaceList.isEmpty()) {
                        baseFaceID = baseFaceList.get(0);
                    }
                }
            }
            return baseFaceID;
        }
       

        public boolean setBodyTexture(NPC_ npc) {
            boolean isChanged = false;

            FormID npcRaceID = npc.getRace();
            ArrayList<MainPackStorage> MPtmpList = this.getValidMainPacksByRace(npcRaceID);
            RACE raceRC = (RACE) mergerMod.getMajor(npcRaceID, GRUP_TYPE.RACE);
            if (!MPtmpList.isEmpty()) {

                FormID baseFaceID = this.getBaseFaceID(npc);

                int rndMP = this.rndfunc.RandomInt(0, MPtmpList.size() - 1);

                ArrayList<FormID> faceTexList = new ArrayList<>();

                ArrayList<LinkedObjSave> bodyArmorList = MPtmpList.get(rndMP).getValidBodyArmorsByRace(npcRaceID);

                if (!bodyArmorList.isEmpty()) {
                    int rndBodyArmor = this.rndfunc.RandomInt(0, bodyArmorList.size() - 1);
                    npc.setSkin((FormID) bodyArmorList.get(rndBodyArmor).getMainObject());
                    String sBodyHash = Integer.toString(TxTool.getArmorHash(npc.getSkin()));
                    if (consisParser != null) {
                        consisParser.addNPC(npc.getFormStr(), bodyString, sBodyHash);
                    }
                    isChanged = true;
                    faceTexList = MPtmpList.get(rndMP).getLinkedFaceTextures(baseFaceID, npcRaceID, bodyArmorList.get(rndBodyArmor).getLinkedSubPacks());
//                    if (faceTexList.size() > 1) {
//                        DebugLogSummary("NPC " + npc.getEDID() + "; " + npc.getFormStr() + " with race " + npc.getRace().getFormStr() + " gets armor " + (FormID) bodyArmorList.get(rndBodyArmor).getMainObject() + " which is linked to "
//                                + bodyArmorList.get(rndBodyArmor).getLinkedSubPacks() + " with face base " + baseFaceID.getFormStr() + " gets face list " + faceTexList.toString());
//                    }
                } else {
                    ArrayList<MainPackStorage.SubPackStorage> SPtmpList = MPtmpList.get(rndMP).getValidSubPacksFaceByRaceBaseID(baseFaceID, npcRaceID);
                    if (!SPtmpList.isEmpty()) {
                        int rndSP = this.rndfunc.RandomInt(0, SPtmpList.size() - 1);
                        faceTexList = SPtmpList.get(rndSP).getNewFaceTextures(baseFaceID, npcRaceID);
                    }

                }

                int rndFace;
                if (!faceTexList.isEmpty()) {
                    rndFace = this.rndfunc.RandomInt(0, faceTexList.size() - 1);
                    npc.setFeatureSet(faceTexList.get(rndFace));
                    npc.getKeywordSet().addKeywordRef(EBDMain.processFaceKYD.getForm());
                    //setFaceHDPTTexture(npc, faceTexList.get(rndFace));
                    String sFaceHash = Integer.toString(TxTool.getTextureSetHash(npc.getFeatureSet()));
                    if (consisParser != null) {
                        consisParser.addNPC(npc.getFormStr(), faceString, sFaceHash);
                    }
                    isChanged = true;
                } else {
                    DebugLogMain("Notice: No face textures found for NPC " + npc.getEDID() + ", " + npc.getFormStr() + ", " + raceRC.getEDID() + " in MainPack " + MPtmpList.get(rndMP).getMainPackName() + ".");
                }

            }

            return isChanged;
        }

        private void setupRecords() {
            for (int i = 0; i < this.MainPacks.size(); i++) {
                this.getMainPack(i).setupRecords();
                this.setupArmorAddonRecords(i);
                this.setupMissingArmorAddonRecords(i);
                this.setupBodyArmorRecords(i);
            }

        }

        //sets up the default armor addons for races which miss either the hands or the torso/tail/feet addons
        private void setupMissingArmorAddonRecords(int NoOfMainPack) {
            LinkedHashSet<FormID> tempRacesHands = new LinkedHashSet<>();
            LinkedHashSet<FormID> tempRacesTorso = new LinkedHashSet<>();

            LinkedHashSet<FormID> missingRacesHands = new LinkedHashSet<>();
            LinkedHashSet<FormID> missingRacesTorso = new LinkedHashSet<>();

            this.getMainPack(NoOfMainPack).getSubPacks(EBDEnums.TextureType.BODY).stream().forEach((subStore) -> {
                subStore.getTorsoFeetTailAAList().stream().forEach((objSave) -> {
                    tempRacesTorso.addAll(objSave.getValidRaces());
                });
            });
            this.getMainPack(NoOfMainPack).getSubPacks(EBDEnums.TextureType.HANDS).stream().forEach((subStore) -> {
                subStore.getHandsAAList().stream().forEach((objSave) -> {
                    tempRacesHands.addAll(objSave.getValidRaces());
                });
            });

            this.getMainPack(NoOfMainPack).getValidRaces().stream().filter((raceID) -> (!tempRacesTorso.contains(raceID) && tempRacesHands.contains(raceID))).forEach((raceID) -> {
                missingRacesTorso.add(raceID);
            });
            this.getMainPack(NoOfMainPack).getValidRaces().stream().filter((raceID) -> (!tempRacesHands.contains(raceID) && tempRacesTorso.contains(raceID))).forEach((raceID) -> {
                missingRacesHands.add(raceID);
            });

            if (!missingRacesTorso.isEmpty()) {
                this.getMainPack(NoOfMainPack).createHelperSubPack("BodyHelperSubPack", EBDEnums.TextureType.BODY, missingRacesTorso);
            }
            if (!missingRacesHands.isEmpty()) {
                this.getMainPack(NoOfMainPack).createHelperSubPack("HandsHelperSubPack", EBDEnums.TextureType.HANDS, missingRacesHands);
            }

        }

        private void setupArmorAddonRecords(int NoOfMainPack) {
            int bodyTXCount = 0;
            int handsTXCount = 0;
            int j;
            int k;
            for (MainPackStorage.SubPackStorage subPack : this.getMainPack(NoOfMainPack).getSubPacks()) { //go through Body SubPacks in MainPack   
                j = this.getMainPack(NoOfMainPack).getSubPacks().indexOf(subPack);
                EBDEnums.TextureType TMPTYPE = EBDEnums.TextureType.BODY;
                for (MainPackStorage.SubPackEffectiveStorage subPackEff : subPack.getEffectiveSubPacks(TMPTYPE)) {
                    k = subPack.getEffectiveSubPacks(TMPTYPE).indexOf(subPackEff);
                    TXST newSkinBodyTX = (TXST) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseTXST(TMPTYPE), "EBD_" + GENDER_STRING.charAt(0) + "_"
                            + RACE_STRING.charAt(0) + "_BodyTX_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    bodyTXCount++;
                    for (int m = 0; m < iMaxFiles; m++) {
                        if (subPackEff.getFilePatheStore().isFileSet(m)) {
                            newSkinBodyTX.setNthMap(m, subPackEff.getFilePatheStore().getFile(m));
                        }
                    }

                    //FLST newSkinSwapBodyFeetFFL = new FLST("EBDSkinBodySwap_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_FL_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    //newSkinSwapBodyFeetFFL.getFormIDEntries().add(newSkinBodyTX.getForm());
                    ARMA newTorsoAA = (ARMA) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseAA(TMPTYPE), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_TorsoAA_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    ARMA newFeetAA = (ARMA) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseAA(EBDEnums.TextureType.FEET), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_FeetAA_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    newTorsoAA.setSkinTexture(newSkinBodyTX.getForm(), SKYPROCGEN);
                    //newTorsoAA.setSkinSwap(newSkinSwapBodyFeetFFL.getForm(), SKYPROCGEN);
                    newTorsoAA.setSkinSwap(FormID.NULL, SKYPROCGEN);
                    newFeetAA.setSkinTexture(newSkinBodyTX.getForm(), SKYPROCGEN);
                    //newFeetAA.setSkinSwap(newSkinSwapBodyFeetFFL.getForm(), SKYPROCGEN);
                    newFeetAA.setSkinSwap(FormID.NULL, SKYPROCGEN);

                    if (EBDParser.hasMeshMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), TMPTYPE)) {
                        newTorsoAA.setModelPath(EBDParser.getMeshFileByMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), TMPTYPE), SKYPROCGEN, skyproc.genenums.Perspective.THIRD_PERSON);
                    }

                    if (EBDParser.hasMeshMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), EBDEnums.TextureType.FEET)) {
                        newFeetAA.setModelPath(EBDParser.getMeshFileByMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), EBDEnums.TextureType.FEET), SKYPROCGEN, skyproc.genenums.Perspective.THIRD_PERSON);
                    }

                    newTorsoAA.getAdditionalRaces().clear();
                    newFeetAA.getAdditionalRaces().clear();
                    newTorsoAA.getAdditionalRaces().addAll(subPackEff.getValidRaces());
                    newFeetAA.getAdditionalRaces().addAll(subPackEff.getValidRaces());

                    if (PACK_RACE.equals(EBDEnums.Race.ARGONIAN) || PACK_RACE.equals(EBDEnums.Race.KHAJIIT)) {
                        ARMA newTailAA = (ARMA) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseAA(EBDEnums.TextureType.TAIL), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_TailAA_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                        newTailAA.setSkinTexture(newSkinBodyTX.getForm(), SKYPROCGEN);
                        //newTailAA.setSkinSwap(newSkinSwapBodyFeetFFL.getForm(), SKYPROCGEN);
                        newTailAA.setSkinSwap(FormID.NULL, SKYPROCGEN);
                        newTailAA.getAdditionalRaces().clear();
                        newTailAA.getAdditionalRaces().addAll(subPackEff.getValidRaces());
                        subPack.addTorsoFeetTailAA(new FormIDTriple(newTorsoAA.getForm(), newFeetAA.getForm(), newTailAA.getForm()),
                                subPackEff.getValidRaces(), subPackEff.getLinkedSubPacks());
                    } else {
                        subPack.addTorsoFeetTailAA(new FormIDTriple(newTorsoAA.getForm(), newFeetAA.getForm(), null), subPackEff.getValidRaces(), subPackEff.getLinkedSubPacks());
                    }
                }

                TMPTYPE = EBDEnums.TextureType.HANDS;

                for (MainPackStorage.SubPackEffectiveStorage subPackEff : subPack.getEffectiveSubPacks(TMPTYPE)) { //go through Hand SubPacks in MainPack
                    k = subPack.getEffectiveSubPacks(TMPTYPE).indexOf(subPackEff);
                    TXST newSkinHandsTX = (TXST) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseTXST(TMPTYPE), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_HandsTX_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    handsTXCount++;
                    for (int m = 0; m < iMaxFiles; m++) {
                        if (subPackEff.getFilePatheStore().isFileSet(m)) {
                            newSkinHandsTX.setNthMap(m, subPackEff.getFilePatheStore().getFile(m));

                        }
                    }
                    //FLST newSkinSwapHandsFFL = new FLST("EBDSkinHandsSwap_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_FL_" + Integer.toString(NoOfMainPack + 1) + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    //newSkinSwapHandsFFL.getFormIDEntries().add(newSkinHandsTX.getForm());
                    ARMA newHandsAA = (ARMA) patchMod.makeCopy(this.getVanillaBodyRecords().getBaseAA(TMPTYPE), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_HandsAA_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    newHandsAA.setSkinTexture(newSkinHandsTX.getForm(), SKYPROCGEN);
                    //newHandsAA.setSkinSwap(newSkinSwapHandsFFL.getForm(), SKYPROCGEN);
                    newHandsAA.setSkinSwap(FormID.NULL, SKYPROCGEN);

                    if (EBDParser.hasMeshMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), TMPTYPE)) {
                        newHandsAA.setModelPath(EBDParser.getMeshFileByMainPack(this.getMainPack(NoOfMainPack).getMainPackName(), TMPTYPE), SKYPROCGEN, skyproc.genenums.Perspective.THIRD_PERSON);
                    }

                    newHandsAA.getAdditionalRaces().clear();
                    newHandsAA.getAdditionalRaces().addAll(subPackEff.getValidRaces());
                    this.getMainPack(NoOfMainPack).getSubPacks().get(j).addHandsAA(newHandsAA.getForm(), subPackEff.getValidRaces(), subPackEff.getLinkedSubPacks());
                }

                for (FaceStorage faceStore : subPack.getFaceStores()) { //go through Head SubPacks in MainPack
                    k = subPack.getFaceStores().indexOf(faceStore);
                    FormID baseID = faceStore.getSingleBaseID();
                    TXST SkinHeadTX = (TXST) mergerMod.getMajor(baseID, GRUP_TYPE.TXST);
                    TXST newSkinHeadTX = (TXST) patchMod.makeCopy(SkinHeadTX, "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0) + "_Head_" + this.getMainPack(NoOfMainPack).getSubPacks().get(j).getFaceStores().get(k).getRaceNames() + "TX_" + Integer.toString(NoOfMainPack + 1) + "_" + Integer.toString(j + 1) + "_" + Integer.toString(k + 1));
                    for (int m = 0; m < iMaxFiles; m++) {
                        if (faceStore.getFilePathes().isFileSet(m)) {
                            newSkinHeadTX.setNthMap(m, faceStore.getFilePathes().getFile(m));
                        }
                    }
                    faceStore.setFaceTexture(newSkinHeadTX.getForm());

                }

            }
            DebugLogMain(this.getMainPack(NoOfMainPack).getMainPackName() + " with " + Integer.toString(bodyTXCount) + " Body TXST Records and " + Integer.toString(handsTXCount) + " Hands TXST Records.");
        }

        private void setupBodyArmorRecords(int NoOfMainPack) {
            //feet,tail and body always have the same textures

            ArrayList<LinkedObjSave> TorsoFeetSaves = this.getMainPack(NoOfMainPack).getTorsoFeetTailAAList();
            TorsoFeetSaves.stream().forEach((torsoFeetTailSave) -> {
                FormIDTriple torsoFeetTailIDs = (FormIDTriple) torsoFeetTailSave.getMainObject();

                ArrayList<LinkedObjSave> HandsSaves = this.getMainPack(NoOfMainPack).getMatchedHandsAAList(torsoFeetTailSave.getLinkedSubPacks());
                if (HandsSaves.isEmpty()) {
                    DebugLogSummary("No Matching Hands Found. MainPack: " + this.getMainPack(NoOfMainPack).getMainPackName());
                    //HandsSaves = this.getMainPack(NoOfMainPack).getHandsAAList();
                }
                HandsSaves.stream().forEach((handsSave) -> {
                    FormID handsID = (FormID) handsSave.getMainObject();

                    LinkedHashSet<FormID> allRacesList = new LinkedHashSet<>();
                    LinkedHashSet<FormID> bodyList = torsoFeetTailSave.getValidRaces();
                    LinkedHashSet<FormID> handsList = handsSave.getValidRaces();

                    allRacesList.addAll(bodyList);
                    allRacesList.addAll(handsList);

                    LinkedHashSet<FormID> cleanedRacesList = new LinkedHashSet<>(allRacesList);
                    LinkedHashSet<String> LinkedSubs = new LinkedHashSet<>();

                    LinkedSubs.addAll(torsoFeetTailSave.getLinkedSubPacks());
                    LinkedSubs.addAll(handsSave.getLinkedSubPacks());
                    allRacesList.stream().forEach((id) -> {

                        if (!bodyList.contains(id)) {
                            cleanedRacesList.remove(id);
                        }
                        if (!handsList.contains(id)) {
                            cleanedRacesList.remove(id);
                        }

                    });
                    if (!cleanedRacesList.isEmpty()) {
                        ARMO newSkinArmor = (ARMO) patchMod.makeCopy(TextureStorage.this.getVanillaBodyRecords().getBaseBodyARMO(), "EBD_" + GENDER_STRING.charAt(0) + "_" + RACE_STRING.charAt(0)
                                + "_AR_" + Integer.toString(NoOfMainPack + 1) + "_b_" + Integer.toString(TorsoFeetSaves.indexOf(torsoFeetTailSave) + 1)
                                + "_h_" + Integer.toString(HandsSaves.indexOf(handsSave) + 1));  //+ "_h" + Integer.toString(iHandsPos) + "_" + Integer.toString(m));
                        ArrayList<FormID> amaList = new ArrayList<>(newSkinArmor.getArmatures());

                        for (FormID ama : amaList) { //remove all skyrim amas, so only mod added ones remain
                            if (EBDUtilityFuncs.npcFuncs.isSkyrimFormID(ama)) {
                                newSkinArmor.getArmatures().remove(ama);
                            }
                        }
                        newSkinArmor.getArmatures().add(torsoFeetTailIDs.getFirstID());
                        newSkinArmor.getArmatures().add(torsoFeetTailIDs.getSecondID());
                        newSkinArmor.getArmatures().add(handsID);

                        FormID tailID = torsoFeetTailIDs.getThirdID();

                        if (tailID != null && !tailID.isNull()) {
                            newSkinArmor.getArmatures().add(tailID);
                        }

                        this.getMainPack(NoOfMainPack).addBodyArmor(newSkinArmor.getForm(), cleanedRacesList, LinkedSubs);
                        TextureConsistencyMap.put(TxTool.getArmorHash(newSkinArmor.getForm()), newSkinArmor.getForm());
                    }
                });
            });

        }

        private class MainPackStorage {

            private final String MainPackName;
            private final LinkedHashSet<FormID> ValidRaces;
            private final ArrayList<SubPackStorage> SubPackStores;
            private final ArrayList<LinkedObjSave> BodyArmorList;

            private MainPackStorage(String Name, ArrayList<String> SubPacksName) {

                this.MainPackName = Name;
                this.BodyArmorList = new ArrayList<>();
                this.ValidRaces = new LinkedHashSet<>();
                this.SubPackStores = new ArrayList<>();
                SubPacksName.stream().forEach((subPackName) -> {
                    this.SubPackStores.add(new SubPackStorage(subPackName));
                });

            }

            @Override
            public int hashCode() {
                int hash = 7;
                hash = 13 * hash + Objects.hashCode(this.MainPackName);
                hash = 13 * hash + Objects.hashCode(this.ValidRaces);
                hash = 13 * hash + Objects.hashCode(this.SubPackStores);
                return hash;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final MainPackStorage other = (MainPackStorage) obj;
                if (!Objects.equals(this.MainPackName, other.MainPackName)) {
                    return false;
                }
                if (!Objects.equals(this.ValidRaces, other.ValidRaces)) {
                    return false;
                }
                return Objects.equals(this.SubPackStores, other.SubPackStores);
            }

            public SubPackStorage createHelperSubPack(String name, EBDEnums.TextureType TYPE, LinkedHashSet<FormID> validRaces) {
                SubPackStorage subPack = new SubPackStorage(name);
                subPack.setType(TYPE);
                subPack.setValidRacesFormID(new ArrayList<>(validRaces));
                LinkedHashSet<String> linkedSubs = new LinkedHashSet<>();
                linkedSubs.add(name);

                DebugLogSummary("Create Helper SubPack: " + this.getMainPackName() + ". Type: " + TYPE.toString() + ". Races:" + validRaces.toString());

                validRaces.stream().forEach((raceID) -> {
                    LinkedHashSet<FormID> tmpValidRaces = new LinkedHashSet<>();
                    tmpValidRaces.add(raceID);
                    BodyVanillaStorage bodyVan = TextureStorage.this.getVanillaBodyRecords();
                    if (TYPE.equals(EBDEnums.TextureType.BODY)) {
                        if (EBDTexturePack.this.PACK_RACE.equals(EBDEnums.Race.HUMANOID)) {
                            subPack.addTorsoFeetTailAA(new FormIDTriple(bodyVan.getArmorAddonByRace(raceID, TYPE), bodyVan.getArmorAddonByRace(raceID, EBDEnums.TextureType.FEET),
                                    null), tmpValidRaces, linkedSubs);
                        } else {
                            subPack.addTorsoFeetTailAA(new FormIDTriple(bodyVan.getArmorAddonByRace(raceID, TYPE), bodyVan.getArmorAddonByRace(raceID, EBDEnums.TextureType.FEET),
                                    bodyVan.getArmorAddonByRace(raceID, EBDEnums.TextureType.TAIL)), tmpValidRaces, linkedSubs);
                        }
                    } else if (TYPE.equals(EBDEnums.TextureType.HANDS)) {
                        subPack.addHandsAA(TextureStorage.this.BaseVanillaBodyTex.getArmorAddonByRace(raceID, TYPE), tmpValidRaces, linkedSubs);
                    }
                });

                this.SubPackStores.add(subPack);

                return subPack;
            }

            public void addBodyHandsPath(FormID raceID, String folderName, LinkedHashSet<String> LinkedSubPacks, TXSTPathStorage FilePathes, EBDEnums.TextureType TYPE) {
                SubPackStorage subStore = this.getSubPackBaseStorageByName(folderName);
                if (subStore != null) {
                    subStore.addLinkedSubPacks(LinkedSubPacks);
                    for (SubPackEffectiveStorage tmpEff : subStore.getEffectiveSubPacks(TYPE)) {
                        if (tmpEff.getFilePatheStore().equals(FilePathes)) {
                            tmpEff.addValidRace(raceID);
                            return;
                        }
                    }
                    subStore.addEffectiveSubPack(new SubPackEffectiveStorage(TYPE, FilePathes, raceID, LinkedSubPacks));
                }

            }

            public void addFaceTexture(TXSTPathStorage FilePathes, String FolderName, LinkedHashSet<String> LinkedSubPacks, FormID baseTexID, FormID raceID) {
                SubPackStorage subStore = this.getSubPackBaseStorageByName(FolderName);
                if (subStore != null) {
                    subStore.addLinkedSubPacks(LinkedSubPacks);
                    for (FaceStorage tmpStore : subStore.getFaceStores()) {
                        if (tmpStore.getFilePathes().equals(FilePathes)) {

                            //DebugLogSummary("File path old: " + tmpStore.getFilePathes().toString() + " file path new: " + FilePathes.toString());
                            tmpStore.addNewBaseID(baseTexID, raceID);
                            tmpStore.addLinkedSubPacks(LinkedSubPacks);
                            //DebugLogSummary("AFTER: Found equal with BaseIDs: " + tmpStore.getBaseIDs() + " race names: " + tmpStore.getRaceNames());
                            return;
                        }
                    }
                    FaceStorage newStore = new FaceStorage(FilePathes);
                    newStore.addNewBaseID(baseTexID, raceID);
                    newStore.addLinkedSubPacks(LinkedSubPacks);
                    subStore.getFaceStores().add(newStore);
                }
            }

            //returns face textures which have the most common textures with the given subpack
            public ArrayList<FormID> getLinkedFaceTextures(FormID baseID, FormID raceID, LinkedHashSet<String> LinkedFolders) {
                HashMap<FormID, Integer> LinkedSubMap = new HashMap<>();
                ArrayList<FormID> LinkedSubList = new ArrayList<>();
                ArrayList<SubPackStorage> subPackStores = this.getSubPacksByNames(new ArrayList<>(LinkedFolders));
                //DebugLogSummary("getLinkedFaceStart");
                this.getValidSubPacksFaceByRaceBaseID(baseID, raceID).stream().forEach((SubPackStorage tmpSubFace) -> {

                    tmpSubFace.getValidFaceStoresByRaceBaseID(baseID, raceID).stream().forEach((FaceStorage faceStore) -> {
                        //DebugLogSummary("baseIDs:" + faceStore.getBaseIDs() + " new face: " + faceStore.getNewFaceTexture().getFormStr() + " race names: " + faceStore.getRaceNames());
                        int cnt = 100;
                        LinkedHashSet<String> faceLinkedPacks = faceStore.getLinkedSubPacks();
                        for (String s1 : faceLinkedPacks) {
                            if (LinkedFolders.contains(s1)) {
                                cnt++;

                            } else {
                                cnt--;

                            }
                            for (String s2 : LinkedFolders) {
                                if (!s1.equalsIgnoreCase(s2)) {
                                    // check whether there are body textures inside a face subpack intersecting with the to be matched body subpack
                                    // really not optimal but it works
                                    if (EBDParser.hasIntersectingBaseFiles(MainPackName, s1, s2, EBDEnums.TextureType.BODY)) {
//                                        DebugLogSummary("Found intersecting base files for " + s1 + " and " + s2);
                                        cnt--;
                                    }
                                }
                            }

                        }

                        if (LinkedSubMap.containsKey(faceStore.getNewFaceTexture())) {
                            if (cnt > LinkedSubMap.get(faceStore.getNewFaceTexture())) {
                                LinkedSubMap.put(faceStore.getNewFaceTexture(), cnt);
                                //DebugLogSummary("Add: " + faceStore.getNewFaceTexture().getFormStr() + ". Count: " + cnt + ". face: " + tmpSubFace.getSubPackName() + ". links: " + faceStore.getLinkedSubPacks().toString() + ". body: " + tmpSubBody.getSubPackName() + ". links: " + bodyStore.getLinkedSubPacks().toString());
                            }
                        } else {
                            LinkedSubMap.put(faceStore.getNewFaceTexture(), cnt);
                            //DebugLogSummary("Create: " + faceStore.getNewFaceTexture().getFormStr() + ". Count: " + cnt + ". face: " + tmpSubFace.getSubPackName() + ". links: " + faceStore.getLinkedSubPacks().toString() + ". body: " + tmpSubBody.getSubPackName() + ". links: " + bodyStore.getLinkedSubPacks().toString());
                        }
                    });
                });
                int high = 0;
                for (int val : LinkedSubMap.values()) {
                    if (high < val) {
                        high = val;

                    }
                }

                for (Map.Entry<FormID, Integer> tmpEntry : LinkedSubMap.entrySet()) {

                    if (tmpEntry.getValue() == high) {
                        LinkedSubList.add(tmpEntry.getKey());

                        //DebugLogSummary(this.getMainPackName() + ": High Value: " + high + " for " + tmpSubBody.getSubPackName() + " with " + tmpEntry.getKey().getFormStr() + ". Base: " + baseID.getFormStr());
                    }
                }

                return LinkedSubList;
            }

            public boolean hasBodyArmor() {
                return !this.BodyArmorList.isEmpty();
            }

            private SubPackStorage getSubPackBaseStorageByName(String Name) {
                for (SubPackStorage store : this.SubPackStores) {
                    if (store.getSubPackName().equalsIgnoreCase(Name)) {
                        return store;
                    }
                }
                return null;
            }

            public ArrayList<LinkedObjSave> getValidBodyArmorsByRace(FormID raceID) {
                ArrayList<LinkedObjSave> tmpList = new ArrayList<>();
                this.BodyArmorList.stream().filter((tmpSave) -> (tmpSave.getValidRaces().contains(raceID))).forEach((tmpSave) -> {
                    tmpList.add(tmpSave);
                });
                return tmpList;
            }

            public ArrayList<SubPackStorage> getValidSubPacksFaceByRaceBaseID(FormID BaseID, FormID RaceID) {
                ArrayList<SubPackStorage> tmpList = new ArrayList<>();
                for (SubPackStorage store : this.SubPackStores) {
                    if (store.hasFaceTextures()) {
                        for (FaceStorage faceStore : store.getFaceStores()) {
                            if (faceStore.getRaceIDs().contains(RaceID)) {
                                if (faceStore.getBaseIDs().contains(BaseID)) {
                                    tmpList.add(store);
                                    break;
                                }
                            }
                        }
                    }
                }
                return tmpList;
            }

            public LinkedHashSet<FormID> getValidRacesForSubPack(ArrayList<String> SubPackNames) {
                LinkedHashSet<FormID> allRaces = new LinkedHashSet<>();
                ArrayList<SubPackStorage> subStores = this.getSubPacksByNames(SubPackNames);
                subStores.stream().forEach((subStore) -> {
                    allRaces.addAll(subStore.getValidRaces());
                });
                LinkedHashSet<FormID> cleanedRaces = new LinkedHashSet<>(allRaces);

                allRaces.stream().forEach((id) -> {
                    for (SubPackStorage subStore : subStores) {
                        if (!subStore.getValidRaces().contains(id)) {
                            cleanedRaces.remove(id);
                            break;
                        }
                    }
                });
                return cleanedRaces;
            }

            public ArrayList<SubPackStorage> getSubPacks() {
                return this.SubPackStores;
            }

            public ArrayList<SubPackStorage> getSubPacks(EBDEnums.TextureType TYPE) {
                ArrayList<SubPackStorage> subList = new ArrayList<>();
                this.SubPackStores.stream().filter((subPack) -> (subPack.hasAA(TYPE))).forEach((subPack) -> {
                    subList.add(subPack);
                });
                return subList;
            }

            //returns the SubPacks which only contain Armor Addons of the given type and no more
            public ArrayList<SubPackStorage> getSubPacksNonMatched(EBDEnums.TextureType TYPE) {
                ArrayList<SubPackStorage> subList = new ArrayList<>();
                this.SubPackStores.stream().forEach((subPack) -> {
                    boolean isValid = false;
                    if (subPack.hasAA(TYPE)) {
                        isValid = true;
                    }
                    for (Enum en : EBDEnums.TextureType.values()) {
                        if (!en.equals(TYPE) && subPack.hasAA((EBDEnums.TextureType) en)) {
                            isValid = false;
                        }
                    }
                    if (isValid) {
                        subList.add(subPack);
                    }
                });
                return subList;
            }

            public ArrayList<SubPackStorage> getSubPacksByNames(ArrayList<String> SubPacks) {
                ArrayList<SubPackStorage> tmpList = new ArrayList<>();
                SubPacks.stream().forEach((subName) -> {
                    this.SubPackStores.stream().filter((subStore) -> (subStore.getSubPackName().equalsIgnoreCase(subName))).forEach((subStore) -> {
                        tmpList.add(subStore);
                    });
                });
                return tmpList;
            }

            public ArrayList<FormID> getNewFaceTextureByRaceID(FormID raceID) {
                ArrayList<FormID> tmpList = new ArrayList<>();
                this.SubPackStores.stream().forEach((subStore) -> {
                    subStore.getFaceStores().stream().filter((tmpStore) -> (tmpStore.getSkyrimRaceIDs().contains(raceID))).forEach((tmpStore) -> {
                        tmpList.add(tmpStore.getNewFaceTexture());
                    });
                });
                return tmpList;
            }

            public void addBodyArmor(FormID id, LinkedHashSet<FormID> races, LinkedHashSet<String> LinkedSubpacks) {
                this.BodyArmorList.add(new LinkedObjSave(id, races, LinkedSubpacks));
            }

            public ArrayList<LinkedObjSave> getBodyArmors() {
                return this.BodyArmorList;
            }

            public String getMainPackName() {
                return this.MainPackName;
            }

            public void setValidRaces(LinkedHashSet<FormID> races) {
                this.ValidRaces.addAll(races);
                this.ValidRaces.stream().map((race) -> (RACE) mergerMod.getMajor(race, GRUP_TYPE.RACE)).forEach((raceRC) -> {
                    this.ValidRaces.add(raceRC.getForm());
                });
                this.SubPackStores.stream().forEach((subStore) -> {
                    subStore.setValidRaces(XmlParser.getActiveRacesForSubPack(this.MainPackName, subStore.getSubPackName()));
                });
            }

            public ArrayList<FormID> getValidRaces() {
                return new ArrayList<>(this.ValidRaces);
            }

            public ArrayList<LinkedObjSave> getHandsAAList() {
                ArrayList<LinkedObjSave> tmpMap = new ArrayList<>();
                this.getSubPacks(EBDEnums.TextureType.HANDS).stream().forEach((subPack) -> {
                    tmpMap.addAll(subPack.getHandsAAList());
                });
                return tmpMap;
            }

            public ArrayList<LinkedObjSave> getTorsoFeetTailAAList() {
                ArrayList<LinkedObjSave> tmpMap = new ArrayList<>();
                this.getSubPacks(EBDEnums.TextureType.BODY).stream().forEach((subPack) -> {
                    tmpMap.addAll(subPack.getTorsoFeetTailAAList());
                });
                return tmpMap;
            }

            public ArrayList<LinkedObjSave> getMatchedHandsAAList(LinkedHashSet<String> linkedFolderList) {
                HashMap<LinkedObjSave, Integer> LinkedSubMap = new HashMap<>();
                ArrayList<LinkedObjSave> LinkedSubList = new ArrayList<>();
                this.getSubPacks(EBDEnums.TextureType.HANDS).stream().forEach((handsSub) -> {
                    handsSub.getHandsAAList().stream().forEach((handStore) -> {
                        int cnt = 100;
                        int nonMatched = 0;
                        for (String s1 : handStore.getLinkedSubPacks()) {
                            if (linkedFolderList.contains(s1)) {
                                cnt++;
                                //break;
                            } else {
                                nonMatched++;
                                //break;
                            }
                        }
                        cnt -= nonMatched;
                        if (LinkedSubMap.containsKey(handStore)) {
                            if (cnt > LinkedSubMap.get(handStore)) {
                                LinkedSubMap.put(handStore, cnt);
                                //DebugLogSummary(this.getMainPackName() + ": Add: " + handStore.getHandsAA().getFormStr() + ". Count: " + cnt + ". face: " + handsSub.getSubPackName()
                                //        + ". links: " + handStore.getLinkedSubPacks().toString() + ". body: " + linkedList.toString() + ". links: ");
                            }
                        } else {
                            LinkedSubMap.put(handStore, cnt);
                            //DebugLogSummary(this.getMainPackName() + ": Create: " + handStore.getHandsAA().getFormStr() + ". Count: " + cnt + ". face: " + handsSub.getSubPackName()
                            //        + ". links: " + handStore.getLinkedSubPacks().toString() + ". body: " + linkedList.toString() + ". links: ");
                        }
                    });
                });

                int high = 0;
                for (int val : LinkedSubMap.values()) {
                    if (val > high) {
                        high = val;

                    }
                }

                for (Map.Entry<LinkedObjSave, Integer> tmpEntry : LinkedSubMap.entrySet()) {
                    if (tmpEntry.getValue() == high) {
                        LinkedSubList.add(new LinkedObjSave(tmpEntry.getKey().getMainObject(), tmpEntry.getKey().getValidRaces(), tmpEntry.getKey().getLinkedSubPacks()));
                        //DebugLogSummary(this.getMainPackName() + ": High Value: " + high + " for hands" + " with " + tmpEntry.getKey().getHandsAA().getFormStr() + ". Links: " + tmpEntry.getKey().getLinkedSubPacks().toString());
                    }
                }

                return LinkedSubList;

            }

            private class RaceFolderSetup {

                private final ArrayList<SubRaceFolderSetup> raceFolders;

                private RaceFolderSetup() {
                    this.raceFolders = new ArrayList<>();
                }

                public void addRaceWithFolders(FormID raceID, LinkedHashSet<String> folders) {
                    for (SubRaceFolderSetup subRF : this.raceFolders) {
                        if (subRF.hasSpecificFolders(folders)) {
                            subRF.addRace(raceID);
                            DebugLogMain("RaceFolder: found folders for race: " + raceID.toString());
                            return;
                        }
                    }
                    DebugLogMain("RaceFolder: new folders: " + folders.toString());
                    this.raceFolders.add(new SubRaceFolderSetup(folders, raceID));
                }

                public ArrayList<LinkedHashSet<String>> getAllFolders() {
                    ArrayList<LinkedHashSet<String>> allFolders = new ArrayList<>();
                    this.raceFolders.stream().forEach((subRF) -> {
                        allFolders.add(subRF.getFolders());
                        DebugLogMain("RaceFolder: get folders: " + subRF.getFolders().toString() + " with races: " + subRF.getRaces().toString());
                    });
                    return allFolders;
                }

                public LinkedHashSet<FormID> getRacesByFolders(LinkedHashSet<String> folders) {
                    for (SubRaceFolderSetup subRF : this.raceFolders) {
                        if (subRF.hasSpecificFolders(folders)) {

                            return subRF.getRaces();
                        }
                    }
                    return new LinkedHashSet<>();
                }

                private class SubRaceFolderSetup {

                    private final LinkedHashSet<FormID> races;
                    private final LinkedHashSet<String> folders;

                    private SubRaceFolderSetup(LinkedHashSet<String> folders, FormID race) {
                        this.races = new LinkedHashSet<>();
                        this.folders = new LinkedHashSet<>(folders);
                        this.races.add(race);
                    }

                    public LinkedHashSet<FormID> getRaces() {
                        return this.races;
                    }

                    public LinkedHashSet<String> getFolders() {
                        return this.folders;
                    }

                    public void addRace(FormID race) {
                        this.races.add(race);
                    }

                    public boolean hasSpecificFolders(LinkedHashSet<String> folders) {
                        if (folders.size() == this.folders.size()) {
                            if (this.folders.containsAll(folders)) {
                                return true;
                            }
                        }
                        return false;
                    }
                }
            }

            private void createSubPacksBodyHands(EBDEnums.TextureType TYPE) {
                RaceFolderSetup raceFolder = new RaceFolderSetup();
                this.ValidRaces.stream().forEach((raceID) -> {
                    LinkedHashSet<String> folders = new LinkedHashSet<>();
                    this.SubPackStores.stream().filter((subStore) -> (subStore.isType(TYPE))).filter((subStore) -> (subStore.getValidRaces().contains(raceID))).forEach((subStore) -> {
                        folders.add(subStore.getSubPackName());
                    });
                    raceFolder.addRaceWithFolders(raceID, folders);
                });
                raceFolder.getAllFolders().stream().forEach((folders) -> {
                    LinkedHashSet<EBDFileParser.TexturePack.MainPack.SubPack> tmpList = EBDParser.getCustomSubPacksForMainPack(this.MainPackName, new ArrayList<>(folders), TYPE);
                    tmpList.stream().forEach((subPack) -> {
                        SubPackStorage subStore = this.getSubPackBaseStorageByName(subPack.getSubPackFolderName());
                        if (subStore != null) {
                            LinkedHashSet<FormID> races = raceFolder.getRacesByFolders(folders);
                            races.stream().forEach((raceID) -> {
                                FormID txID = TextureStorage.this.getVanillaBodyRecords().getTextureByRace(raceID, TYPE);
                                DebugLogMain("NOTICE: new file path created in " + this.getMainPackName() + " for type: " + TYPE.name() + " for Race " + raceID.getTitle());
                                if (txID != null) {
                                    DebugLogMain("NOTICE: texID for new file path: " + txID.getTitle());
                                    TXST SkinTX = (TXST) mergerMod.getMajor(txID, GRUP_TYPE.TXST);
                                    TXSTPathStorage newFilePathStore = new TXSTPathStorage(subPack.getRelativePathOfAllFiles());
                                    for (int m = 0; m < iMaxFiles; m++) {
                                        if (!newFilePathStore.isFileSet(m)) {
                                            if (SkinTX.getNthMap(m) != null) {
                                                newFilePathStore.setFile(m, SkinTX.getNthMap(m));
                                            }
                                        }
                                    }
                                    this.addBodyHandsPath(raceID, subPack.getSubPackFolderName(), subPack.getTexPackNames(), newFilePathStore, TYPE);
                                }
                            });
                        }
                    });
                });
            }

            private void createSubPacksFace() {
                RaceFolderSetup raceFolder = new RaceFolderSetup();
                this.ValidRaces.stream().forEach((raceID) -> {
                    LinkedHashSet<String> folders = new LinkedHashSet<>();
                    this.SubPackStores.stream().filter((subStore) -> (subStore.isType(EBDEnums.TextureType.FACE)))
                            .filter((subStore) -> (subStore.getValidRaces().contains(raceID))).forEach((subStore) -> {
                        folders.add(subStore.getSubPackName());
                    });
                    raceFolder.addRaceWithFolders(raceID, folders);
                });

                raceFolder.getAllFolders().stream().forEach((folders) -> {
                    LinkedHashSet<EBDFileParser.TexturePack.MainPack.SubPack> tmpList = EBDParser.getCustomSubPacksForMainPack(this.MainPackName, new ArrayList<>(folders), EBDEnums.TextureType.FACE);
                    tmpList.stream().forEach((subPack) -> {
                        SubPackStorage subStore = this.getSubPackBaseStorageByName(subPack.getSubPackFolderName());
                        if (subStore != null) {
                            LinkedHashSet<FormID> races = raceFolder.getRacesByFolders(folders);
                            DebugLogMain("Input folders: " + folders.toString() + " output races: " + races.toString());
                            races.stream().forEach((raceID) -> {
                                DebugLogMain("NOTICE: new file path created for Race in " + this.getMainPackName() + " for type: " + "FACE" + " with id " + raceID.getTitle());
                                TextureStorage.this.getBaseFaceTexturesByRace(raceID).stream().forEach((baseID) -> {
                                    TXST SkinHeadTX = (TXST) mergerMod.getMajor(baseID, GRUP_TYPE.TXST);
                                    TXSTPathStorage newFilePathStore = new TXSTPathStorage(subPack.getRelativePathOfAllFiles());
                                    for (int m = 0; m < iMaxFiles; m++) {
                                        if (!newFilePathStore.isFileSet(m)) {
                                            if (SkinHeadTX.getNthMap(m) != null) {
                                                newFilePathStore.setFile(m, SkinHeadTX.getNthMap(m));
                                            }
                                        }
                                    }

                                    this.addFaceTexture(newFilePathStore, subPack.getSubPackFolderName(), subPack.getTexPackNames(), baseID, raceID);

                                });

                            });
                        }
                    });
                });
            }

            public void setupRecords() {
                this.createSubPacksBodyHands(EBDEnums.TextureType.BODY);
                this.createSubPacksBodyHands(EBDEnums.TextureType.HANDS);
                this.createSubPacksFace();
            }

            private class SubPackStorage {

                private final String SubPackName;
                private final LinkedHashSet<FormID> ValidRaces;
                private final LinkedHashSet<String> LinkedSubPacks;
                private final LinkedHashSet<LinkedObjSave> HandsAAList;
                private final LinkedHashSet<LinkedObjSave> TorsoFeetTailAAList;
                private final ArrayList<SubPackEffectiveStorage> SubPackStoresEffective;
                private final ArrayList<FaceStorage> newFaceStores;
                private final EnumMap<EBDEnums.TextureType, Boolean> typeMap;

                private SubPackStorage(String Name) {

                    this.SubPackName = Name;
                    this.ValidRaces = new LinkedHashSet<>();
                    this.HandsAAList = new LinkedHashSet<>();
                    this.TorsoFeetTailAAList = new LinkedHashSet<>();
                    this.SubPackStoresEffective = new ArrayList<>();
                    this.newFaceStores = new ArrayList<>();
                    this.LinkedSubPacks = new LinkedHashSet<>();
                    this.typeMap = new EnumMap<>(EBDEnums.TextureType.class);
                    this.typeMap.put(EBDEnums.TextureType.FACE, EBDParser.isSubPackType(MainPackName, this.SubPackName, EBDEnums.TextureType.FACE));
                    this.typeMap.put(EBDEnums.TextureType.BODY, EBDParser.isSubPackType(MainPackName, this.SubPackName, EBDEnums.TextureType.BODY));
                    this.typeMap.put(EBDEnums.TextureType.HANDS, EBDParser.isSubPackType(MainPackName, this.SubPackName, EBDEnums.TextureType.HANDS));
                }

                @Override
                public int hashCode() {
                    int hash = 7;
                    hash = 83 * hash + Objects.hashCode(this.SubPackName);
                    hash = 83 * hash + Objects.hashCode(this.ValidRaces);
                    hash = 83 * hash + Objects.hashCode(this.LinkedSubPacks);
                    hash = 83 * hash + Objects.hashCode(this.SubPackStoresEffective);
                    hash = 83 * hash + Objects.hashCode(this.newFaceStores);
                    return hash;
                }

                @Override
                public boolean equals(Object obj) {
                    if (obj == null) {
                        return false;
                    }
                    if (getClass() != obj.getClass()) {
                        return false;
                    }
                    final SubPackStorage other = (SubPackStorage) obj;
                    if (!Objects.equals(this.SubPackName, other.SubPackName)) {
                        return false;
                    }
                    if (!Objects.equals(this.ValidRaces, other.ValidRaces)) {
                        return false;
                    }
                    if (!Objects.equals(this.LinkedSubPacks, other.LinkedSubPacks)) {
                        return false;
                    }
                    if (!Objects.equals(this.SubPackStoresEffective, other.SubPackStoresEffective)) {
                        return false;
                    }
                    return Objects.equals(this.newFaceStores, other.newFaceStores);
                }

                public String getSubPackName() {
                    return this.toString();
                }

                public void setType(EBDEnums.TextureType TYPE) {
                    this.typeMap.put(TYPE, Boolean.TRUE);
                }

                @Override
                public String toString() {
                    return this.SubPackName;
                }

                public LinkedHashSet<String> getLinkedSubPacks() {
                    return this.LinkedSubPacks;
                }

                public void addLinkedSubPacks(LinkedHashSet<String> packs) {
                    this.LinkedSubPacks.addAll(packs);
                }

                public boolean isType(EBDEnums.TextureType TYPE) {
                    Boolean b = this.typeMap.get(TYPE);
                    if (b != null) {
                        return b;
                    } else {
                        return false;
                    }
                }

                public LinkedHashSet<LinkedObjSave> getHandsAAList() {
                    return this.HandsAAList;
                }

                public void addHandsAA(FormID armorAddon, LinkedHashSet<FormID> validRaces, LinkedHashSet<String> LinkedSubPacks) {
                    LinkedObjSave newSave = new LinkedObjSave(armorAddon, validRaces, LinkedSubPacks);
                    for (LinkedObjSave objSave : this.HandsAAList) {
                        if (objSave.equals(newSave)) {
                            objSave.merge(newSave);
                            return;
                        }
                    }
                    this.HandsAAList.add(newSave);
                }

                public LinkedHashSet<LinkedObjSave> getTorsoFeetTailAAList() {
                    return this.TorsoFeetTailAAList;
                }

                public void addTorsoFeetTailAA(FormIDTriple armorAddon, LinkedHashSet<FormID> validRaces, LinkedHashSet<String> LinkedSubPacks) {
                    LinkedObjSave newSave = new LinkedObjSave(armorAddon, validRaces, LinkedSubPacks);
                    for (LinkedObjSave objSave : this.TorsoFeetTailAAList) {
                        if (objSave.equals(newSave)) {
                            objSave.merge(newSave);
                            return;
                        }
                    }
                    this.TorsoFeetTailAAList.add(newSave);
                }

                public ArrayList<FaceStorage> getValidFaceStoresByRaceBaseID(FormID baseID, FormID raceID) {
                    ArrayList<FaceStorage> tmpList = new ArrayList<>();
                    this.newFaceStores.stream().filter((faceStore) -> (faceStore.getRaceIDs().contains(raceID) && faceStore.getBaseIDs().contains(baseID))).forEach((faceStore) -> {
                        tmpList.add(faceStore);
                    });
                    return tmpList;
                }

                public boolean hasAA(EBDEnums.TextureType TYPE) {
                    switch (TYPE) {
                        case HANDS:
                            return !this.getHandsAAList().isEmpty();
                        case BODY:
                            return !this.getTorsoFeetTailAAList().isEmpty();
                        default:
                            return false;
                    }
                }

                public boolean hasFaceTextures() {
                    return !this.newFaceStores.isEmpty();
                }

                public ArrayList<FaceStorage> getFaceStores() {
                    return this.newFaceStores;
                }

                public ArrayList<SubPackEffectiveStorage> getEffectiveSubPacks(EBDEnums.TextureType TYPE) {
                    ArrayList<SubPackEffectiveStorage> subPacksEff = new ArrayList<>();
                    this.SubPackStoresEffective.stream().filter((subStoreEff) -> (subStoreEff.getType().equals(TYPE))).forEach((subStoreEff) -> {
                        subPacksEff.add(subStoreEff);
                    });
                    return subPacksEff;
                }

                public ArrayList<FormID> getNewFaceTextures(FormID baseID, FormID raceID) {
                    ArrayList<FormID> faceTexList = new ArrayList<>();
                    this.getValidFaceStoresByRaceBaseID(baseID, raceID).stream().forEach((tmpFace) -> {
                        faceTexList.add(tmpFace.getNewFaceTexture());
                    });
                    return faceTexList;
                }

                public void addEffectiveSubPack(SubPackEffectiveStorage subPackEff) {
                    this.SubPackStoresEffective.add(subPackEff);
                }

                public void setValidRaces(ArrayList<FormID> races) {
                    races.stream().map((race) -> (RACE) mergerMod.getMajor(race, GRUP_TYPE.RACE)).forEach((raceRC) -> {
                        this.ValidRaces.add(raceRC.getForm());
                    });
                }

                public void setValidRacesFormID(ArrayList<FormID> races) {
                    this.ValidRaces.addAll(races);
                }

                public ArrayList<FormID> getValidRaces() {
                    return new ArrayList<>(this.ValidRaces);
                }

            }

            private class SubPackEffectiveStorage {

                private final LinkedHashSet<FormID> validRaces;
                private final LinkedHashSet<String> LinkedSubPacks;
                private final TXSTPathStorage FilePathes;
                private final EBDEnums.TextureType TYPE;

                private SubPackEffectiveStorage(EBDEnums.TextureType type, TXSTPathStorage filepath, FormID race, LinkedHashSet<String> LinkedSubs) {
                    this.validRaces = new LinkedHashSet<>();
                    this.validRaces.add(race);
                    this.FilePathes = filepath;
                    this.TYPE = type;
                    this.LinkedSubPacks = LinkedSubs;
                }

                private SubPackEffectiveStorage(EBDEnums.TextureType type, TXSTPathStorage filepath, LinkedHashSet<FormID> races, LinkedHashSet<String> LinkedSubs) {
                    this.validRaces = races;
                    this.FilePathes = filepath;
                    this.TYPE = type;
                    this.LinkedSubPacks = LinkedSubs;
                }

                @Override
                public int hashCode() {
                    int hash = 3;
                    hash = 59 * hash + Objects.hashCode(this.validRaces);
                    hash = 59 * hash + Objects.hashCode(this.LinkedSubPacks);
                    hash = 59 * hash + Objects.hashCode(this.FilePathes);
                    hash = 59 * hash + Objects.hashCode(this.TYPE.toString());
                    return hash;
                }

                @Override
                public boolean equals(Object obj) {
                    if (obj == null) {
                        return false;
                    }
                    if (getClass() != obj.getClass()) {
                        return false;
                    }
                    final SubPackEffectiveStorage other = (SubPackEffectiveStorage) obj;
                    if (!Objects.equals(this.validRaces, other.validRaces)) {
                        return false;
                    }
                    if (!Objects.equals(this.LinkedSubPacks, other.LinkedSubPacks)) {
                        return false;
                    }
                    if (!Objects.equals(this.FilePathes, other.FilePathes)) {
                        return false;
                    }
                    return this.TYPE == other.TYPE;
                }

                public LinkedHashSet<String> getLinkedSubPacks() {
                    return this.LinkedSubPacks;
                }

                public EBDEnums.TextureType getType() {
                    return this.TYPE;
                }

                public void addValidRace(FormID raceID) {
                    this.validRaces.add(raceID);
                }

                public LinkedHashSet<FormID> getValidRaces() {
                    return this.validRaces;
                }

                public TXSTPathStorage getFilePatheStore() {
                    return this.FilePathes;
                }
            }

        }

        private class RaceFaceVanillaStorage {

            private final ArrayList<SRaceFaceVanillaStorage> FaceStore;

            private RaceFaceVanillaStorage() {
                this.FaceStore = new ArrayList<>();
                AllRacesIDList.stream().map((raceID) -> new SRaceFaceVanillaStorage(raceID)).forEach((tmp) -> {
                    this.FaceStore.add(tmp);
                });
            }

            public ArrayList<FormID> getAllFaceTexturesByRace(FormID raceID) {
                ArrayList<FormID> tmpList = new ArrayList<>();
                this.FaceStore.stream().filter((tmpStore) -> (tmpStore.getRace().equals(raceID))).forEach((tmpStore) -> {
                    tmpList.addAll(tmpStore.getFaceIDs());
                });
                return tmpList;
            }

            private class SRaceFaceVanillaStorage {

                private final FormID RaceID;
                private final ArrayList<FormID> FaceIDs;

                private SRaceFaceVanillaStorage(FormID race) {
                    this.RaceID = race;
                    RACE raceRC = (RACE) mergerMod.getMajor(this.RaceID, GRUP_TYPE.RACE);
                    this.FaceIDs = new ArrayList<>();
                    this.FaceIDs.addAll(raceRC.getFaceDetailsTextureSet(SKYPROCGEN));
                }

                public FormID getRace() {
                    return this.RaceID;
                }

                public ArrayList<FormID> getFaceIDs() {
                    return this.FaceIDs;
                }
            }
        }

        private class FaceStorage {

            private final LinkedHashSet<FormID> baseIDs;
            private final LinkedHashSet<FormID> raceIDs;
            private final LinkedHashSet<String> raceNames;
            private final TXSTPathStorage FilePatheStores;
            private final LinkedHashSet<String> LinkedSubPacks;
            private FormID newFaceTexture;

            public FaceStorage(TXSTPathStorage FilePathes) {
                this.baseIDs = new LinkedHashSet<>();
                this.raceIDs = new LinkedHashSet<>();
                this.LinkedSubPacks = new LinkedHashSet<>();
                this.raceNames = new LinkedHashSet<>();
                this.FilePatheStores = FilePathes;
            }

            public void addNewBaseID(FormID baseID, FormID raceID) {
                this.baseIDs.add(baseID);
                this.raceIDs.add(raceID);
                RACE race = (RACE) mergerMod.getMajor(raceID, GRUP_TYPE.RACE);
                this.raceNames.add(race.getName());
            }

            public LinkedHashSet<FormID> getRaceIDs() {
                return raceIDs;
            }

            @Override
            public int hashCode() {
                int hash = 7;
                hash = 97 * hash + Objects.hashCode(this.baseIDs);
                hash = 97 * hash + Objects.hashCode(this.raceIDs);
                hash = 97 * hash + Objects.hashCode(this.FilePatheStores);
                hash = 97 * hash + Objects.hashCode(this.LinkedSubPacks);
                hash = 97 * hash + Objects.hashCode(this.newFaceTexture);
                return hash;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final FaceStorage other = (FaceStorage) obj;
                if (!Objects.equals(this.baseIDs, other.baseIDs)) {
                    return false;
                }
                if (!Objects.equals(this.raceIDs, other.raceIDs)) {
                    return false;
                }
                if (!Objects.equals(this.FilePatheStores, other.FilePatheStores)) {
                    return false;
                }
                if (!Objects.equals(this.LinkedSubPacks, other.LinkedSubPacks)) {
                    return false;
                }
                return Objects.equals(this.newFaceTexture, other.newFaceTexture);
            }

            public void setFaceTexture(FormID faceTex) {
                this.newFaceTexture = faceTex;
                TextureConsistencyMap.put(TxTool.getTextureSetHash(this.newFaceTexture), faceTex);
            }

            public void addLinkedSubPacks(LinkedHashSet<String> packs) {
                this.LinkedSubPacks.addAll(packs);
            }

            public LinkedHashSet<String> getLinkedSubPacks() {
                return this.LinkedSubPacks;
            }

            public String getRaceNames() {
                String S = new String();
                S = this.raceNames.stream().map((race) -> race.substring(0, 3)).reduce(S, String::concat);
                return S;
            }

            public FormID getNewFaceTexture() {
                return this.newFaceTexture;
            }

            public LinkedHashSet<FormID> getBaseIDs() {
                return this.baseIDs;
            }

            public FormID getSingleBaseID() {
                ArrayList<FormID> tmpList = new ArrayList<>(this.baseIDs);
                return tmpList.get(0);
            }

            public LinkedHashSet<FormID> getSkyrimRaceIDs() {
                return this.raceIDs;
            }

            public TXSTPathStorage getFilePathes() {
                return this.FilePatheStores;
            }
        }

        private class BodyVanillaStorage {

            private final FormID SkinTorsoAAID;
            private final FormID SkinHandsAAID;
            private final FormID SkinFeetAAID;

            private final FormID SkinBeastTailAAID;

            private final ARMA SkinTorsoAA;
            private final ARMA SkinHandsAA;
            private final ARMA SkinFeetAA;

            private final ARMA SkinBeastTailAA;

            private final FormID SkinBodyARID;
            private final ARMO SkinArmorAR;

            private final TXST BodySkinBaseTXST;
            private final TXST HandsSkinBaseTXST;

            private final FormID BodySkinBaseID;
            private final FormID HandsSkinBaseID;

            private final ArrayList<PartsByRace> TXSTStorage;

            private BodyVanillaStorage() {
                switch (PACK_RACE) {
                    case HUMANOID:
                        this.SkinBodyARID = new FormID("000D64", "Skyrim.esm");
                        this.SkinTorsoAAID = new FormID("000D67", "Skyrim.esm");
                        this.SkinHandsAAID = new FormID("000D6C", "Skyrim.esm");
                        this.SkinFeetAAID = new FormID("000D6E", "Skyrim.esm");
                        this.SkinBeastTailAAID = null;
                        switch (PACK_GENDER) {
                            case MALE:
                                BodySkinBaseID = new FormID("03EDE8", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("03EDEA", "Skyrim.esm");
                                break;
                            case FEMALE:
                                BodySkinBaseID = new FormID("03EDE7", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("03EDE9", "Skyrim.esm");
                                break;
                            default:
                                BodySkinBaseID = null;
                                HandsSkinBaseID = null;
                                break;
                        }
                        break;
                    case KHAJIIT:
                        this.SkinBodyARID = new FormID("069CE3", "Skyrim.esm");
                        this.SkinTorsoAAID = new FormID("081BA5", "Skyrim.esm");
                        this.SkinHandsAAID = new FormID("081BA4", "Skyrim.esm");
                        this.SkinFeetAAID = new FormID("081BA3", "Skyrim.esm");
                        this.SkinBeastTailAAID = new FormID("0B79B9", "Skyrim.esm");
                        switch (PACK_GENDER) {
                            case MALE:
                                BodySkinBaseID = new FormID("081B9F", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("081BA0", "Skyrim.esm");
                                break;
                            case FEMALE:
                                BodySkinBaseID = new FormID("0B79B8", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("0B79B7", "Skyrim.esm");
                                break;
                            default:
                                BodySkinBaseID = null;
                                HandsSkinBaseID = null;
                                break;
                        }
                        break;
                    case ARGONIAN:
                        this.SkinBodyARID = new FormID("069CE3", "Skyrim.esm");
                        this.SkinTorsoAAID = new FormID("04E76E", "Skyrim.esm");
                        this.SkinHandsAAID = new FormID("04E76D", "Skyrim.esm");
                        this.SkinFeetAAID = new FormID("04E76F", "Skyrim.esm");
                        this.SkinBeastTailAAID = new FormID("069CE2", "Skyrim.esm");
                        switch (PACK_GENDER) {
                            case MALE:
                                BodySkinBaseID = new FormID("04E86F", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("07C716", "Skyrim.esm");
                                break;
                            case FEMALE:
                                BodySkinBaseID = new FormID("058DB9", "Skyrim.esm");
                                HandsSkinBaseID = new FormID("0868FA", "Skyrim.esm");
                                break;
                            default:
                                BodySkinBaseID = null;
                                HandsSkinBaseID = null;
                                break;
                        }
                        break;
                    default:
                        this.SkinBodyARID = null;
                        this.SkinTorsoAAID = null;
                        this.SkinHandsAAID = null;
                        this.SkinFeetAAID = null;
                        this.SkinBeastTailAAID = null;
                        BodySkinBaseID = null;
                        HandsSkinBaseID = null;
                        break;
                }

                this.SkinArmorAR = (ARMO) mergerMod.getMajor(SkinBodyARID, GRUP_TYPE.ARMO);

                this.BodySkinBaseTXST = (TXST) mergerMod.getMajor(this.BodySkinBaseID, GRUP_TYPE.TXST);
                this.HandsSkinBaseTXST = (TXST) mergerMod.getMajor(this.HandsSkinBaseID, GRUP_TYPE.TXST);

                this.SkinTorsoAA = (ARMA) mergerMod.getMajor(SkinTorsoAAID, GRUP_TYPE.ARMA);
                this.SkinHandsAA = (ARMA) mergerMod.getMajor(SkinHandsAAID, GRUP_TYPE.ARMA);
                this.SkinFeetAA = (ARMA) mergerMod.getMajor(SkinFeetAAID, GRUP_TYPE.ARMA);
                this.SkinBeastTailAA = (ARMA) mergerMod.getMajor(SkinBeastTailAAID, GRUP_TYPE.ARMA);

                this.TXSTStorage = new ArrayList<>();
                AllRacesIDList.stream().forEach((raceID) -> {
                    this.TXSTStorage.add(new PartsByRace(raceID));
                });
            }

            public ARMO getBaseBodyARMO() {
                return this.SkinArmorAR;
            }

            public ARMA getBaseAA(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        return this.SkinTorsoAA;
                    case HANDS:
                        return this.SkinHandsAA;
                    case FEET:
                        return this.SkinFeetAA;
                    case TAIL:
                        return this.SkinBeastTailAA;
                    default:
                        return null;
                }
            }

            private FormID getBaseID(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        return BodySkinBaseID;
                    case HANDS:
                        return HandsSkinBaseID;
                    default:
                        return null;
                }
            }

            private TXST getBaseTXST(EBDEnums.TextureType TYPE) {
                switch (TYPE) {
                    case BODY:
                        return BodySkinBaseTXST;
                    case HANDS:
                        return HandsSkinBaseTXST;
                    default:
                        return null;
                }
            }

            public FormID getTextureByRace(FormID raceID, EBDEnums.TextureType TYPE) {
                FormID texID = null;
                for (PartsByRace part : this.TXSTStorage) {
                    if (part.getRace().equals(raceID)) {
                        texID = part.getTexture(TYPE);
                    }
                }
                if (texID != null) {
                    if (!texID.isNull()) {
                        return texID;
                    } else {
                        return this.getBaseID(TYPE);
                    }
                } else {
                    return this.getBaseID(TYPE);
                }
            }

            public FormID getArmorAddonByRace(FormID raceID, EBDEnums.TextureType TYPE) {
                FormID AA_ID = null;
                for (PartsByRace part : this.TXSTStorage) {
                    if (part.getRace().equals(raceID)) {
                        AA_ID = part.getArmorAddon(TYPE);
                    }
                }
                if (AA_ID != null) {
                    if (!AA_ID.isNull()) {
                        return AA_ID;
                    } else {
                        return this.getBaseAA(TYPE).getForm();
                    }
                } else {
                    if (this.getBaseAA(TYPE) != null) {
                        return this.getBaseAA(TYPE).getForm();
                    } else {
                        DebugLogSummary("No Base AA: " + TYPE.toString() + " Race: " + raceID.toString());
                        return null;
                    }

                }
            }

            private class PartsByRace {

                private final FormID RaceID;
                private final FormID SkinArmorID;
                private final HashMap<EBDEnums.TextureType, FormID> TXSTMap;
                private final HashMap<EBDEnums.TextureType, FormID> AAMap;

                private PartsByRace(FormID raceID) {

                    this.RaceID = raceID;
                    this.TXSTMap = new HashMap<>();
                    this.AAMap = new HashMap<>();
                    RACE raceRC = (RACE) mergerMod.getMajor(this.RaceID, GRUP_TYPE.RACE);
                    this.SkinArmorID = raceRC.getWornArmor();
                    if (this.SkinArmorID != null) {
                        ARMO tmpARMO = (ARMO) mergerMod.getMajor(this.SkinArmorID, GRUP_TYPE.ARMO);
                        if (tmpARMO != null) {
                            tmpARMO.getArmatures().stream()
                                    .map((tmpID) -> (ARMA) mergerMod.getMajor(tmpID, GRUP_TYPE.ARMA))
                                    .filter((tmpAA) -> (tmpAA.getRace().equals(this.RaceID) || tmpAA.getAdditionalRaces().contains(this.RaceID)))
                                    .forEach((tmpAA) -> {
                                        if (tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Normal, FirstPersonFlags.BODY) || tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Biped, FirstPersonFlags.BODY)) {
                                            this.TXSTMap.put(EBDEnums.TextureType.BODY, tmpAA.getSkinTexture(SKYPROCGEN));
                                            this.AAMap.put(EBDEnums.TextureType.BODY, tmpAA.getForm());

                                        } else if (tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Normal, FirstPersonFlags.HANDS) || tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Biped, FirstPersonFlags.HANDS)) {
                                            this.TXSTMap.put(EBDEnums.TextureType.HANDS, tmpAA.getSkinTexture(SKYPROCGEN));
                                            this.AAMap.put(EBDEnums.TextureType.HANDS, tmpAA.getForm());
                                        } else if (tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Normal, FirstPersonFlags.FEET) || tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Biped, FirstPersonFlags.FEET)) {
                                            this.AAMap.put(EBDEnums.TextureType.FEET, tmpAA.getForm());
                                        } else if (tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Normal, FirstPersonFlags.TAIL) || tmpAA.getBodyTemplate().get(BodyTemplate.BodyTemplateType.Biped, FirstPersonFlags.TAIL)) {
                                            this.AAMap.put(EBDEnums.TextureType.TAIL, tmpAA.getForm());
                                        }
                                    });
                        } else {
                            DebugLogMain("Found race with no default armor: " + raceRC.getEDID() + ". For this race the default armor will be used.");
                        }
                    }

                }

                public FormID getTexture(EBDEnums.TextureType TYPE) {
                    return this.TXSTMap.get(TYPE);
                }

                public FormID getArmorAddon(EBDEnums.TextureType TYPE) {
                    return this.AAMap.get(TYPE);
                }

                public FormID getRace() {
                    return this.RaceID;
                }
            }
        }

        private class LinkedObjSave {

            private final Object obj;
            private final LinkedHashSet<FormID> validRaces;
            private final LinkedHashSet<String> linkedSubpacks;

            private LinkedObjSave(Object object, LinkedHashSet<FormID> races, LinkedHashSet<String> linkedPacks) {
                this.obj = object;
                this.validRaces = races;
                this.linkedSubpacks = linkedPacks;
            }

            public Object getMainObject() {
                return this.obj;
            }

            public LinkedHashSet<FormID> getValidRaces() {
                return this.validRaces;
            }

            public LinkedHashSet<String> getLinkedSubPacks() {
                return this.linkedSubpacks;
            }

            public void merge(LinkedObjSave objSave) {
                this.validRaces.addAll(objSave.getValidRaces());
                this.linkedSubpacks.addAll(objSave.getLinkedSubPacks());
            }

            @Override
            public int hashCode() {
                return this.obj.hashCode();
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final LinkedObjSave other = (LinkedObjSave) obj;
                return Objects.equals(this.obj, other.obj);
            }

        }

        private class FormIDTriple {

            private final FormID firstID;
            private final FormID secondID;
            private final FormID thirdID;

            private FormIDTriple(FormID first, FormID second, FormID third) {
                this.firstID = first;
                this.secondID = second;
                this.thirdID = third;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final FormIDTriple other = (FormIDTriple) obj;
                if (!Objects.equals(this.firstID, other.firstID)) {
                    return false;
                }
                if (!Objects.equals(this.secondID, other.secondID)) {
                    return false;
                }
                if (this.thirdID != null) {
                    return Objects.equals(this.thirdID, other.thirdID);
                }
                return true;
            }

            @Override
            public int hashCode() {
                int hash = 5;
                hash = 11 * hash + Objects.hashCode(this.firstID);
                hash = 11 * hash + Objects.hashCode(this.secondID);
                if (this.thirdID != null) {
                    hash = 11 * hash + Objects.hashCode(this.thirdID);
                }

                return hash;
            }

            public FormID getFirstID() {
                return this.firstID;
            }

            public FormID getSecondID() {
                return this.secondID;
            }

            public FormID getThirdID() {
                return this.thirdID;
            }

        }

        private class TXSTPathStorage {

            private final int iMaxFiles;
            private final String[] FilePath;

            public TXSTPathStorage(int iMaxFiles) {
                this.iMaxFiles = iMaxFiles;
                this.FilePath = new String[this.iMaxFiles];

                for (int i = 0; i < this.iMaxFiles; i++) {
                    this.FilePath[i] = "";
                }
            }

            public TXSTPathStorage(String[] filePathes) {
                this.iMaxFiles = filePathes.length;
                this.FilePath = Arrays.copyOf(filePathes, filePathes.length);

            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final TXSTPathStorage other = (TXSTPathStorage) obj;
                if (this.iMaxFiles != other.iMaxFiles) {
                    return false;
                }
                return Arrays.deepEquals(this.FilePath, other.FilePath);
            }

            @Override
            public int hashCode() {
                int hash = 7;
                hash = 97 * hash + this.iMaxFiles;
                hash = 97 * hash + Arrays.deepHashCode(this.FilePath);
                return hash;
            }

            @Override
            public String toString() {
                String s = Integer.toString(this.iMaxFiles);
                for (int i = 0; i < this.iMaxFiles; i++) {
                    s += "_" + this.FilePath[i];
                }
                return s;
            }

            public boolean isFileSet(int NoOfFile) {
                return !this.FilePath[NoOfFile].equalsIgnoreCase("");
            }

            public void setFile(int NoOfFile, String path) {
                this.FilePath[NoOfFile] = path;
            }

            public String getFile(int NoOfFile) {
                return this.FilePath[NoOfFile];
            }

        }
    }

    //small class to get reproducable hashes for Armors and Textures by their filepathes
    class TextureSetTools {

        ArrayList<String> getFaceTexturePaths(FormID txID) {
            TXST txSet = (TXST) patchMod.getMajor(txID, GRUP_TYPE.TXST);

            return txSet.getTextures();

        }

        ArrayList<String> getBodyArmorPaths(FormID arID) {
            ARMO armo = (ARMO) patchMod.getMajor(arID, GRUP_TYPE.ARMO);

            for (FormID aaID : armo.getArmatures()) {
                if (aaID.getMaster().printNoSuffix().equalsIgnoreCase(EBD.EBDMain.myPatchName)) { //only check for EBD's parts
                    ARMA arma = (ARMA) patchMod.getMajor(aaID, GRUP_TYPE.ARMA);
                    if (arma != null) {
                        FormID skinTex = arma.getSkinTexture(SKYPROCGEN);
                        if (skinTex != null && !skinTex.isNull()) {
                            TXST txSet = (TXST) patchMod.getMajor(arma.getSkinTexture(SKYPROCGEN), GRUP_TYPE.TXST);
                            return txSet.getTextures();
                        }
                    }
                }
            }
            return new ArrayList<>();
        }

        int getTextureSetHash(FormID txID) {
            int hash = 23;
            TXST txSet = (TXST) patchMod.getMajor(txID, GRUP_TYPE.TXST);
            for (int i = 0; i < 8; i++) {
                if (txSet.getNthMap(i) != null) {
                    hash = 29 * hash + txSet.getNthMap(i).hashCode();
                }
            }
            return hash;
        }

        int getArmorHash(FormID arID) {

            int hash = 3;
            ARMO armo = (ARMO) patchMod.getMajor(arID, GRUP_TYPE.ARMO);
            Mod mod;
            for (FormID aaID : armo.getArmatures()) {
                if (aaID.getMaster().printNoSuffix().equalsIgnoreCase(EBD.EBDMain.myPatchName)) { //only check for EBD's parts
                    mod = patchMod;
                    ARMA arma = (ARMA) mod.getMajor(aaID, GRUP_TYPE.ARMA);
                    if (arma != null) {
                        FormID skinTex = arma.getSkinTexture(SKYPROCGEN);
                        if (skinTex != null && !skinTex.isNull()) {
                            TXST txSet = (TXST) mod.getMajor(arma.getSkinTexture(SKYPROCGEN), GRUP_TYPE.TXST);
                            for (int i = 0; i < 8; i++) {
                                if (txSet.getNthMap(i) != null) {
                                    hash = 23 * hash + txSet.getNthMap(i).hashCode();
                                }
                            }
                        }
                    }
                }
            }
            return hash;
        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import EBD.EBDMain.EBDEnums;
import EBD.EBDUtilityFuncs.npcFuncs;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import skyproc.*;
import skyproc.genenums.HeadPartFlags;

/**
 *
 * @author Don
 */
public class EBDHeadPack {

    private void DebugLogMain(String message) {
        if (EBDMain.EBDSettings.isDebugEnabled()) {
            SPGlobal.log(this.getClass().getSimpleName(), message);
        }
    }

    private void DebugLogSummary(String message) {
        SPGlobal.logSpecial(EBDMain.EBDLOGS.SUMMARY, this.getClass().getSimpleName() + "_" + this.GenKeyWord + this.PartKeyWord, message);
        DebugLogMain(message);
    }
    private Mod mergerMod;
    private final boolean disableVanillaHeadparts;
    private final EBDEnums.Gender PACKGEN;
    private final EBDEnums.Headpart PACKTYPE;
    private final skyproc.HDPT.HDPT_Type PACK_HDPT_FLAG;
    private final skyproc.genenums.HeadPartFlags PACK_GEN_FLAG;
    private final String GenKeyWord;
    private final String PartKeyWord;
    private final ArrayList<RaceHeadPartStore> AllPartsByRace;
    //private ArrayList<FormID> AllHairColors;
    private final ArrayList<FormID> AllPlayableRaces;

    EBDHeadPack(Mod merger, EBDEnums.Gender GEN, EBDEnums.Headpart HEADPART, boolean disableVanilla) {

        DebugLogMain("Constructor");
        this.PACKGEN = GEN;
        this.PACKTYPE = HEADPART;
        this.mergerMod = merger;
        this.disableVanillaHeadparts = disableVanilla;
        this.AllPartsByRace = new ArrayList<>();
        //this.AllHairColors = new ArrayList<>();
        this.AllPlayableRaces = new ArrayList<>(npcFuncs.getAllRaces(merger.getRaces()));

        switch (this.PACKGEN) {
            case FEMALE:
                this.GenKeyWord = "Female";
                this.PACK_GEN_FLAG = skyproc.genenums.HeadPartFlags.Female;
                break;
            case MALE:
                this.GenKeyWord = "Male";
                this.PACK_GEN_FLAG = skyproc.genenums.HeadPartFlags.Male;
                break;
            default:
                this.GenKeyWord = "None";
                this.PACK_GEN_FLAG = null;
        }

        switch (this.PACKTYPE) {
            case HAIR:
                this.PartKeyWord = "Hair";
                this.PACK_HDPT_FLAG = skyproc.HDPT.HDPT_Type.Hair;
                break;
            case EYES:
                this.PartKeyWord = "Eyes";
                this.PACK_HDPT_FLAG = skyproc.HDPT.HDPT_Type.Eyes;
                break;
            case BROWS:
                this.PartKeyWord = "Brows";
                this.PACK_HDPT_FLAG = skyproc.HDPT.HDPT_Type.Eyebrows;
                break;
            case SCARS:
                this.PartKeyWord = "Scars";
                this.PACK_HDPT_FLAG = skyproc.HDPT.HDPT_Type.Scar;
                break;
            case BEARD:
                this.PartKeyWord = "Beards";
                this.PACK_HDPT_FLAG = skyproc.HDPT.HDPT_Type.Facial_Hair;
                break;
            default:
                this.PartKeyWord = "None";
                this.PACK_HDPT_FLAG = null;
        }

        setupPartsByRace();
        HashSet<FormID> AllParts = new HashSet<>();
        this.AllPartsByRace.stream().forEach((store) -> {
            AllParts.addAll(store.getHairIDs());
        });
        DebugLogSummary("HeadPack was created. It is valid for " + Integer.toString(this.AllPlayableRaces.size())
                + " races and contains " + Integer.toString(AllParts.size()) + " headparts.");
    }


    private void setupPartsByRace() {
        this.AllPlayableRaces.stream().map((raceID) -> new RaceHeadPartStore(raceID)).forEach((tmpStore) -> {
            this.AllPartsByRace.add(tmpStore);
        });

        DebugLogMain("StartSetup");
        DebugLogMain("Number Headparts: " + Integer.toString(this.mergerMod.getHeadParts().size()));

        mainLoop:
        for (HDPT hdpt : this.mergerMod.getHeadParts()) {
            if (hdpt.getHeadPartFlag(HeadPartFlags.Playable) && !hdpt.getHeadPartFlag(HeadPartFlags.Is_Extra_Part) && hdpt.getHDPT_Type().equals(this.PACK_HDPT_FLAG)) {
                //DebugLogMain(this.PartKeyWord + " " + hdpt.getEDID() + "; " + hdpt.getForm().getFormStr());
                if (hdpt.getHeadPartFlag(this.PACK_GEN_FLAG)
                        || (!hdpt.getHeadPartFlag(HeadPartFlags.Male) && !hdpt.getHeadPartFlag(HeadPartFlags.Female))) {  //allow headparts with no gender to be valid for both genders
                    if (this.disableVanillaHeadparts && EBDUtilityFuncs.npcFuncs.isSkyrimFormID(hdpt.getForm())) {  //check if vanilla headpart and if it is therefore disabled
                        DebugLogMain(hdpt.getEDID() + " originates from vanilla Skyrim and is skipped.");
                        continue;
                    }
                    for (String s : EBDTextFileParser.getHeadPartEDIDBlacklist()) {
                        if (s.startsWith("\"") && s.endsWith("\"")) {  //check for exact match inside double quotes
                            String sn = s.replace("\"", "");
                            if (hdpt.getEDID().toLowerCase().matches(sn.toLowerCase())) {
                                DebugLogMain(hdpt.getEDID() + " is blocked in the blockfile and is skipped. It matches the keyword \"" + s + "\".");
                                continue mainLoop;

                            }
                            continue;    //do not check for containing if exact match is forced
                        }
                        if (hdpt.getEDID().toLowerCase().contains(s.toLowerCase())) {
                            DebugLogMain(hdpt.getEDID() + " is blocked in the blockfile and is skipped. It contains the keyword \"" + s + "\".");
                            continue mainLoop;
                        }
                    }
                    DebugLogMain(this.GenKeyWord + this.PartKeyWord + ": " + "found part: " + hdpt.getEDID());
                    FormID raceList = hdpt.getResourceList();
                    if (raceList != null && !raceList.isNull()) {
                        FLST validRaces = (FLST) mergerMod.getMajor(raceList, GRUP_TYPE.FLST);
                        for (RaceHeadPartStore store : this.AllPartsByRace) {
                            if (validRaces != null && validRaces.getFormIDEntries() != null) {      //Collections.disjoint is false if both collection contain at least one common entry
                                if (validRaces.getFormIDEntries().contains(store.getRaceID()) || !Collections.disjoint(validRaces.getFormIDEntries(), store.getAdditionalRaces())) {
                                    store.addHairID(hdpt.getForm());
                                    //DebugLogMain(this.GenKeyWord + this.PartKeyWord + ": " + hdpt.getEDID() + " is added to race " + store.getRaceID().getTitle());
                                }

                            } else {
                                DebugLogSummary("Warning: Found a problem with the valid races of a headpart. EDID: " + hdpt.getEDID() + ". FormID: " + hdpt.getFormStr() + ". This headpart will be ignored by EBD.");
                                break;
                            }
                        }
                    } else {
                        DebugLogSummary("Warning: Found a headpart which has no valid races set. EDID: " + hdpt.getEDID() + ". FormID: " + hdpt.getFormStr() + ". This headpart will be ignored by EBD.");
                        break;
                    }
                }
            }
        }
    }

    public ArrayList<FormID> getAllRaces() {
        ArrayList<FormID> tmpList = new ArrayList<>();
        this.AllPartsByRace.stream().filter((tmpStore) -> (tmpStore.hasPart())).forEach((tmpStore) -> {
            tmpList.add(tmpStore.getRaceID());
        });
        return tmpList;
    }

    public ArrayList<FormID> getHeadPartsByRace(FormID race) {

        for (RaceHeadPartStore store : this.AllPartsByRace) {
            if (race.equals(store.getRaceID())) {
                if (store.hasPart()) {
                    return store.getHairIDs();
                }
            }

        }
        return null;
    }

    public ArrayList<FormID> getHeadPartsForNPC(NPC_ npc) {

        for (RaceHeadPartStore store : this.AllPartsByRace) {
            if (npc.getRace().equals(store.getRaceID())) {
                if (store.hasPart()) {
                    return store.getHairIDs();
                }
            }

        }
        return null;
    }

    public int getHighestHeadPartCount() {
        int size = 0;
        for (RaceHeadPartStore store : this.AllPartsByRace) {
            if (store.getHairIDs().size() > size) {
                size = store.getHairIDs().size();
            }
        }
        return size;
    }

    public FormID getRandomHeadPart(NPC_ npc) {

        for (RaceHeadPartStore store : this.AllPartsByRace) {
            if (npc.getRace().equals(store.getRaceID())) {
                if (store.hasPart()) {
                    return store.getRandomHair();
                }
            }
        }
        return null;
    }

    private class RaceHeadPartStore {
        
        private final EBDRandomFunctions rnd;
        private final FormID raceID;
        private final ArrayList<FormID> partIDs;
        private final ArrayList<FormID> additionalRaces; //assign vanilla races to custom races to get their headparts
        final EBDHeadRaceJSON headJson;

        public RaceHeadPartStore(FormID race) {
            this.rnd = new EBDRandomFunctions();
            this.raceID = race;
            this.partIDs = new ArrayList<>();
            this.headJson = new EBDHeadRaceJSON();
            this.additionalRaces = this.headJson.getRaceList(race);
        }

        public void addHairID(FormID newHairID) {
            if (!this.partIDs.contains(newHairID)) {
                this.partIDs.add(newHairID);
            }
        }
        
        public ArrayList<FormID> getAdditionalRaces() {
            return this.additionalRaces;
        }

        public boolean hasPart() {
            return this.partIDs.size() > 1;
        }

        public FormID getRaceID() {
            return this.raceID;
        }

        public ArrayList<FormID> getHairIDs() {
            return this.partIDs;
        }

        public FormID getRandomHair() {
            if (this.hasPart()) {
                return partIDs.get(rnd.RandomInt(0, this.partIDs.size() - 1));
            } else {
                return null;
            }
        }
    }
}

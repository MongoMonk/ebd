/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import skyproc.SkyProcSave;

/**
 *
 * @author Justin Swanson
 */
public class EBDSaveFile extends SkyProcSave {

    @Override
    protected void initSettings() {
        //  The Setting,	    The default value,	    Whether or not it changing means a new patch should be made
        Add(Settings.IMPORT_AT_START, true, false);
        Add(Settings.ALL_MODS_MASTERS, false, false);
        Add(Settings.BODY_HEIGHT, false, true);
        Add(Settings.BODY_TEXTURES_MESHES, false, true);
        Add(Settings.BODY_TEX_MESH_FEMALE, false, true);
        Add(Settings.BODY_TEX_MESH_MALE, false, true);
        Add(Settings.BODY_TEX_MESH_DISABLE_UNIQUES, false, true);
        Add(Settings.BODY_TEX_MESH_FEMALE_KHAJIIT, false, true);
        Add(Settings.BODY_TEX_MESH_MALE_KHAJIIT, false, true);
        Add(Settings.BODY_TEX_MESH_FEMALE_ARGONIAN, false, true);
        Add(Settings.BODY_TEX_MESH_MALE_ARGONIAN, false, true);
        Add(Settings.BODY_TEX_OVERWRITE_CUSTOM, false, true);
        Add(Settings.FACE_TEX_COMP_SLIDER, false, true);
        Add(Settings.DEBUG, false, false);
        //Add(Settings.DEBUG_SCRIPT, false, true);
        Add(Settings.BODY_HEAD, false, true);
        Add(Settings.ANIM_FEMALE, false, true);
        Add(Settings.HEAD_DISABLE_UNIQUES, false, true);
        Add(Settings.HEAD_DISABLE_ELDER, true, true);
        Add(Settings.HEAD_DISABLE_IN_COMBAT, true, true);
        Add(Settings.HEAD_SPELLS, false, true);
        Add(Settings.HEAD_SPELLS_DEBUG, false, true);

        Add(Settings.HEIGHT_FEMALE_OFFSET, -2, true);
        Add(Settings.HEIGHT_DISABLE_UNIQUES, false, true);
        Add(Settings.HEIGHT_Nord_MIN, 103, true);
        Add(Settings.HEIGHT_Nord_MAX, 105, true);
        Add(Settings.HEIGHT_Breton_MIN, 96, true);
        Add(Settings.HEIGHT_Breton_MAX, 100, true);
        Add(Settings.HEIGHT_Imperial_MIN, 98, true);
        Add(Settings.HEIGHT_Imperial_MAX, 102, true);
        Add(Settings.HEIGHT_Redguard_MIN, 101, true);
        Add(Settings.HEIGHT_Redguard_MAX, 103, true);
        Add(Settings.HEIGHT_Orc_MIN, 104, true);
        Add(Settings.HEIGHT_Orc_MAX, 107, true);
        Add(Settings.HEIGHT_HighElf_MIN, 105, true);
        Add(Settings.HEIGHT_HighElf_MAX, 108, true);
        Add(Settings.HEIGHT_WoodElf_MIN, 98, true);
        Add(Settings.HEIGHT_WoodElf_MAX, 102, true);
        Add(Settings.HEIGHT_DarkElf_MIN, 99, true);
        Add(Settings.HEIGHT_DarkElf_MAX, 103, true);
        Add(Settings.HEIGHT_Argonian_MIN, 100, true);
        Add(Settings.HEIGHT_Argonian_MAX, 104, true);
        Add(Settings.HEIGHT_Khajiit_MIN, 102, true);
        Add(Settings.HEIGHT_Khajiit_MAX, 105, true);
        Add(Settings.HEIGHT_Elder_MIN, 97, true);
        Add(Settings.HEIGHT_Elder_MAX, 100, true);
        Add(Settings.HEIGHT_Other_MIN, 98, true);
        Add(Settings.HEIGHT_Other_MAX, 102, true);

        Add(Settings.CONSISTENCY, true, true);
    }

    @Override
    protected void initHelp() {

        helpInfo.put(Settings.IMPORT_AT_START,
                "If enabled, the patcher will begin importing your mods when the program starts.\n\n"
                + "If turned off, the program will wait until you click patch before importing.\n\n"
                + "This setting has to be enabled in order for the TexturePack Editor and for the Custom Race Editor to work.\n\n"
                + "NOTE: The setting will not take effect until the next time the patcher is run.\n\n"
                + "Benefits:\n"
                + "- Slightly faster patching.\n"
                + "- More information displayed in GUI, as it will have access to the records from your mods."
                + "\n\n"
                + "Downsides:\n"
                + "- Having this on might make the GUI respond sluggishly while it processes in the "
                + "background.");

        helpInfo.put(Settings.ALL_MODS_MASTERS,
                "If enabled, the patcher will add all mods from your load as masters.\n\n"
                + "Benefits:\n"
                + "- Significantly faster patching.\n"
                + "\n\n"
                + "Downsides:\n"
                + "- Every time you remove one single mod from your load order (even unrelated ones to EBD) you will have to rerun the patcher.");

        helpInfo.put(Settings.BODY_HEIGHT,
                "Change body height of npcs based on some predefined per-race values. Refer to readme for more information.\n\n"
                + "NOTE: This feature DOES NOT require the helper script. EBD remains script free when using it.");

        helpInfo.put(Settings.BODY_HEAD,
                "Disable/Enable changing headparts like hair/eyes/scars/brows/beards. Takes precedence over any settings you do in the Head Settings Menu.\n\n"
                + "Even if you rerun the patcher with a different mod setup headparts will stay the same for each NPC. As long as you did not remove the mod containing the assigned headparts, of course-.\n\n"
                + "Note that by default all headparts are turned on with a chance of 50% per NPC of appearing. You may disable some manually or change their probability in the Head Settings menu.\n\n"
                + "Enabling this will also enable the helper script, so EBD won't be entirely script free anymore.\n"
                + "It is very lightweight but performance of the script varies with amount of enabled features & installed mods.\n\n"
                + "Even with the script EBD is very safe to uninstall but please go somewhere inside with few NPCs if you plan on uninstalling.\n\n"
                + "REQUIRES SKSE 1.06.16 or higher AND JContainers 3.3 or higher!");

        helpInfo.put(Settings.BODY_TEX_MESH_FEMALE,
                "Change the textures (body and face, if present) of female humanoid NPCs.\n\n"
                + "All non-beast races are humanoid, i.e. Elves, Nords, Orcs, Bretons, etc.\n\n"
                + "This will also change the female meshes if they exist.\n\n");

        helpInfo.put(Settings.BODY_TEX_MESH_FEMALE_ARGONIAN,
                "Change the textures (body and face, if present) of female Argonian NPCs.\n\n"
                + "This will also change the male meshes if they exist.\n\n"
                + "Be sure to check Force Patch on Exit in the upper right corner (if possible).\n\n");
        helpInfo.put(Settings.BODY_TEX_MESH_FEMALE_KHAJIIT,
                "Change the textures (body and face, if present) of female Khajiit NPCs.\n\n"
                + "This will also change the male meshes if they exist.\n\n"
                + "Be sure to check Force Patch on Exit in the upper right corner (if possible).\n\n");

        helpInfo.put(Settings.BODY_TEXTURES_MESHES,
                "Disable/Enable all texture/mesh related settings. Takes precedence over any settings you do in the Texture/Mesh Settings Menu.\n\n"
                + "Each time you run the patcher every NPC will receive a different texture/mesh.\n\n"
                + "Note that the mesh feature is only a addon to the texture feature. If no meshes are setup by you then the mesh feature is completely disabled without bothering you.\n"
                + "So if you don't want to use the mesh feature just ignore it.\n\n"
                + "Enabling this will also enable the helper script, so EBD won't be entirely script free anymore.\n\n"
                + "It is very lightweight, especially if you're not using the headpart feature.\n\n"
                + "The script is very safe to uninstall but please go somewhere inside with few NPCs if you plan on uninstalling.\n\n"
                + "REQUIRES SKSE 1.06.16 or higher!");

        helpInfo.put(Settings.BODY_TEX_MESH_DISABLE_UNIQUES,
                "Disable all options on this page for unique NPCs. This means important named NPCs will not be affected by the texture/mesh feature.\n\n"
                + "Only generic NPCs will have their textures/meshes changed.");

        helpInfo.put(Settings.BODY_TEX_MESH_MALE,
                "Change the textures (body and face, if present) of male Humanoid NPCs.\n\n"
                + "All non-beast races are humanoid, i.e. Elves, Nords, Orcs, Bretons, etc.\n\n"
                + "This will also change the male meshes if they exist.\n\n");
        helpInfo.put(Settings.BODY_TEX_MESH_MALE_ARGONIAN,
                "Change the textures (body and face, if present) of male Argonian NPCs.\n\n"
                + "This will also change the male meshes if they exist.\n\n"
                + "Be sure to check Force Patch on Exit in the upper right corner (if possible).\n\n");
        helpInfo.put(Settings.BODY_TEX_MESH_MALE_KHAJIIT,
                "Change the textures (body and face, if present) of male Khajiit NPCs.\n\n"
                + "This will also change the male meshes if they exist.\n\n"
                + "Be sure to check Force Patch on Exit in the upper right corner (if possible).\n\n");
        
        helpInfo.put(Settings.BODY_TEX_OVERWRITE_CUSTOM,
                "If an NPC already has a custom body or body texture it will not receive any EBD textures by default.\n\n"
                + "Enabling this setting means that EBD will overwrite the changes done by any mod and apply its own body textures/body mesh.\n\n"
                + "This is useful if you don't want to use the custom bodies/textures which are sometimes provided for follower mods or by NPC visual overhauls.\n\n");
        
        helpInfo.put(Settings.FACE_TEX_COMP_SLIDER,
                "Face Textures for the Player Character\n\n"
                + "BONUS Feature:\n"
                + "Adds all the face textures the patcher finds to the complexion slider ingame.\n\n"
                + "You can use the complexion slider in the Race Menu to choose between the different face textures for your player character.\n\n"
                + "NOTE: This is a beta and a bonus feature. Do not be surprised if it doesn't work. I won't invest much work into this. It's just a small extra for you to play around with.\n\n"
                + "If you have problems with it then just disable it.\n\n"
                + "Won't work for custom races and vampires."
                + "NOTE: This feature DOES NOT require the helper script. EBD remains script free when using it.");

        helpInfo.put(Settings.ANIM_FEMALE,
                "Sets all females to use female animations. There are some female NPCs (like Housecarls) which use male animations. "
                + "Enabling this means they'll use female animations from now on. This will also allow Orc females to use female animations. \n\n"
                + "Please note: some females using opposite gender animations is (I think) a design choice by Bethesda. So this is not a \"fix\", it is just a matter of personal preference.\n\n"
                + "IMPORTANT: You might have to start a new game to see the effect for females you have already encountered. \n"
                + "Furthermore it is possible that the female animations stick for NPCs you've met even when you disable this feature. Use with care.\n\n"
                + "NOTE: This feature DOES NOT require the helper script. EBD remains script free when using it.");

        helpInfo.put(Settings.DEBUG,
                "Enable debug output to log files.");

//        helpInfo.put(Settings.DEBUG_SCRIPT,
//                "Enable debug output in the Face Fix Script.");
        helpInfo.put(Settings.OTHER_SETTINGS,
                "These are other patcher related settings.");

        helpInfo.put(Settings.TEXTURE_MESH_SETTINGS,
                "These are the texture and mesh related settings of the EBD patcher.");

        helpInfo.put(Settings.MAIN_SETTINGS,
                "These are the main npc related settings of the EBD patcher.");

        helpInfo.put(Settings.HEAD_SETTINGS,
                "These are the head (eyes, hair) related settings of the EBD patcher.\n\n"
                + "IMPORTANT: Enable the helper script or features on this page WILL NOT WORK at all.");

        helpInfo.put(Settings.HEAD_DISABLE_UNIQUES,
                "Disable all options on this page for unique NPCs. This means important named NPCs will not be affected by the headpart feature. "
                + "Only generic NPCs will have their headparts changed.");
        helpInfo.put(Settings.HEAD_DISABLE_ELDER,
                "Disable all options on this page for elderly NPCs. This means old NPCs will not be affected by the changes on this page.\n\n"
                + "Useful to reduce stutter/script load if you don't care about old NPCs.\n\n");
        helpInfo.put(Settings.HEAD_DISABLE_IN_COMBAT,
                "Disable all options on this page for NPCs which are in combat. This reduces engine load in combat situations and might improve stability.\n\n"
                + "Recommended if you run NPC adding mods which have big fights in the wilds. When you meet the NPCs again & they are out of combat the script will run again on them.\n\n");

        helpInfo.put(Settings.HEAD_SPELLS,
                "Add the customizer spell to the player's powers."
                + "It is highly recommended to enable this setting to control an NPC's headparts ingame.\n\n"
                + "The EBDCustomizer spell allows you to give a NPC completely new headparts, like hair, eyes, etc. in game. Espcially useful if you don't like the randomly selected ones.\n"
                + "It also allows to exclude specfic NPCs from certain (or all) headparts altogether. You can also force enable NPCs, so that they get new headparts irrespective of the change probability you set.\n\n"
                + "Note: the EBDCustomizer spell REQUIRES you to have UIExtensions OR Extensible Follower Framework (EFF) installed or it won't be enabled.");

        helpInfo.put(Settings.HEAD_SPELLS_DEBUG,
                "Add the debug spell to the player's powers."
                + "The EBDDebug spell prints the used headparts of an NPC to the console and log, reapplies face tints and allows you to visually inspect a NPC.\n\n"
                + "Enabling this spell is highly optional.");

        helpInfo.put(Settings.HEIGHT_SETTINGS,
                "These are the race height related settings. Please note that 100 is the default value for NPCs. Setting the Maximum to, let's say, 110 creates already quite tall NPCs while 90 is pretty small.\n\n"
                + "In general I'd not recommend to set the Min/Max difference of any race higher than 10. Except of course you like very noticeable height differences.\n\n"
                + "Please note: The height of any NPC will only be changed if its original value equals the default value of 100. This means NPCs already altered by mods (or NPCs with unusual heights) will not be modified.");

        helpInfo.put(Settings.HEIGHT_FEMALE_OFFSET,
                "This value determines whether females should be smaller or taller than males. It is added (if positive) or substracted (if negative) from the race height values you set below.\n"
                + "Set to 0 to disable.\n\n"
                + "Example if set to -2:\n"
                + "Let's assume you've set Nord Minimum Height to 103 and Nord Maximum Height to 105. Female Nords will now have 101 as Minimum Height and 103 as Maximum Height.\n\n"
                + "Default value: -2");

        helpInfo.put(Settings.HEIGHT_DISABLE_UNIQUES,
                "Disable all options on this page for unique NPCs. This means important named NPCs will not be affected by the height feature.\n\n"
                + "Only generic NPCs will have their height changed.");

        helpInfo.put(Settings.HEIGHT_Nord_MIN,
                "Nord Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Nord_MAX,
                "Nord Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Imperial_MIN,
                "Imperial Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Imperial_MAX,
                "Imperial Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Breton_MIN,
                "Breton Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Breton_MAX,
                "Breton Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Redguard_MIN,
                "Redguard Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Redguard_MAX,
                "Redguard Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Orc_MIN,
                "Orc Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Orc_MAX,
                "Orc Maximum Height.");

        helpInfo.put(Settings.HEIGHT_HighElf_MIN,
                "HighElf Minimum Height.");

        helpInfo.put(Settings.HEIGHT_HighElf_MAX,
                "HighElf Maximum Height.");

        helpInfo.put(Settings.HEIGHT_WoodElf_MIN,
                "WoodElf Minimum Height.");

        helpInfo.put(Settings.HEIGHT_WoodElf_MAX,
                "WoodElf Maximum Height.");

        helpInfo.put(Settings.HEIGHT_DarkElf_MIN,
                "DarkElf Minimum Height.");

        helpInfo.put(Settings.HEIGHT_DarkElf_MAX,
                "DarkElf Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Argonian_MIN,
                "Argonian Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Argonian_MAX,
                "Argonian Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Khajiit_MIN,
                "Khajiit Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Khajiit_MAX,
                "Khajiit Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Elder_MIN,
                "Old People Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Elder_MAX,
                "Old People Maximum Height.");

        helpInfo.put(Settings.HEIGHT_Other_MIN,
                "Other races (for example custom races) Minimum Height.");

        helpInfo.put(Settings.HEIGHT_Other_MAX,
                "Other races (for example custom races) Maximum Height.");

        helpInfo.put(Settings.CONSISTENCY,
                "Enable consistency for the height feature and the texture/mesh feature.\n\n"
                + "This means the patcher will not assign new values to an already processed NPC.\n"
                + "Instead it will import them from the last run.\n\n"
                + "It will only work if you have not changed your relevant configuration for a given feature.\n"
                + "For more information refer to the readme.");

    }

    // Note that some settings just have help info, and no actual values in
    // initSettings().
    public enum Settings {

        IMPORT_AT_START,
        ALL_MODS_MASTERS,
        BODY_HEIGHT,
        BODY_HEAD,
        BODY_TEXTURES_MESHES,
        BODY_TEX_MESH_FEMALE,
        BODY_TEX_MESH_MALE,
        BODY_TEX_MESH_DISABLE_UNIQUES,
        BODY_TEX_MESH_FEMALE_KHAJIIT,
        BODY_TEX_MESH_MALE_KHAJIIT,
        BODY_TEX_MESH_FEMALE_ARGONIAN,
        BODY_TEX_MESH_MALE_ARGONIAN,
        BODY_TEX_OVERWRITE_CUSTOM,
        FACE_TEX_COMP_SLIDER,
        DEBUG,
        HEAD_SPELLS,
        HEAD_SPELLS_DEBUG,
        ANIM_FEMALE,
        HEAD_DISABLE_UNIQUES,
        HEAD_DISABLE_ELDER,
        HEAD_DISABLE_IN_COMBAT,
        HEIGHT_FEMALE_OFFSET,
        HEIGHT_DISABLE_UNIQUES,
        HEIGHT_Nord_MIN,
        HEIGHT_Nord_MAX,
        HEIGHT_Breton_MIN,
        HEIGHT_Breton_MAX,
        HEIGHT_Imperial_MIN,
        HEIGHT_Imperial_MAX,
        HEIGHT_Redguard_MIN,
        HEIGHT_Redguard_MAX,
        HEIGHT_Orc_MIN,
        HEIGHT_Orc_MAX,
        HEIGHT_HighElf_MIN,
        HEIGHT_HighElf_MAX,
        HEIGHT_WoodElf_MIN,
        HEIGHT_WoodElf_MAX,
        HEIGHT_DarkElf_MIN,
        HEIGHT_DarkElf_MAX,
        HEIGHT_Argonian_MIN,
        HEIGHT_Argonian_MAX,
        HEIGHT_Khajiit_MIN,
        HEIGHT_Khajiit_MAX,
        HEIGHT_Elder_MIN,
        HEIGHT_Elder_MAX,
        HEIGHT_Other_MIN,
        HEIGHT_Other_MAX,
        TEXTURE_LIST_FEMALE,
        TEXTURE_LIST_MALE,
        HEIGHT_SETTINGS,
        OTHER_SETTINGS,
        MAIN_SETTINGS,
        TEXTURE_MESH_SETTINGS,
        HEAD_SETTINGS,
        CONSISTENCY
    }
}

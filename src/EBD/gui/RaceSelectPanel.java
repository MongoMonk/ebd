/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDXMLTexPackParser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import lev.gui.LComboBox;
import lev.gui.LTextField;
import skyproc.FormID;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class RaceSelectPanel extends SPSettingPanel {

    HashMap<FormID, String> raceMap;
    HashMap<FormID, String> raceNameMap;
    HashMap<LCheckBox, FormID> raceBoxes;
    ArrayList<FormID> RaceIDs;
    SPSettingPanel prevPanel;
    EBDXMLTexPackParser XmlParser;
    final String MainPackName;
    final String SubPackName;
    LCheckBox enablePackCBox;
    LComboBox<String> SubPacksBox;
    LButton goBack;
    LTextField toggleField;
    boolean isSubPackSelector;

    public RaceSelectPanel(SPMainMenuPanel parent_, String MainName, String SubName, SPSettingPanel pan, boolean isSubPackSel, EBDXMLTexPackParser parser, String title) {
        super(parent_, title, EBDMain.headerColor);
        this.MainPackName = MainName;
        this.SubPackName = SubName;
        this.prevPanel = pan;
        this.isSubPackSelector = isSubPackSel;
        this.XmlParser = parser;
        this.raceMap = EBDMain.getAllRacesAlt();
        this.raceNameMap = EBDMain.getAllRacesNames();
    }

    private class SubPackListener implements ActionListener {

        private final LComboBox box;

        SubPackListener(LComboBox b) {
            this.box = b;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            for (LCheckBox cBox : raceBoxes.keySet()) {

                XmlParser.setActiveRaceForMainPackInMemory(MainPackName, raceBoxes.get(cBox), cBox.isSelected());

            }
            XmlParser.updateXMLMainPackFromUI(MainPackName);

            RaceSelectPanel RSPanel = new RaceSelectPanel(parent, MainPackName, (String) this.box.getSelectedItem(), RaceSelectPanel.this, true, XmlParser, (String) this.box.getSelectedItem());
            parent.openPanel(RSPanel);
            RSPanel.open();

        }
    }

    private class RaceToggleListener implements ActionListener {

        private final LTextField textField;

        RaceToggleListener(LTextField field) {
            this.textField = field;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            for (LCheckBox cBox : raceBoxes.keySet()) {
                if (cBox.getName().toLowerCase().contains(this.textField.getText())) {
                    if (cBox.isSelected()) {
                        cBox.setSelected(false);
                    } else {
                        cBox.setSelected(true);
                    }
                }
            }

        }
    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        String tit = "Edit ";
        String mess = "Here you may select the valid races for the ";
        if (!this.isSubPackSelector) {
            for (LCheckBox cBox : this.raceBoxes.keySet()) {
                cBox.setSelected(this.XmlParser.isRaceActiveForMainPack(this.MainPackName, raceBoxes.get(cBox)));
            }
            tit += "MainPack";
            mess += "MainPack " + MainPackName + ".\n\n";
        } else {
            tit += "SubPack";
            mess += "SubPack " + SubPackName + " in MainPack " + MainPackName + ".\n\n";
        }
        SUMGUI.helpPanel.setTitle(tit);
        SUMGUI.helpPanel.setContent(mess
                + "Some races share the same (face) normal maps in Skyrim.\n"
                + "I list them here for the non-obvious ones:\n\n"
                + "DA13AfflictedRace same as BretonRace\n\n"
                + "DremoraRace,DLC2DremoraRace same as DarkElfRace\n\n"
                + "SnowElfRace same as HighElfRace");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();
        enablePackCBox = new LCheckBox("Active", EBDMain.settingsFont, EBDMain.settingsColor);
        if (this.isSubPackSelector) {

            enablePackCBox.setSelected(XmlParser.isSubPackActive(MainPackName, SubPackName));
        } else {
            enablePackCBox.setSelected(XmlParser.isMainPackActive(MainPackName));
        }
        enablePackCBox.setOffset(2);
        enablePackCBox.addShadow();
        enablePackCBox.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                SUMGUI.helpPanel.setTitle("Disable or enable pack.");
                SUMGUI.helpPanel.setContent("Use this button to quickly enable/disable the currently selected MainPack/SubPack.\n\n"
                        + "This way you may temporarily enable/disable certain MainPacks/SubPacks without deleting/moving them on your hard drive.");
                SUMGUI.helpPanel.focusOn(enablePackCBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(enablePackCBox);
        Add(enablePackCBox);

        if (!isSubPackSelector) {

            SubPacksBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
            SubPacksBox.addItem("None");

            SubPacksBox.setSize(250, 30);
            SubPacksBox.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    SUMGUI.helpPanel.setTitle("Select MainPack");
                    SUMGUI.helpPanel.setContent("Use this combo box to select the SubPack you want to edit.\n\n");
                    SUMGUI.helpPanel.focusOn(SubPacksBox, 0);
                }

                @Override
                public void mouseExited(MouseEvent me) {
                }

            });
            setPlacement(SubPacksBox);
            Add(SubPacksBox);

            SubPacksBox.removeAllItems();
            for (String s : this.XmlParser.getAllSubPacksByMainPack(this.MainPackName)) {

                SubPacksBox.addItem(s);

            }
            SubPackListener mp = new SubPackListener(SubPacksBox);
            SubPacksBox.addEnterButton("Set", mp);
        }

        toggleField = new LTextField("Toggle Races");
        toggleField.setFocusable(true);
        RaceToggleListener raceListen = new RaceToggleListener(toggleField);
        toggleField.addEnterButton("Toggle", raceListen);
        toggleField.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Toggle Races");
                SUMGUI.helpPanel.setContent("Use this text field to quickly enable/disable certain races.\n\n"
                        + "Just enter part of the name of the races you want to toggle and click \"Toggle\".\n\n"
                        + "For example enter \"elf\" to quickly toggle all elven races or enter \"vampire\" to toggle all vampire races.\n\n"
                        + "Leave blank to toggle all races.");
                SUMGUI.helpPanel.focusOn(toggleField, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });

        setPlacement(toggleField);
        Add(toggleField);

        this.raceBoxes = new HashMap<>();
        if (!this.isSubPackSelector) {
            this.RaceIDs = this.XmlParser.getAllRacesForMainPack(this.MainPackName);
        } else {
            this.RaceIDs = this.XmlParser.getAllRacesForSubPack(this.MainPackName, this.SubPackName);
        }
        for (FormID race : this.RaceIDs) {
            //System.out.print(this.jComboBox1.getItemAt(i).toString());
            final LCheckBox tmpBox = new LCheckBox(this.raceMap.get(race), EBDMain.settingsFont, EBDMain.settingsColor);
            if (!this.isSubPackSelector) {
                tmpBox.setSelected(this.XmlParser.isRaceActiveForMainPack(this.MainPackName, race));
            } else {
                tmpBox.setSelected(this.XmlParser.isRaceActiveForSubPack(this.MainPackName, this.SubPackName, race));
            }
            this.raceBoxes.put(tmpBox, race);
            tmpBox.setOffset(2);
            tmpBox.addShadow();
            setPlacement(tmpBox);
            Add(tmpBox);
            tmpBox.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    SUMGUI.helpPanel.setTitle("Enable/Disable Race");
                    SUMGUI.helpPanel.setContent("Use this check box to enable/disable the " + raceMap.get(race) + " (" + raceNameMap.get(race) + ").");
                    SUMGUI.helpPanel.focusOn(toggleField, 0);
                }

                @Override
                public void mouseExited(MouseEvent me) {
                }

            });
        }

        goBack = new LButton("Save Settings");
        goBack.setFocusable(true);
        goBack.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (LCheckBox cBox : raceBoxes.keySet()) {
                    if (!isSubPackSelector) {
                        XmlParser.setActiveRaceForMainPackInMemory(MainPackName, raceBoxes.get(cBox), cBox.isSelected());
                    } else {
                        XmlParser.setActiveRaceForSubPackInMemory(MainPackName, SubPackName, raceBoxes.get(cBox), cBox.isSelected());
                    }
                }
                if (!isSubPackSelector) {
                    XmlParser.setMainPackActive(MainPackName, enablePackCBox.isSelected());
                } else {
                    XmlParser.setSubPackActive(MainPackName, SubPackName, enablePackCBox.isSelected());
                }
                XmlParser.updateXMLMainPackFromUI(MainPackName);
                if (isSubPackSelector) {
                    prevPanel.open();
                }
                parent.openPanel(prevPanel);

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Save Selected Races");
                SUMGUI.helpPanel.setContent("Use this button to save changes you made to the valid races.\n\n"
                        + "Clicking this button will take you back to the previous panel.");
                SUMGUI.helpPanel.focusOn(goBack, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(goBack);
        Add(goBack);

        alignRight();

    }
}

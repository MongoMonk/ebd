/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDSaveFile;
import lev.gui.LCheckBox;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class OtherSettingsPanel extends SPSettingPanel {

    LCheckBox importOnStartup;
    LCheckBox allModsAsMasters;
    LCheckBox Debug;
    LCheckBox DebugScript;

    public OtherSettingsPanel(SPMainMenuPanel parent_) {
        super(parent_, "Other Settings", EBDMain.headerColor);
    }

    @Override
    protected void initialize() {
        super.initialize();

        importOnStartup = new LCheckBox("Import Mods on Startup", EBDMain.settingsFont, EBDMain.settingsColor);
        importOnStartup.tie(EBDSaveFile.Settings.IMPORT_AT_START, EBDMain.save, SUMGUI.helpPanel, true);
        importOnStartup.setOffset(2);
        importOnStartup.addShadow();
        setPlacement(importOnStartup);
        AddSetting(importOnStartup);

        allModsAsMasters = new LCheckBox("Add all mods as masters", EBDMain.settingsFont, EBDMain.settingsColor);
        allModsAsMasters.tie(EBDSaveFile.Settings.ALL_MODS_MASTERS, EBDMain.save, SUMGUI.helpPanel, true);
        allModsAsMasters.setOffset(2);
        allModsAsMasters.addShadow();
        setPlacement(allModsAsMasters);
        AddSetting(allModsAsMasters);

        Debug = new LCheckBox("Enable Debug", EBDMain.settingsFont, EBDMain.settingsColor);
        Debug.tie(EBDSaveFile.Settings.DEBUG, EBDMain.save, SUMGUI.helpPanel, true);
        Debug.setOffset(2);
        Debug.addShadow();
        setPlacement(Debug);
        AddSetting(Debug);

//        DebugScript = new LCheckBox("Enable Debug in Script", EBDMain.settingsFont, EBDMain.settingsColor);
//        DebugScript.tie(EBDSaveFile.Settings.DEBUG_SCRIPT, EBDMain.save, SUMGUI.helpPanel, true);
//        DebugScript.setOffset(2);
//        DebugScript.addShadow();
//        setPlacement(DebugScript);
//        AddSetting(DebugScript);
        alignRight();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDSaveFile;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class TextureMeshSettingsPanel extends SPSettingPanel {

    LCheckBox bodyTexturesFemale;
    LCheckBox bodyTexturesFemaleKhajiit;
    LCheckBox bodyTexturesFemaleArgonian;
    LCheckBox bodyTexturesMale;
    LCheckBox bodyTexturesMaleKhajiit;
    LCheckBox bodyTexturesMaleArgonian;
    LCheckBox DisableUniqueNPCs;
    LCheckBox OverwriteCustomBody;
    LCheckBox AddFaceTexToComplexions;
    LButton TexturePackEditor;

    public TextureMeshSettingsPanel(SPMainMenuPanel parent_) {
        super(parent_, "Texture/Mesh Settings", EBDMain.headerColor);
    }

    @Override
    protected void initialize() {
        super.initialize();

        bodyTexturesFemale = new LCheckBox("Female Humanoid", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesFemale.tie(EBDSaveFile.Settings.BODY_TEX_MESH_FEMALE, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesFemale.setOffset(2);
        bodyTexturesFemale.addShadow();
        setPlacement(bodyTexturesFemale);
        AddSetting(bodyTexturesFemale);

        bodyTexturesFemaleKhajiit = new LCheckBox("Female Khajiit", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesFemaleKhajiit.tie(EBDSaveFile.Settings.BODY_TEX_MESH_FEMALE_KHAJIIT, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesFemaleKhajiit.setOffset(2);
        bodyTexturesFemaleKhajiit.addShadow();
        setPlacement(bodyTexturesFemaleKhajiit);
        AddSetting(bodyTexturesFemaleKhajiit);

        bodyTexturesFemaleArgonian = new LCheckBox("Female Argonian", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesFemaleArgonian.tie(EBDSaveFile.Settings.BODY_TEX_MESH_FEMALE_ARGONIAN, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesFemaleArgonian.setOffset(2);
        bodyTexturesFemaleArgonian.addShadow();
        setPlacement(bodyTexturesFemaleArgonian);
        AddSetting(bodyTexturesFemaleArgonian);

        bodyTexturesMale = new LCheckBox("Male Humanoid", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesMale.tie(EBDSaveFile.Settings.BODY_TEX_MESH_MALE, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesMale.setOffset(2);
        bodyTexturesMale.addShadow();
        setPlacement(bodyTexturesMale);
        AddSetting(bodyTexturesMale);

        bodyTexturesMaleKhajiit = new LCheckBox("Male Khajiit", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesMaleKhajiit.tie(EBDSaveFile.Settings.BODY_TEX_MESH_MALE_KHAJIIT, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesMaleKhajiit.setOffset(2);
        bodyTexturesMaleKhajiit.addShadow();
        setPlacement(bodyTexturesMaleKhajiit);
        AddSetting(bodyTexturesMaleKhajiit);

        bodyTexturesMaleArgonian = new LCheckBox("Male Argonian", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexturesMaleArgonian.tie(EBDSaveFile.Settings.BODY_TEX_MESH_MALE_ARGONIAN, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexturesMaleArgonian.setOffset(2);
        bodyTexturesMaleArgonian.addShadow();
        setPlacement(bodyTexturesMaleArgonian);
        AddSetting(bodyTexturesMaleArgonian);

        DisableUniqueNPCs = new LCheckBox("Disable Unique NPCs", EBDMain.settingsFont, EBDMain.settingsColor);
        DisableUniqueNPCs.tie(EBDSaveFile.Settings.BODY_TEX_MESH_DISABLE_UNIQUES, EBDMain.save, SUMGUI.helpPanel, true);
        DisableUniqueNPCs.setOffset(2);
        DisableUniqueNPCs.addShadow();
        setPlacement(DisableUniqueNPCs);
        AddSetting(DisableUniqueNPCs);
        
        OverwriteCustomBody = new LCheckBox("Overwrite Custom NPCs", EBDMain.settingsFont, EBDMain.settingsColor);
        OverwriteCustomBody.tie(EBDSaveFile.Settings.BODY_TEX_OVERWRITE_CUSTOM, EBDMain.save, SUMGUI.helpPanel, true);
        OverwriteCustomBody.setOffset(2);
        OverwriteCustomBody.addShadow();
        setPlacement(OverwriteCustomBody);
        AddSetting(OverwriteCustomBody);
        
        AddFaceTexToComplexions = new LCheckBox("Face Textures for PC", EBDMain.settingsFont, EBDMain.settingsColor);
        AddFaceTexToComplexions.tie(EBDSaveFile.Settings.FACE_TEX_COMP_SLIDER, EBDMain.save, SUMGUI.helpPanel, true);
        AddFaceTexToComplexions.setOffset(2);
        AddFaceTexToComplexions.addShadow();
        setPlacement(AddFaceTexToComplexions);
        AddSetting(AddFaceTexToComplexions);

        TexturePackEditor = new LButton("TexturePack Editor");
        TexturePackEditor.setFocusable(true);
        TexturePackEditor.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                TexturePackEditor TPPanel = new TexturePackEditor(parent);
                parent.openPanel(TPPanel);
                TPPanel.open();

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("TexturePack Editor");
                SUMGUI.helpPanel.setContent("Open the TexturePack Editor.\n\n"
                        + "Wait until the patcher has finished importing your load order before opening the editor!");
                SUMGUI.helpPanel.focusOn(TexturePackEditor, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        setPlacement(TexturePackEditor);

        Add(TexturePackEditor);

        alignRight();
    }

}

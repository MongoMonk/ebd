/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedHashMap;
import lev.gui.LButton;
import lev.gui.LComboBox;
import skyproc.FormID;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeadPartCustomRacePanel extends SPSettingPanel {

    SPSettingPanel prevPanel;
    LComboBox<String> RaceBox;
    final String Title;
    LinkedHashMap<String, FormID> custRaceEDIDFormMap;
    LinkedHashMap<String, FormID> raceEDIDFormMap;

    LButton buttonLoad;
    LButton goBack;

    public HeadPartCustomRacePanel(SPMainMenuPanel parent_, SPSettingPanel pan) {
        super(parent_, "Custom Race Editor", EBDMain.headerColor);
        this.prevPanel = pan;
        this.Title = "Custom Race Editor";
        this.custRaceEDIDFormMap = new LinkedHashMap<>();
        this.raceEDIDFormMap = new LinkedHashMap<>();
    }

    private class RaceListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            HeadPartCustomRaceEditor EditPanel = new HeadPartCustomRaceEditor(parent, HeadPartCustomRacePanel.this, RaceBox.getSelectedItem(), custRaceEDIDFormMap.get(RaceBox.getSelectedItem()), raceEDIDFormMap);
            parent.openPanel(EditPanel);
            EditPanel.open();

        }
    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        SUMGUI.helpPanel.setTitle(this.Title);
        SUMGUI.helpPanel.setContent(
                "Here you may assign custom races to vanilla races in order to enable headpart distribution for them.\n\n"
                + "By default custom races don't have any valid headparts."
                + "The editor allows you enable distribution of headparts to custom (and DLC) races.\n\n"
                + "This is achieved by treating the custom race as a vanilla Skyrim race you select in the editor.\n\n"
                + "Default settings for the DLC races are included."
                + "Wait until the patcher has finished importing your load order before opening the editor!");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();
        RaceBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        //RaceBox.addItem("HUHU");

        RaceBox.setSize(250, 30);
        RaceBox.addItem("Click on \"Load Custom Races\"");
        RaceBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Custom Races");
                SUMGUI.helpPanel.setContent("Use this combo box to select the race you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(RaceBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(RaceBox);
        Add(RaceBox);

        buttonLoad = new LButton("Load Custom Races");
        buttonLoad.setFocusable(true);
        buttonLoad.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                custRaceEDIDFormMap = EBDMain.getCustomRaces();
                raceEDIDFormMap = EBDMain.getVanillaRaces();
                RaceBox.removeAllItems();
                boolean foundOne = false;
                for (String race : custRaceEDIDFormMap.keySet()) {
                    RaceBox.addItem(race);
                    foundOne = true;
                }
                if (!foundOne) {
                    RaceBox.addItem("No Custom Races Found!");
                }
                RaceListener rl = new RaceListener();
                RaceBox.addEnterButton("Set", rl);
                //buttonLoad.setVisible(false);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                buttonLoad.setText("LOADING...");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                buttonLoad.setText("Load Custom Races");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Load Custom Races");
                SUMGUI.helpPanel.setContent("Wait until the patcher has finished importing & then press this button to load the custom races.");
                SUMGUI.helpPanel.focusOn(buttonLoad, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(buttonLoad);
        Add(buttonLoad);

        goBack = new LButton("Go Back");
        goBack.setFocusable(true);
        goBack.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                parent.openPanel(prevPanel);

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Go Bacl");
                SUMGUI.helpPanel.setContent("Use this button to go back to the previous panel.");
                SUMGUI.helpPanel.focusOn(goBack, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(goBack);
        Add(goBack);

        alignRight();

    }
}

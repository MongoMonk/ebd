/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDSaveFile;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import lev.gui.LNumericSetting;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class MainSettingsPanel extends SPSettingPanel {

    LCheckBox bodyHead;
    LNumericSetting bodyWeightOffset;
    LCheckBox bodyHeight;
    LCheckBox bodyTexMesh;
    LCheckBox femaleAnimations;
    LCheckBox enableConsistency;
    LNumericSetting bodyColorOffset;
    LButton openBlockList;

    public MainSettingsPanel(SPMainMenuPanel parent_) {
        super(parent_, "Main Settings", EBDMain.headerColor);
    }

    @Override
    protected void initialize() {
        super.initialize();


        bodyHeight = new LCheckBox("Change Height", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyHeight.tie(EBDSaveFile.Settings.BODY_HEIGHT, EBDMain.save, SUMGUI.helpPanel, true);
        bodyHeight.setOffset(2);
        bodyHeight.addShadow();
        setPlacement(bodyHeight);
        AddSetting(bodyHeight);

        bodyHead = new LCheckBox("Change Headparts", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyHead.tie(EBDSaveFile.Settings.BODY_HEAD, EBDMain.save, SUMGUI.helpPanel, true);
        bodyHead.setOffset(2);
        bodyHead.addShadow();
        setPlacement(bodyHead);
        AddSetting(bodyHead);

        bodyTexMesh = new LCheckBox("Change Textures/Meshes", EBDMain.settingsFont, EBDMain.settingsColor);
        bodyTexMesh.tie(EBDSaveFile.Settings.BODY_TEXTURES_MESHES, EBDMain.save, SUMGUI.helpPanel, true);
        bodyTexMesh.setOffset(2);
        bodyTexMesh.addShadow();
        setPlacement(bodyTexMesh);
        AddSetting(bodyTexMesh);

        this.femaleAnimations = new LCheckBox("Change Female Animations", EBDMain.settingsFont, EBDMain.settingsColor);
        this.femaleAnimations.tie(EBDSaveFile.Settings.ANIM_FEMALE, EBDMain.save, SUMGUI.helpPanel, true);
        this.femaleAnimations.setOffset(2);
        this.femaleAnimations.addShadow();
        setPlacement(this.femaleAnimations);
        AddSetting(this.femaleAnimations);

        enableConsistency = new LCheckBox("Enable Consistency", EBDMain.settingsFont, EBDMain.settingsColor);
        enableConsistency.tie(EBDSaveFile.Settings.CONSISTENCY, EBDMain.save, SUMGUI.helpPanel, true);
        enableConsistency.setOffset(2);
        enableConsistency.addShadow();
        setPlacement(enableConsistency);
        AddSetting(enableConsistency);


        alignRight();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDMain.EBDEnums;
import EBD.EBDXMLHeadPartParser;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import lev.gui.LNumericSetting;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeadPartEditor extends SPSettingPanel {

    SPSettingPanel prevPanel;
    EBDXMLHeadPartParser XMLParser;
    final EBDEnums.Gender GEN;
    final EBDEnums.Headpart HDPT;
    final String HeadPartType;
    final String Gender;
    final String Title;
    LCheckBox isEnabledBox;
    LCheckBox DisableVanillaHeadparts;
    LNumericSetting HeadChangeProbab;
    LButton goBack;

    public HeadPartEditor(SPMainMenuPanel parent_, String Gender, String HeadPartType, SPSettingPanel pan) {
        super(parent_, Gender + " " + HeadPartType, EBDMain.headerColor);
        this.HeadPartType = HeadPartType;
        this.Gender = Gender;
        this.prevPanel = pan;
        this.Title = Gender + " " + HeadPartType;
        this.XMLParser = new EBDXMLHeadPartParser();

        switch (this.Gender) {
            case "Female":
                this.GEN = EBDEnums.Gender.FEMALE;
                break;
            case "Male":
                this.GEN = EBDEnums.Gender.MALE;
                break;
            default:
                this.GEN = EBDEnums.Gender.FEMALE;
                break;
        }
        switch (this.HeadPartType) {
            case "Hair":
                this.HDPT = EBDEnums.Headpart.HAIR;
                break;
            case "Eyes":
                this.HDPT = EBDEnums.Headpart.EYES;
                break;
            case "Brows":
                this.HDPT = EBDEnums.Headpart.BROWS;
                break;
            case "Scars":
                this.HDPT = EBDEnums.Headpart.SCARS;
                break;
            case "Beard":
                this.HDPT = EBDEnums.Headpart.BEARD;
                break;
            default:
                this.HDPT = EBDEnums.Headpart.HAIR;
                break;
        }
    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        SUMGUI.helpPanel.setTitle(this.Title);
        SUMGUI.helpPanel.setContent("Here you may set whether a specific type of headpart is enabled for a the given gender.\n\n"
                + "The probability determines how likely it is that the headpart is changed for a random NPC.");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();
        isEnabledBox = new LCheckBox("Enabled", EBDMain.settingsFont, EBDMain.settingsColor);

        isEnabledBox.setOffset(2);
        isEnabledBox.addShadow();
        isEnabledBox.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                SUMGUI.helpPanel.setTitle("Change " + Gender + HeadPartType);
                SUMGUI.helpPanel.setContent("Check this box if you want the " + HeadPartType + " for " + Gender + "s changed.");
                SUMGUI.helpPanel.focusOn(isEnabledBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        isEnabledBox.setSelected(XMLParser.isActive(GEN, HDPT));
        setPlacement(isEnabledBox);
        Add(isEnabledBox);

        DisableVanillaHeadparts = new LCheckBox("Disable Vanilla Headparts", EBDMain.settingsFont, EBDMain.settingsColor);

        DisableVanillaHeadparts.setOffset(2);
        DisableVanillaHeadparts.addShadow();
        DisableVanillaHeadparts.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                SUMGUI.helpPanel.setTitle("Disable Vanilla");
                SUMGUI.helpPanel.setContent("Disable the distribution of vanilla " + HeadPartType + " for " + Gender + ".\n\n"
                        + "It is recommended to enable this setting for all headparts types which have mods adding them to your Skyrim installation.\n"
                        + "This means " + HeadPartType + " from Skyrim will not be redistributed by the patcher.");
                SUMGUI.helpPanel.focusOn(DisableVanillaHeadparts, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        DisableVanillaHeadparts.setSelected(XMLParser.isVanillaDisabled(GEN, HDPT));
        setPlacement(DisableVanillaHeadparts);
        Add(DisableVanillaHeadparts);

        HeadChangeProbab = new LNumericSetting("Probability for a change", EBDMain.settingsFont, EBDMain.settingsColor, 0, 100, 1);
        HeadChangeProbab.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                SUMGUI.helpPanel.setTitle("Probability for change");
                SUMGUI.helpPanel.setContent(
                        "Probability to change the " + HeadPartType + " for " + Gender + "s.\n\n"
                        + "This setting is useful if you don't want every NPC's headparts changed by EBD and/or reduce engine load.\n\n"
                        + "The script will decide for every NPC whether to change that NPC or not. The decision taken (to change or not) for that NPC will be remembered until you change the probability here.\n\n"                   
                        + "Using the EBDCustomizerSpell in game it is possible to override the probability set here for specific NPCs.\n\n"
                        + "NOTE: You may set the probability to 0 and use the EBDCustomizerSpell to assign headparts only to specific NPCs."
                        + "Setting it to 100 means that every NPC will be changed.\n\n"
                        + "IMPORTANT: If you change the probability the cards will be mixed again, i.e. the decision whether to change a NPC or not will be made again.\n\n"
                        + "Default: 50");
                SUMGUI.helpPanel.focusOn(HeadChangeProbab, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        HeadChangeProbab.setValue(XMLParser.getProbability(GEN, HDPT));
        setPlacement(HeadChangeProbab);
        Add(HeadChangeProbab);

        goBack = new LButton("Save Settings");
        goBack.setFocusable(true);
        goBack.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                XMLParser.setValues(GEN, HDPT, isEnabledBox.getValue(), DisableVanillaHeadparts.getValue(), HeadChangeProbab.getValue());
                parent.openPanel(prevPanel);

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Save Selected Settings");
                SUMGUI.helpPanel.setContent("Use this button to save changes you made on this page.\n\n"
                        + "Clicking this button will take you back to the previous panel.");
                SUMGUI.helpPanel.focusOn(goBack, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(goBack);
        Add(goBack);

        alignRight();

    }
}

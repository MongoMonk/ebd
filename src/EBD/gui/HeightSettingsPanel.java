/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDSaveFile;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import lev.gui.LNumericSetting;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeightSettingsPanel extends SPSettingPanel {

    LCheckBox DisableUniqueNPCs;
    LNumericSetting FemaleOffset;
    LNumericSetting NordMin;
    LNumericSetting NordMax;
    LNumericSetting BretonMin;
    LNumericSetting BretonMax;
    LNumericSetting ImperialMin;
    LNumericSetting ImperialMax;
    LNumericSetting RedguardMin;
    LNumericSetting RedguardMax;
    LNumericSetting OrcMin;
    LNumericSetting OrcMax;
    LNumericSetting HighElfMin;
    LNumericSetting HighElfMax;
    LNumericSetting WoodElfMin;
    LNumericSetting WoodElfMax;
    LNumericSetting DarkElfMin;
    LNumericSetting DarkElfMax;
    LNumericSetting ArgonianMin;
    LNumericSetting ArgonianMax;
    LNumericSetting KhajiitMin;
    LNumericSetting KhajiitMax;
    LNumericSetting ElderMin;
    LNumericSetting ElderMax;
    LNumericSetting OtherMin;
    LNumericSetting OtherMax;
    LButton SaveSettings;

    public HeightSettingsPanel(SPMainMenuPanel parent_) {
        super(parent_, "Height Settings", EBDMain.headerColor);
    }

    @Override
    protected void initialize() {
        super.initialize();

        DisableUniqueNPCs = new LCheckBox("Disable Unique NPCs", EBDMain.settingsFont, EBDMain.settingsColor);
        DisableUniqueNPCs.tie(EBDSaveFile.Settings.HEIGHT_DISABLE_UNIQUES, EBDMain.save, SUMGUI.helpPanel, true);
        DisableUniqueNPCs.setOffset(2);
        DisableUniqueNPCs.addShadow();
        setPlacement(DisableUniqueNPCs);
        AddSetting(DisableUniqueNPCs);

        FemaleOffset = new LNumericSetting("Female Height Offset", EBDMain.settingsFont, EBDMain.settingsColor, -500, 500, 1);
        FemaleOffset.tie(EBDSaveFile.Settings.HEIGHT_FEMALE_OFFSET, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(FemaleOffset);
        AddSetting(FemaleOffset);

        NordMin = new LNumericSetting("Nord Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        NordMin.tie(EBDSaveFile.Settings.HEIGHT_Nord_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(NordMin);
        AddSetting(NordMin);

        NordMax = new LNumericSetting("Nord Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        NordMax.tie(EBDSaveFile.Settings.HEIGHT_Nord_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        //   NordMax.centerOn(500, NordMin);
        setPlacement(NordMax);
        AddSetting(NordMax);

        BretonMin = new LNumericSetting("Breton Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        BretonMin.tie(EBDSaveFile.Settings.HEIGHT_Breton_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(BretonMin);
        AddSetting(BretonMin);

        BretonMax = new LNumericSetting("Breton Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        BretonMax.tie(EBDSaveFile.Settings.HEIGHT_Breton_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(BretonMax);
        AddSetting(BretonMax);

        ImperialMin = new LNumericSetting("Imperial Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ImperialMin.tie(EBDSaveFile.Settings.HEIGHT_Imperial_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ImperialMin);
        AddSetting(ImperialMin);

        ImperialMax = new LNumericSetting("Imperial Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ImperialMax.tie(EBDSaveFile.Settings.HEIGHT_Imperial_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ImperialMax);
        AddSetting(ImperialMax);

        RedguardMin = new LNumericSetting("Redguard Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        RedguardMin.tie(EBDSaveFile.Settings.HEIGHT_Redguard_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(RedguardMin);
        AddSetting(RedguardMin);

        RedguardMax = new LNumericSetting("Redguard Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        RedguardMax.tie(EBDSaveFile.Settings.HEIGHT_Redguard_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(RedguardMax);
        AddSetting(RedguardMax);

        OrcMin = new LNumericSetting("Orc Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        OrcMin.tie(EBDSaveFile.Settings.HEIGHT_Orc_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(OrcMin);
        AddSetting(OrcMin);

        OrcMax = new LNumericSetting("Orc Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        OrcMax.tie(EBDSaveFile.Settings.HEIGHT_Orc_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(OrcMax);
        AddSetting(OrcMax);

        HighElfMin = new LNumericSetting("HighElf Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        HighElfMin.tie(EBDSaveFile.Settings.HEIGHT_HighElf_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(HighElfMin);
        AddSetting(HighElfMin);

        HighElfMax = new LNumericSetting("HighElf Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        HighElfMax.tie(EBDSaveFile.Settings.HEIGHT_HighElf_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(HighElfMax);
        AddSetting(HighElfMax);

        WoodElfMin = new LNumericSetting("WoodElf Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        WoodElfMin.tie(EBDSaveFile.Settings.HEIGHT_WoodElf_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(WoodElfMin);
        AddSetting(WoodElfMin);

        WoodElfMax = new LNumericSetting("WoodElf Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        WoodElfMax.tie(EBDSaveFile.Settings.HEIGHT_WoodElf_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(WoodElfMax);
        AddSetting(WoodElfMax);

        DarkElfMin = new LNumericSetting("DarkElf Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        DarkElfMin.tie(EBDSaveFile.Settings.HEIGHT_DarkElf_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(DarkElfMin);
        AddSetting(DarkElfMin);

        DarkElfMax = new LNumericSetting("DarkElf Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        DarkElfMax.tie(EBDSaveFile.Settings.HEIGHT_DarkElf_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(DarkElfMax);
        AddSetting(DarkElfMax);

        ArgonianMin = new LNumericSetting("Argonian Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ArgonianMin.tie(EBDSaveFile.Settings.HEIGHT_Argonian_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ArgonianMin);
        AddSetting(ArgonianMin);

        ArgonianMax = new LNumericSetting("Argonian Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ArgonianMax.tie(EBDSaveFile.Settings.HEIGHT_Argonian_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ArgonianMax);
        AddSetting(ArgonianMax);

        KhajiitMin = new LNumericSetting("Khajiit Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        KhajiitMin.tie(EBDSaveFile.Settings.HEIGHT_Khajiit_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(KhajiitMin);
        AddSetting(KhajiitMin);

        KhajiitMax = new LNumericSetting("Khajiit Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        KhajiitMax.tie(EBDSaveFile.Settings.HEIGHT_Khajiit_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(KhajiitMax);
        AddSetting(KhajiitMax);

        ElderMin = new LNumericSetting("Elder Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ElderMin.tie(EBDSaveFile.Settings.HEIGHT_Elder_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ElderMin);
        AddSetting(ElderMin);

        ElderMax = new LNumericSetting("Elder Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        ElderMax.tie(EBDSaveFile.Settings.HEIGHT_Elder_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(ElderMax);
        AddSetting(ElderMax);

        OtherMin = new LNumericSetting("Other Height Minimum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        OtherMin.tie(EBDSaveFile.Settings.HEIGHT_Other_MIN, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(OtherMin);
        AddSetting(OtherMin);

        OtherMax = new LNumericSetting("Other Height Maximum", EBDMain.settingsFont, EBDMain.settingsColor, 1, 500, 1);
        OtherMax.tie(EBDSaveFile.Settings.HEIGHT_Other_MAX, EBDMain.save, SUMGUI.helpPanel, true);
        setPlacement(OtherMax);
        AddSetting(OtherMax);

        SaveSettings = new LButton("Save Values");
        SaveSettings.setSize(100, 30);
        //      SaveSettings.centerOn(SkipFaceTextures, SkipFaceTextures.getBottom());
        //     SaveSettings.centerOn(or, locateTargetSkins.getBottom() + spacing);
        SaveSettings.setFocusable(true);
        SaveSettings.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Save Changed Values");
                SUMGUI.helpPanel.setContent("Use this button to save changes you made");
                SUMGUI.helpPanel.focusOn(SaveSettings, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(SaveSettings);
        Add(SaveSettings);

        alignRight();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDHeadRaceJSON;
import EBD.EBDMain;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import skyproc.FormID;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeadPartCustomRaceEditor extends SPSettingPanel {

    SPSettingPanel prevPanel;
    final HashMap<String, FormID> raceEDIDFormMap;
    final Map<String, LCheckBox> raceBoxes;
    final String Title;
    final EBDHeadRaceJSON headJson;
    final ArrayList<FormID> activeRaces;
    final String currentRaceEDID;
    final FormID currentRaceID;

    LButton goBack;

    public HeadPartCustomRaceEditor(SPMainMenuPanel parent_, SPSettingPanel pan, String sRace, FormID fRace, HashMap<String, FormID> raceEDIDFormMap) {
        super(parent_, sRace, EBDMain.headerColor);
        this.prevPanel = pan;
        this.Title = "Edit " + sRace;
        this.headJson = new EBDHeadRaceJSON();
        this.raceEDIDFormMap = raceEDIDFormMap; //all vanilla races
        this.activeRaces = new ArrayList<>();
        this.currentRaceEDID = sRace;
        this.currentRaceID = fRace;
        this.raceBoxes = new HashMap<>();
        for (Iterator<Map.Entry<String, FormID>> it = this.raceEDIDFormMap.entrySet().iterator(); it.hasNext();) { //remove vampire races && self
            Map.Entry<String, FormID> entry = it.next();
            if (entry.getKey().contains("Vampire") || entry.getKey().equals(this.currentRaceEDID)) {
                it.remove();
            }
        }

    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        SUMGUI.helpPanel.setTitle(this.Title);
        SUMGUI.helpPanel.setContent("Here you may assign vanilla races to treat " +  this.currentRaceEDID + " like those you select.\n\n"
                + "For example if you check NordRace, then NPCs from the " +  this.currentRaceEDID + " will be assigned the same headparts as NPCs from the NordRace.\n\n"
                + "You may select more than one race. In virtually all cases, however, one race should be enough.");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();
        //   LCheckBox tmpBox = new LCheckBox("TEST", EBDMain.settingsFont, EBDMain.settingsColor);
        //tmpBox.setSelected(this.headJson.isActiveRace(this.raceEDIDFormMap.get(race)));            
        //    tmpBox.setOffset(2);
        //    tmpBox.addShadow();
        //     setPlacement(tmpBox);
        //    Add(tmpBox);
        for (String race : this.raceEDIDFormMap.keySet()) {

            //System.out.print(this.jComboBox1.getItemAt(i).toString());
            final LCheckBox tmpBox = new LCheckBox(race, EBDMain.settingsFont, EBDMain.settingsColor);
            //tmpBox.setSelected(this.headJson.isActiveRace(this.raceEDIDFormMap.get(race)));            
            this.raceBoxes.put(race, tmpBox);
            tmpBox.setSelected(headJson.isActiveRace(currentRaceID, this.raceEDIDFormMap.get(race)));
            tmpBox.setOffset(2);
            tmpBox.addShadow();
            setPlacement(tmpBox);
            Add(tmpBox);

        }

        goBack = new LButton("Save Settings");
        goBack.setFocusable(true);
        goBack.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (String race : raceBoxes.keySet()) {
                    if (raceBoxes.get(race).getValue()) {
                        activeRaces.add(raceEDIDFormMap.get(race));
                    }
                }

                headJson.setRace(currentRaceID, activeRaces);
                parent.openPanel(prevPanel);

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Save Selected Settings");
                SUMGUI.helpPanel.setContent("Use this button to save changes you made on this page.\n\n"
                        + "Clicking this button will take you back to the previous panel.");
                SUMGUI.helpPanel.focusOn(goBack, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(goBack);
        Add(goBack);

        alignRight();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import EBD.EBDSaveFile;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LCheckBox;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeadSettingsPanel extends SPSettingPanel {

    LButton HeadPartEditor;
    LButton CustomRaceEditor;
    LCheckBox DisableVanillaHeadparts;
    LCheckBox DisableUniqueNPCs;
    LCheckBox DisableOldNPCs;
    LCheckBox DisableInCombat;
    LCheckBox AddSpells;
    LCheckBox AddSpellsDebug;

    public HeadSettingsPanel(SPMainMenuPanel parent_) {
        super(parent_, "Head Settings", EBDMain.headerColor);
    }

    @Override
    protected void initialize() {
        super.initialize();



        HeadPartEditor = new LButton("Headpart Editor");
        HeadPartEditor.setFocusable(true);
        HeadPartEditor.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {

                HeadPartEditorPanel HPanel = new HeadPartEditorPanel(parent, HeadSettingsPanel.this);
                parent.openPanel(HPanel);
                HPanel.open();

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Headpart Editor");
                SUMGUI.helpPanel.setContent(
                        "Use this to open the Headpart Editor.\n\n"
                        + "There you may enable/disable certain headparts, set their probability and set whether vanilla headparts should be included.");
                SUMGUI.helpPanel.focusOn(HeadPartEditor, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(HeadPartEditor);
        Add(HeadPartEditor);

        CustomRaceEditor = new LButton("Custom Race Editor");
        CustomRaceEditor.setFocusable(true);
        CustomRaceEditor.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {

                HeadPartCustomRacePanel HPanel = new HeadPartCustomRacePanel(parent, HeadSettingsPanel.this);
                parent.openPanel(HPanel);
                HPanel.open();

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Custom Race Editor");
                SUMGUI.helpPanel.setContent(
                        "Use this to open the Custom Race Editor.\n\n"
                        + "By default custom races don't have any valid headparts."                              
                        + "The editor allows you enable distribution of headparts to custom (and DLC) races.\n\n"
                        + "This is achieved by treating the custom race as a vanilla Skyrim race you select in the editor.\n\n"
                        + "Default settings for the DLC races are included."
                        + "Wait until the patcher has finished importing your load order before opening the editor!");
                SUMGUI.helpPanel.focusOn(CustomRaceEditor, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        setPlacement(CustomRaceEditor);
        Add(CustomRaceEditor);        

        DisableUniqueNPCs = new LCheckBox("Disable Unique NPCs", EBDMain.settingsFont, EBDMain.settingsColor);
        DisableUniqueNPCs.tie(EBDSaveFile.Settings.HEAD_DISABLE_UNIQUES, EBDMain.save, SUMGUI.helpPanel, true);
        DisableUniqueNPCs.setOffset(2);
        DisableUniqueNPCs.addShadow();
        setPlacement(DisableUniqueNPCs);
        AddSetting(DisableUniqueNPCs);

        DisableOldNPCs = new LCheckBox("Disable Elderly NPCs", EBDMain.settingsFont, EBDMain.settingsColor);
        DisableOldNPCs.tie(EBDSaveFile.Settings.HEAD_DISABLE_ELDER, EBDMain.save, SUMGUI.helpPanel, true);
        DisableOldNPCs.setOffset(2);
        DisableOldNPCs.addShadow();
        setPlacement(DisableOldNPCs);
        AddSetting(DisableOldNPCs);

        DisableInCombat = new LCheckBox("Disable NPCs in Combat", EBDMain.settingsFont, EBDMain.settingsColor);
        DisableInCombat.tie(EBDSaveFile.Settings.HEAD_DISABLE_IN_COMBAT, EBDMain.save, SUMGUI.helpPanel, true);
        DisableInCombat.setOffset(2);
        DisableInCombat.addShadow();
        setPlacement(DisableInCombat);
        AddSetting(DisableInCombat);

        AddSpells = new LCheckBox("Enable Customizer Spell", EBDMain.settingsFont, EBDMain.settingsColor);
        AddSpells.tie(EBDSaveFile.Settings.HEAD_SPELLS, EBDMain.save, SUMGUI.helpPanel, true);
        AddSpells.setOffset(2);
        AddSpells.addShadow();
        setPlacement(AddSpells);
        AddSetting(AddSpells);

        AddSpellsDebug = new LCheckBox("Enable Debug Spell", EBDMain.settingsFont, EBDMain.settingsColor);
        AddSpellsDebug.tie(EBDSaveFile.Settings.HEAD_SPELLS_DEBUG, EBDMain.save, SUMGUI.helpPanel, true);
        AddSpellsDebug.setOffset(2);
        AddSpellsDebug.addShadow();
        setPlacement(AddSpellsDebug);
        AddSetting(AddSpellsDebug);

        alignRight();

    }
}

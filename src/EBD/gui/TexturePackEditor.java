/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDFileParser;
import EBD.EBDMain;
import EBD.EBDMain.EBDEnums;
import EBD.EBDXMLTexPackParser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LComboBox;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class TexturePackEditor extends SPSettingPanel {

    LComboBox<String> MainPacksBox;
    LButton LoadMainPacks;
    LComboBox<String> GenderBox;
    LComboBox<String> RaceBox;
    EBDXMLTexPackParser XmlParser;

    public TexturePackEditor(SPMainMenuPanel parent_) {
        super(parent_, "TexturePack Editor", EBDMain.headerColor);

    }

    private class MainPackListener implements ActionListener {

        private final LComboBox box;

        MainPackListener(LComboBox b) {
            this.box = b;
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            RaceSelectPanel RSPanel = new RaceSelectPanel(parent, (String) this.box.getSelectedItem(), "None", TexturePackEditor.this, false, XmlParser, (String) this.box.getSelectedItem());
            parent.openPanel(RSPanel);
            RSPanel.open();

        }
    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        SUMGUI.helpPanel.setTitle("TexturePack Editor");
        SUMGUI.helpPanel.setContent("Here you may set the valid races for each MainPack and its SubPacks.\n\n"
                + "Use the lower two combo boxes to select the type of MainPack you want to edit the races for.");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();

        MainPacksBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        MainPacksBox.addItem("Click on \"Load MainPacks\"");

        MainPacksBox.setSize(250, 30);
        MainPacksBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("TexturePack Editor");
                SUMGUI.helpPanel.setContent("Use this combo box to select the MainPack you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(MainPacksBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(MainPacksBox);
        Add(MainPacksBox);

        GenderBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        GenderBox.addItem("Female");
        GenderBox.addItem("Male");

        GenderBox.setSize(250, 30);
        GenderBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("TexturePack Editor");
                SUMGUI.helpPanel.setContent("Use this combo box to select the gender for the MainPacks you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(GenderBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(GenderBox);
        Add(GenderBox);

        RaceBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        RaceBox.addItem("Humanoid");
        RaceBox.addItem("Khajiit");
        RaceBox.addItem("Argonian");
        RaceBox.setSize(250, 30);
        RaceBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("TexturePack Editor");
                SUMGUI.helpPanel.setContent("Use this combo box to select the races for the MainPacks you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(RaceBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(RaceBox);
        Add(RaceBox);

        LoadMainPacks = new LButton("Load MainPacks");
        LoadMainPacks.setFocusable(true);
        LoadMainPacks.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainPacksBox.removeAllItems();
                EBDFileParser FileParser;
                EBDEnums.Gender GENDER;
                EBDEnums.Race RACE;
                switch (GenderBox.getSelectedItem()) {
                    case "Female":
                        GENDER = EBDEnums.Gender.FEMALE;
                        break;
                    case "Male":
                        GENDER = EBDEnums.Gender.MALE;
                        break;
                    default:
                        GENDER = null;
                        break;
                }
                switch (RaceBox.getSelectedItem()) {
                    case "Humanoid":
                        RACE = EBDEnums.Race.HUMANOID;
                        break;
                    case "Khajiit":
                        RACE = EBDEnums.Race.KHAJIIT;
                        break;
                    case "Argonian":
                        RACE = EBDEnums.Race.ARGONIAN;
                        break;
                    default:
                        RACE = null;
                        break;
                }
                if (RACE != null && GENDER != null) {
                    FileParser = new EBDFileParser(GENDER, RACE);

                    XmlParser = new EBDXMLTexPackParser(FileParser.getMainPacksWithSubPacks(), GENDER, RACE);
                    for (String s : XmlParser.getAllMainPacks()) {
                        MainPacksBox.addItem(s);
                    }
                    MainPackListener mp = new MainPackListener(MainPacksBox);
                    MainPacksBox.addEnterButton("Set", mp);
                }
            }

            @Override

            public void mousePressed(MouseEvent e) {
                LoadMainPacks.setText("LOADING...");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                LoadMainPacks.setText("Load MainPacks");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Load Texture Packs");
                SUMGUI.helpPanel.setContent("Wait until the patcher has finished importing & then press this button to list the texture packs.");
                SUMGUI.helpPanel.focusOn(LoadMainPacks, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        }
        );
        setPlacement(LoadMainPacks);
        Add(LoadMainPacks);

        alignRight();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD.gui;

import EBD.EBDMain;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import lev.gui.LButton;
import lev.gui.LComboBox;
import skyproc.gui.SPMainMenuPanel;
import skyproc.gui.SPSettingPanel;
import skyproc.gui.SUMGUI;

/**
 *
 * @author Justin Swanson
 */
public class HeadPartEditorPanel extends SPSettingPanel {

    SPSettingPanel prevPanel;
    LComboBox<String> GenderBox;
    LComboBox<String> HeadPartBox;
    LButton HeadPartEditor;
    final String Title;

    LButton goBack;

    public HeadPartEditorPanel(SPMainMenuPanel parent_, SPSettingPanel pan) {
        super(parent_, "Headpart Editor", EBDMain.headerColor);
        this.prevPanel = pan;
        this.Title = "Headpart Editor";

    }

    @Override
    public void onOpen(SPMainMenuPanel parent) {
        SUMGUI.helpPanel.setTitle(this.Title);
        SUMGUI.helpPanel.setContent("Here you may choose a gender and headpart type to edit.");
        SUMGUI.helpPanel.setDefaultPos();
        SUMGUI.helpPanel.hideArrow();
    }

    @Override
    protected void initialize() {
        super.initialize();

        GenderBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        GenderBox.addItem("Female");
        GenderBox.addItem("Male");

        GenderBox.setSize(250, 30);
        //MainPackListener mp = new MainPackListener(MainPacksBox, EBDMain.getAllRaces());
        GenderBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("HeadPart Editor");
                SUMGUI.helpPanel.setContent("Use this combo box to select the gender for the headpart you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(GenderBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(GenderBox);
        Add(GenderBox);

        HeadPartBox = new LComboBox<>("", EBDMain.settingsFont, EBDMain.settingsColor);
        HeadPartBox.addItem("Hair");
        HeadPartBox.addItem("Eyes");
        HeadPartBox.addItem("Scars");
        HeadPartBox.addItem("Brows");
        HeadPartBox.addItem("Beard");

        HeadPartBox.setSize(250, 30);
        HeadPartBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("HeadPart Editor");
                SUMGUI.helpPanel.setContent("Use this combo box to select headpart you want to edit.\n\n");
                SUMGUI.helpPanel.focusOn(HeadPartBox, 0);
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

        });
        setPlacement(HeadPartBox);
        Add(HeadPartBox);

        HeadPartEditor = new LButton("Edit");
        HeadPartEditor.setFocusable(true);
        HeadPartEditor.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {

                HeadPartEditor HPanel = new HeadPartEditor(parent, (String) GenderBox.getSelectedItem(), HeadPartBox.getSelectedItem(), HeadPartEditorPanel.this);
                parent.openPanel(HPanel);
                HPanel.open();

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Edit Headpart");
                SUMGUI.helpPanel.setContent("Edit the settings for the selected gender/headpart combination.");
                SUMGUI.helpPanel.focusOn(HeadPartEditor, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(HeadPartEditor);
        Add(HeadPartEditor);

        goBack = new LButton("Go Back");
        goBack.setFocusable(true);
        goBack.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                parent.openPanel(prevPanel);

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                SUMGUI.helpPanel.setTitle("Go Back");
                SUMGUI.helpPanel.setContent("Use this button to go back to the previous panel.");
                SUMGUI.helpPanel.focusOn(goBack, 0);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        setPlacement(goBack);
        Add(goBack);

        alignRight();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import EBD.EBDMain.EBDEnums;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Don
 */
public class EBDXMLHeadPartParser {

    private final File XML_FILE;
    private HashMap<String, Boolean> isEnabledMap;
    private HashMap<String, Boolean> isVanillaDisabledMap;
    private HashMap<String, Integer> iProbabMap;

    public EBDXMLHeadPartParser() {
        String XML_FILE_PATH = "DynamicSettings\\HeadPartSettings.xml";
        this.XML_FILE = new File(XML_FILE_PATH);

        this.isEnabledMap = new HashMap<>();
        this.isVanillaDisabledMap = new HashMap<>();
        this.iProbabMap = new HashMap<>();

        this.setupDefaults();

        if (!this.XML_FILE.exists()) {
            try {
                this.XML_FILE.createNewFile();
                this.writeValues();

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "XML File Creation error. File: " + this.XML_FILE.getPath() + ". Error: " + ex);
            }

        } else {
            try {
                this.loadFromFileToMemory();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "XML File Load error. File: " + this.XML_FILE.getPath() + ". Error: " + ex);
            }
        }

    }

    private void setupDefaults() {
        for (EBDEnums.Gender GEN : EBDEnums.Gender.values()) {
            for (EBDEnums.Headpart HDPT : EBDEnums.Headpart.values()) {
                this.isEnabledMap.put(GEN.toString() + HDPT.toString(), Boolean.TRUE);
                if (GEN == EBDEnums.Gender.FEMALE && HDPT == EBDEnums.Headpart.BEARD) {
                    this.isEnabledMap.put(GEN.toString() + HDPT.toString(), Boolean.FALSE); //disable beards for females by default... makes sense right?
                }
                this.isVanillaDisabledMap.put(GEN.toString() + HDPT.toString(), Boolean.TRUE);
                this.iProbabMap.put(GEN.toString() + HDPT.toString(), 50);
            }
        }
    }

    public boolean isActive(EBDEnums.Gender GENDER, EBDEnums.Headpart HDPT) {
        return this.isEnabledMap.get(GENDER.toString() + HDPT.toString());
    }

    //check if at least one HDPT for the Gender is active
    public boolean isActive(EBDEnums.Gender GENDER) {
        boolean isActive = false;
        for (String key : this.isEnabledMap.keySet()) {
            if (key.contains(GENDER.toString())) {
                if (this.isEnabledMap.get(key)) {
                    isActive = true;
                }
            }
        }
        return isActive;
    }

    public boolean isVanillaDisabled(EBDEnums.Gender GENDER, EBDEnums.Headpart HDPT) {
        return this.isVanillaDisabledMap.get(GENDER.toString() + HDPT.toString());
    }

    public int getProbability(EBDEnums.Gender GENDER, EBDEnums.Headpart HDPT) {
        return this.iProbabMap.get(GENDER.toString() + HDPT.toString());
    }

    public void setValues(EBDEnums.Gender GENDER, EBDEnums.Headpart HDPT, boolean isEnabled, boolean isVanillaDisabled, int iProbab) {
        this.isEnabledMap.put(GENDER.toString() + HDPT.toString(), isEnabled);
        this.isVanillaDisabledMap.put(GENDER.toString() + HDPT.toString(), isVanillaDisabled);
        this.iProbabMap.put(GENDER.toString() + HDPT.toString(), iProbab);
        this.writeValues();
    }

    private void setValues(String key, boolean isEnabled, boolean isVanillaDisabled, int iProbab) {
        this.isEnabledMap.put(key, isEnabled);
        this.isVanillaDisabledMap.put(key, isVanillaDisabled);
        this.iProbabMap.put(key, iProbab);
    }

    private File getXML_FILE() {
        return XML_FILE;
    }

    private Document getDocument() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(this.getXML_FILE());
            doc.getDocumentElement().normalize();
            return doc;
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(EBDXMLHeadPartParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void writeValues() {
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("HeadPartSettings");
            doc.appendChild(rootElement);

            for (EBDEnums.Gender GEN : EBDEnums.Gender.values()) {
                for (EBDEnums.Headpart HDPT : EBDEnums.Headpart.values()) {
                    Element Val_Element = doc.createElement("Setting");
                    rootElement.appendChild(Val_Element);
                    Val_Element.setAttribute("Key", GEN.toString() + HDPT.toString());
                    Val_Element.setAttribute("isEnabled", Boolean.toString(this.isActive(GEN, HDPT)));
                    Val_Element.setAttribute("isVanillaDisabled", Boolean.toString(this.isVanillaDisabled(GEN, HDPT)));
                    Val_Element.setAttribute("Probability", Integer.toString(this.getProbability(GEN, HDPT)));
                }
            }

            this.writeXMLFile(doc);

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(EBDMain.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File Write Values error: " + " : " + ex);
        }
    }

    private void loadFromFileToMemory() {

        Document doc = this.getDocument();

        NodeList Val_List = doc.getElementsByTagName("Setting");
        for (int j = 0; j < Val_List.getLength(); j++) {
            Element NPC_Element = (Element) Val_List.item(j);

            this.setValues(NPC_Element.getAttribute("Key"), Boolean.valueOf(NPC_Element.getAttribute("isEnabled")), Boolean.valueOf(NPC_Element.getAttribute("isVanillaDisabled")), Integer.valueOf(NPC_Element.getAttribute("Probability")));

        }

    }

    private void writeXMLFile(Document doc) {
        try {

            doc.getDocumentElement().normalize();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(this.getXML_FILE());

            transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
            transformer.setOutputProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Logger.getLogger(EBDXMLHeadPartParser.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "XML File Write error: " + this.getXML_FILE().getName() + " : " + ex);
        }
    }

}

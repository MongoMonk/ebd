package EBD;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Random;
import org.uncommons.maths.random.*;

/**
 *
 * @author Don
 */
public class EBDRandomFunctions {

    private final Random RandomGen;

    public EBDRandomFunctions() {
        this.RandomGen = new MersenneTwisterRNG();

    }

    public int RandomInt(int min, int max) {

        if (min == max) {
            return min;
        }

        if (min > max) {  //swap them around if messed up min/max
            int tmp = min;
            min = max;
            max = tmp;
        }

        DiscreteUniformGenerator DUG = new DiscreteUniformGenerator(min, max, this.RandomGen);

        return DUG.nextValue();
    }

    public boolean Decider(int probab) {
        int i = this.RandomInt(1, 100);
        return i <= probab;
    }


//    public float getTwoDecimalFloat(float flt) {
//        DecimalFormat df = new DecimalFormat("0.00");
//        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
//        return Float.parseFloat(df.format(flt));
//    }

//    public float RandomFloat(double mean, double sd) {
//        GaussianGenerator gen = new GaussianGenerator(mean, sd, RandomGen);      
//        double value = gen.nextValue();
//        return Float.parseFloat(new DecimalFormat("##.##").format(value));
//    }
}

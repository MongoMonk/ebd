/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EBD;

import EBD.EBDMain.EBDSettings;
import java.util.ArrayList;
import skyproc.*;

/**
 *
 * @author Don
 */
public class EBDUtilityFuncs {

    private static final EBDRandomFunctions rnd = new EBDRandomFunctions();

    static class npcFuncs {

        private static final FormID PlayerCharID = new FormID("000007", "Skyrim.esm");
        private static final FormID ActorNPC = new FormID("013794", "Skyrim.esm");
        private static final FormID IsBeastRace = new FormID("0D61D1", "Skyrim.esm");
        //   private static final String[] FORBIDDEN_STRINGS = {"Test", "Manakin", "Invisible"};

        public static FormID getPlayerID() {
            return PlayerCharID;
        }

        // this function sets the npc's FTST record (i.e. the face texture)
        // to the default one of the npc's race if it's not set already
        // this is needed in conjunction with the headpart feature
        // if FTST is null, then applying custom headparts will lead to dark face
        public static boolean setBaseFaceID(NPC_ npc, RACE raceRC, skyproc.genenums.Gender SKYPROCGEN) {
            if (!hasNPCHeadPartTexture(npc) & (npc.getFeatureSet() == null | npc.getFeatureSet().isNull())) {
                npc.setFeatureSet(getBaseFaceID(npc, raceRC, SKYPROCGEN));
                return true;
            }
            return false;
        }

        private static FormID getBaseFaceID(NPC_ npc, RACE raceRC, skyproc.genenums.Gender SKYPROCGEN) {
            FormID baseFaceID = npc.getFeatureSet();
            if (baseFaceID.isNull()) {
                baseFaceID = raceRC.getDefaultFaceTexture(SKYPROCGEN);
                if (baseFaceID.isNull()) {
                    ArrayList<FormID> baseFaceList = raceRC.getFaceDetailsTextureSet(SKYPROCGEN);
                    if (!baseFaceList.isEmpty()) {
                        baseFaceID = baseFaceList.get(0);
                    }
                }
            }
            return baseFaceID;
        }

        // check whether the NPC has a face texture set via a headpart
        private static boolean hasNPCHeadPartTexture(NPC_ npc) {
            for (FormID hdpt_id : npc.getHeadParts()) {
                HDPT hdpt = (HDPT) EBD.EBDMain.merger.getMajor(hdpt_id, GRUP_TYPE.HDPT);
                try {
                    if (hdpt.getHDPT_Type().equals(skyproc.HDPT.HDPT_Type.Face)) {
                        if (hdpt.getBaseTexture() != null && !hdpt.getBaseTexture().isNull()) {
                            return true;
                        }
                    }
                } catch (Exception e) {

                }

            }
            return false;
        }

        public static boolean isValidNPCTexChangeHuman(NPC_ npc, RACE npcrace) {
            return (isValidNPCTexChange(npc) && (isPlayableRaceHumanoid(npcrace)));
        }

        public static boolean isValidNPCTexChangeKhajiit(NPC_ npc, RACE npcrace) {
            return (isValidNPCTexChange(npc) && (isPlayableRaceKhajiit(npcrace)));
        }

        public static boolean isValidNPCTexChangeArgonian(NPC_ npc, RACE npcrace) {
            return (isValidNPCTexChange(npc) && (isPlayableRaceArgonian(npcrace)));
        }

        private static boolean isValidNPCTexChange(NPC_ npc) {
            // check if NPC is unique and disable uniques enabled
            if (EBDSettings.isBodyTexturesDisableUniquesEnabled() && npc.get(NPC_.NPCFlag.Unique)) {
                return false;
            }
            // if overwrite custom is enabled the NPC is now valid
            if (EBDSettings.isBodyTexturesOverwriteCustomEnabled()) {
                return true;
            }
            // if overwrite custom skin is not enabled, check whether the NPC
            // has a custom skin already set
            return (npc.getSkin().isNull());// && (!npc.get(NPC_.NPCFlag.IsGhost))); //only change NPCs which don't already have a custom skin set; ghost flag doesn't seem to be for ghosts but rather for actors not taking damage
        }

        public static boolean isValidNPCHeightChange(NPC_ npc) {
            if (EBDSettings.HeightSettings.isHeightDisableUniquesEnabled() && npc.get(NPC_.NPCFlag.Unique)) {
                return false;
            }
            return (npc.getHeight() == 1.0);   //only change NPC height if it is at the default value of 1.0. This means that already height altered NPCs are not changed
        }

        public static boolean isValidNPC(NPC_ npc, RACE npcrace) {
            boolean validNpc = false;
            if ((!(npc.getForm().equals(PlayerCharID))) && (isPlayableRace(npcrace))) {  // && (!npc.get(NPC_.NPCFlag.IsCharGenFacePreset); don't think they have to be excluded
                validNpc = true;
            }
            for (String s : EBDTextFileParser.getNpcEDIDBlacklist()) {
                if (s.startsWith("\"") && s.endsWith("\"")) {  //check for exact match inside double quotes
                    String sn = s.replace("\"", "");
                    if (npc.getEDID().toLowerCase().matches(sn.toLowerCase())) {
                        validNpc = false;  //matches a forbidden string

                    }
                    continue;    //do not check for containing if exact match is forced

                }
                if (npc.getEDID().toLowerCase().contains(s.toLowerCase())) {
                    validNpc = false;  //contains a forbidden string
                }

            }
            return validNpc;
        }

        public static boolean isPlayableRace(RACE race) {
            boolean validRace = false;
            if (race != null) {
                if (race.getKeywordSet().getKeywordRefs().contains(ActorNPC) && (!race.get(RACE.RACEFlags.Child)) && !race.getEDID().toLowerCase().contains("child")) {
                    validRace = true; //no child and NPC race
                    for (String s : EBDTextFileParser.getRaceEDIDBlacklist()) {
                        if (s.startsWith("\"") && s.endsWith("\"")) {  //check for exact match inside double quotes
                            String sn = s.replace("\"", "");
                            if (race.getEDID().toLowerCase().matches(sn.toLowerCase())) {
                                validRace = false;  //matches a forbidden string

                            }
                            continue;    //do not check for containing if exact match is forced
                        }
                        if (race.getEDID().toLowerCase().contains(s.toLowerCase())) {
                            validRace = false;  //contains a forbidden string
                        }

                    }
                }

            }
            return validRace;
        }

        public static boolean isSkyrimFormID(FormID form) {
            ModListing skyrimModListing = new ModListing("Skyrim", true);
            return form.getMaster().equals(skyrimModListing);
        }

        public static boolean isPlayableRaceHumanoid(RACE race) {

            if (race != null) {
                if ((!race.getKeywordSet().getKeywordRefs().contains(IsBeastRace)) && isPlayableRace(race)) {
                    return true;
                }

            }
            return false;
        }

        public static boolean isPlayableRaceKhajiit(RACE race) {
            boolean validRace = false;
            if (race != null) {
                if (isPlayableRace(race) && (race.getKeywordSet().getKeywordRefs().contains(IsBeastRace))) {
                    ArrayList<String> tmpList = EBDTextFileParser.getKhajiitEDIDList();
                    if (tmpList.isEmpty()) {
                        validRace = true;
                    } else {
                        for (String s : tmpList) {
                            if (s.startsWith("\"") && s.endsWith("\"")) {  //check for exact match inside double quotes
                                String sn = s.replace("\"", "");
                                if (race.getEDID().toLowerCase().matches(sn.toLowerCase())) {
                                    validRace = true;  //matches an allowed string

                                }
                                continue;    //do not check for containing if exact match is forced
                            }
                            if (race.getEDID().toLowerCase().contains(s.toLowerCase())) {
                                validRace = true;  //contains a valid string
                            }

                        }
                    }
                }

            }
            return validRace;
        }

        public static boolean isPlayableRaceArgonian(RACE race) {
            boolean validRace = false;
            if (race != null) {
                if (isPlayableRace(race) && (race.getKeywordSet().getKeywordRefs().contains(IsBeastRace))) {
                    ArrayList<String> tmpList = EBDTextFileParser.getArgonianEDIDList();
                    if (tmpList.isEmpty()) {
                        validRace = true;
                    } else {
                        for (String s : tmpList) {
                            if (s.startsWith("\"") && s.endsWith("\"")) {  //check for exact match inside double quotes
                                String sn = s.replace("\"", "");
                                if (race.getEDID().toLowerCase().matches(sn.toLowerCase())) {
                                    validRace = true;  //matches an allowed string

                                }
                                continue;    //do not check for containing if exact match is forced
                            }
                            if (race.getEDID().toLowerCase().contains(s.toLowerCase())) {
                                validRace = true;  //contains a valid string
                            }

                        }
                    }
                }
            }
            return validRace;
        }

        public static ArrayList<FormID> getAllRaces(GRUP<RACE> RACES) {
            ArrayList<FormID> tmpList = new ArrayList<>();
            RACES.getRecords().stream().filter((tmpRace) -> ((isPlayableRace(tmpRace)))).forEach((tmpRace) -> {
                tmpList.add(tmpRace.getForm());
            });
            return tmpList;
        }

        public static int convertRGBtoInt(float value) {
            value = value * 255;
            return (int) value;
        }

        public static float converRGBtoFloat(int value) {
            float tmpfloat = (float) value / 255;
            return tmpfloat;
        }

        public static float getOffset(float value, int offset) {
            int tmpval = convertRGBtoInt(value);
            tmpval = rnd.RandomInt(tmpval - offset, tmpval + offset);
            if (tmpval < 0) {
                tmpval = 0;
            }
            if (tmpval > 255) {
                tmpval = 255;
            }
            return converRGBtoFloat(tmpval);
        }

        public static int findTintLayer(float red, float green, float blue, ArrayList<NPC_.TintLayer> face) {
            short sred = (short) convertRGBtoInt(red);
            short sgreen = (short) convertRGBtoInt(green);
            short sblue = (short) convertRGBtoInt(blue);
            int index = -1;
            for (int i = 0; i < face.size(); i++) {
                if ((face.get(i).getColor(RGBA.Blue) == sblue) && (face.get(i).getColor(RGBA.Red) == sred) && (face.get(i).getColor(RGBA.Green) == sgreen)) {
                    index = i;
                }
            }
            //       if (face.isEmpty() == false) {
//            for (NPC_.TintLayer tmpface : face) {
            //               if ((tmpface.getColor(RGBA.Blue) == sblue) && (tmpface.getColor(RGBA.Red) == sred) && (tmpface.getColor(RGBA.Green) == sgreen)) {
            //                  index = tmpface.getIndex();
            //              }
            //         }
            //   }
            //   face.size();
            return index;
        }

        public static float getNPCHeightByRace(RACE race, NPC_ npc) {
            if (race == null) { //if no race is found just return old height
                return npc.getHeight();
            } else {
                String RaceEDID = race.getEDID();
                int newWeight;
                if (RaceEDID.contains("Nord")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getNordMin(), EBDSettings.HeightSettings.getNordMax());
                } else if (RaceEDID.contains("Breton")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getBretonMin(), EBDSettings.HeightSettings.getBretonMax());
                } else if (RaceEDID.contains("Imperial")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getImperialMin(), EBDSettings.HeightSettings.getImperialMax());
                } else if (RaceEDID.contains("Redguard")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getRedguardMin(), EBDSettings.HeightSettings.getRedguardMax());
                } else if (RaceEDID.contains("Orc")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getOrcMin(), EBDSettings.HeightSettings.getOrcMax());
                } else if (RaceEDID.contains("HighElf")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getHighElfMin(), EBDSettings.HeightSettings.getHighElfMax());
                } else if (RaceEDID.contains("WoodElf")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getWoodElfMin(), EBDSettings.HeightSettings.getWoodElfMax());
                } else if (RaceEDID.contains("DarkElf")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getDarkElfMin(), EBDSettings.HeightSettings.getDarkElfMax());
                } else if (RaceEDID.contains("Argonian")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getArgonianMin(), EBDSettings.HeightSettings.getArgonianMax());
                } else if (RaceEDID.contains("Khajiit")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getKhajiitMin(), EBDSettings.HeightSettings.getKhajiitMax());
                } else if (RaceEDID.contains("Elder")) {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getElderMin(), EBDSettings.HeightSettings.getElderMax());

                } else {
                    newWeight = rnd.RandomInt(EBDSettings.HeightSettings.getOtherMin(), EBDSettings.HeightSettings.getOtherMax());
                }
                if (npc.get(NPC_.NPCFlag.Female)) { //if npc is female add/substract the female weight offset
                    newWeight += EBDSettings.HeightSettings.getFemaleHeightOffset();

                }

                return (float) newWeight / 100;
            }
        }

        public static int getHeightHash() {
            int hash;
            hash = 41;

            hash = 79 * hash + EBDSettings.HeightSettings.getNordMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getNordMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getBretonMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getBretonMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getImperialMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getImperialMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getRedguardMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getRedguardMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getOrcMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getOrcMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getHighElfMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getHighElfMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getWoodElfMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getWoodElfMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getDarkElfMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getDarkElfMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getArgonianMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getArgonianMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getKhajiitMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getKhajiitMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getElderMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getElderMax();

            hash = 79 * hash + EBDSettings.HeightSettings.getOtherMin();
            hash = 79 * hash + EBDSettings.HeightSettings.getOtherMax();

            hash = 79 * hash + (int) EBDSettings.HeightSettings.getFemaleHeightOffset();
            return hash;
        }

        public static int getRandomWeight() {
            int i = rnd.RandomInt(0, 100);
            return i;
        }

        public static int getWeightOffset(int weight, int offset) {
            weight = rnd.RandomInt(weight - offset, weight + offset);
            if (weight < 0) {
                weight = 0;
            } else if (weight > 100) {
                weight = 100;
            }
            return weight;
        }

        public static int getWeightOffsetRec(int weight, int offset) {
            if (weight > 100) {
                weight = 100;
            } else if (weight < 0) {
                weight = 0;
            }
            if (offset == 100) {
                return getRandomWeight();
            } else if (offset == 0) {
                return weight;
            } else {
                int newweight = rnd.RandomInt(weight - offset, weight + offset);
                while ((newweight < 0) || (newweight > 100)) {
                    newweight = rnd.RandomInt(weight - offset, weight + offset);
                }
                return newweight;
            }
        }

    }
}
